sbcl := $(shell which sbcl)
ccl := $(shell which lx86cl64)

scenic-demo:
	$(sbcl) --eval '(progn (push :scenic-demo *features*)(ql:quickload :scenic-demo)(save-lisp-and-die "scenic-demo" :toplevel (symbol-function (intern "DEMO-TOPLEVEL" :scenic.demo)) :executable t :compression t))'

scenic-demo-ccl:
	$(ccl) --eval '(progn (push :scenic-demo *features*)(ql:quickload :scenic-demo)(save-application "scenic-demo" :toplevel-function (symbol-function (intern "DEMO-TOPLEVEL" :scenic.demo)) :prepend-kernel t))'
