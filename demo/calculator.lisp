(ql:quickload (list :COM.INFORMATIMAGO.COMMON-LISP.CESARUM
                    :cl-hooks
                    :parse-float))

(defpackage :scenic.demo.calculator
  (:use :cl :scenic :scenic.demo
        :scenic-helpers
        :COM.INFORMATIMAGO.COMMON-LISP.CESARUM.DFA
        :hooks))

(in-package :scenic.demo.calculator)

(defun send-event (dfa event &rest args)
  (apply #'hooks:run-hook (hooks:object-hook dfa 'events-hook)
         event
         dfa
         args))

(defun calc-sub (dfa)
  (if (null (result dfa))
      (setf (result dfa)
            (parse-float:parse-float (short-exp dfa)))
      ;; else
      (setf (result dfa)
            (funcall (operator dfa)
                     (result dfa)
                     (parse-float:parse-float (short-exp dfa)))))
  (setf (short-exp dfa) ""))

(defun clear-calc (dfa)
  (setf (short-exp dfa) ""))

(defun clear-all-calc (dfa)
  (clear-calc dfa)
  (setf (result dfa) nil))

(define-state-machine calculator-dfa
    :slots ((result :initform nil :accessor result)
            (short-exp :initform "" :accessor short-exp)
            (operator :initform nil :accessor operator)
            (events-hook :initform nil))
    :initial ready
    :states
    ((ready
      (minus ()
             (setf (short-exp dfa) "-")
             (send-event dfa :update-display (short-exp dfa))
             (go-to-state operand-1))
      (oper (op)
            (setf (operator dfa) op)
            (send-event dfa :update-display (result dfa))
            (go-to-state op-entered))
      (digit (d)
             (when (not (zerop d))
               (setf (short-exp dfa)
                     (format nil "~A~A" (short-exp dfa) d))
               (send-event dfa :update-display (short-exp dfa))
               (go-to-state operand-1)))
      (point ()
             (setf (short-exp dfa)
                   (format nil "~A." (short-exp dfa)))
             (send-event dfa :update-display (short-exp dfa))
             (go-to-state operand-1))
      ;; We would need substates for specifying clear and clear-all only once
      (res ()
           (calc-sub dfa)
           (send-event dfa :update-display (result dfa))
           (go-to-state ready))
      (clear ()
             (clear-calc dfa)
             (send-event dfa :update-display "0"))
      (clear-all ()
                 (clear-all-calc dfa)
                 (send-event dfa :update-display "0")
                 (go-to-state ready)))
     (operand-1
      (minus ()
             (calc-sub dfa)
             (setf (operator dfa) #'-)
             (send-event dfa :update-display (result dfa))
             (go-to-state op-entered))
      (oper (op)
            (calc-sub dfa)
            (setf (operator dfa) op)
            (send-event dfa :update-display (result dfa))
            (go-to-state op-entered))
      (digit (d)
             (setf (short-exp dfa)
                   (format nil "~A~A" (short-exp dfa) d))
             (send-event dfa :update-display (short-exp dfa)))
      (point ()
             (setf (short-exp dfa)
                   (format nil "~A." (short-exp dfa)))
             (send-event dfa :update-display (short-exp dfa)))
      ;; We would need substates for specifying clear and clear-all only once
      (res ()
           (calc-sub dfa)
           (send-event dfa :update-display (result dfa))
           (go-to-state ready))
      (clear ()
             (clear-calc dfa)
             (send-event dfa :update-display "0"))
      (clear-all ()
                 (clear-all-calc dfa)
                 (send-event dfa :update-display "0")
                 (go-to-state ready)))
     (op-entered
      (:entry ()
              (send-event dfa :update-display "0"))
      #+nil(:exit  ()
                   (print `(exiting op-entered)))
      #+nil(oper ((op (eql :minus)))
                 (setf (short-exp dfa) "-")
                 (send-event dfa :update-display)
                 (go-to-state operand-2))
      (minus ()
             ;; Replace the operation? Error?
             (setf (operator dfa) #'-))
      (oper (op)
            ;; Replace the operation? Error?
            (setf (operator dfa) op))
      (digit (d)
             (setf (short-exp dfa)
                   (format nil "~A~A" (short-exp dfa) d))
             (send-event dfa :update-display (short-exp dfa))
             (go-to-state operand-2))
      (point ()
             (setf (short-exp dfa)
                   (format nil "~A." (short-exp dfa)))
             (send-event dfa :update-display (short-exp dfa))
             (go-to-state operand-2))
      ;; We would need substates for specifying clear and clear-all only once
      (res ()
           (calc-sub dfa)
           (send-event dfa :update-display (result dfa))
           (go-to-state ready))
      (clear ()
             (clear-calc dfa)
             (send-event dfa :update-display "0"))
      (clear-all ()
                 (clear-all-calc dfa)
                 (send-event dfa :update-display "0")
                 (go-to-state ready)))
     (operand-2
      (minus ()
             (calc-sub dfa)
             (setf (operator dfa) #'-)
             (send-event dfa :update-display (result dfa))
             (go-to-state op-entered))
      (oper (op)
            (calc-sub dfa)
            (setf (operator dfa) op)
            (send-event dfa :update-display (result dfa))
            (go-to-state op-entered))
      (digit (d)
             (setf (short-exp dfa)
                   (format nil "~A~A" (short-exp dfa) d))
             (send-event dfa :update-display (short-exp dfa)))
      (point ()
             (setf (short-exp dfa)
                   (format nil "~A." (short-exp dfa)))
             (send-event dfa :update-display (short-exp dfa)))
      ;; We would need substates for specifying clear and clear-all only once
      (res ()
           (calc-sub dfa)
           (send-event dfa :update-display (result dfa))
           (go-to-state ready))
      (clear ()
             (clear-calc dfa)
             (send-event dfa :update-display "0"))
      (clear-all ()
                 (clear-all-calc dfa)
                 (send-event dfa :update-display "0")
                 (go-to-state ready)))))

(defmethod initialize-instance :after ((dfa calculator-dfa) &rest initargs)
  (hooks:object-hook dfa 'events-hook))

(defparameter *calculator-dfa* (make-calculator-dfa))

(hooks:add-to-hook (hooks:object-hook *calculator-dfa* 'events-hook)
                   (lambda (event dfa &rest args)
                     (format t "Event: ~A DFA: ~A ~@{~A~}~%" event dfa args)))

;; logging
(defmethod (setf short-exp) :after (val (dfa calculator-dfa))
  (format t "Short exp: ~A~%" val))

(defmethod (setf result) :after (val (dfa calculator-dfa))
  (format t "Result: ~A~%" val))

(DEFMETHOD ON-ENTRY :before ((DFA CALCULATOR-DFA) state &rest args)
  (format t "Entering: ~A~%" state))

;; (digit *calculator-dfa* 2)
;; (digit *calculator-dfa* 3)
;; (oper *calculator-dfa* '+)
;; (digit *calculator-dfa* 2)
;; (digit *calculator-dfa* 5)
;; (res *calculator-dfa*)
;; (oper *calculator-dfa* '-)
;; (digit *calculator-dfa* 2)
;; (res *calculator-dfa*)


;; (digit *calculator-dfa* 2)
;; (digit *calculator-dfa* 3)
;; (oper *calculator-dfa* '+)
;; (digit *calculator-dfa* 2)
;; (digit *calculator-dfa* 5)
;; (oper *calculator-dfa* '-)
;; (digit *calculator-dfa* 2)
;; (digit *calculator-dfa* 5)
;; (res *calculator-dfa*)
;; (res *calculator-dfa*)

;; (digit *calculator-dfa* 2)
;; (digit *calculator-dfa* 3)
;; (point *calculator-dfa*)
;; (digit *calculator-dfa* 5)
;; (oper *calculator-dfa* '+)
;; (digit *calculator-dfa* 2)
;; (digit *calculator-dfa* 5)
;; (oper *calculator-dfa* '-)
;; (digit *calculator-dfa* 2)
;; (digit *calculator-dfa* 5)
;; (res *calculator-dfa*)

;; (clear-all *calculator-dfa*)

;; (minus *calculator-dfa*)
;; (digit *calculator-dfa* 3)
;; (point *calculator-dfa*)
;; (digit *calculator-dfa* 5)
;; (oper *calculator-dfa* '+)
;; (digit *calculator-dfa* 2)
;; (digit *calculator-dfa* 5)
;; (oper *calculator-dfa* '-)
;; (digit *calculator-dfa* 2)
;; (digit *calculator-dfa* 5)
;; (res *calculator-dfa*)

;; (clear-all *calculator-dfa*)

;; (minus *calculator-dfa*)
;; (minus *calculator-dfa*)

(defun calculator ()
  (let* ((display nil)
         (calculator-dfa (make-calculator-dfa)))
    (hooks:add-to-hook (hooks:object-hook calculator-dfa 'events-hook)
                       (lambda (event dfa &rest args)
                         (ecase event
                           (:update-display (setf (text display) (princ-to-string (first args)))))))
    (flet ((calc-button (label action)
             (let ((button (scenic.nui::nui-button-labelled label)))
               (scenic.nui::on button :click action)
               button))
           (digit-action (digit)
             (lambda (btn ev)
               (digit calculator-dfa digit))))
      (with-bindings
        (background
         :bind calc
         cl-colors:+white+
         (scenic.nui::flow-col (:spacing 5)
           (sizer (setf display (label "0" :size 50)) :height 50)
           (sizer
            (grid nil nil
                  (append
                   (loop for ns in '((7 8 9) (4 5 6) (1 2 3))
                      for operator in '(+ - *)
                      collect
                        (cons :row
                              (append
                               (loop for n in ns
                                  collect
                                    (list :cell (uniform-padding
                                                 2
                                                 (calc-button (princ-to-string n)
                                                              (let ((n0 n))
                                                                (digit-action n0))))))
                               (list (list :cell (uniform-padding
                                                  2
                                                  (calc-button (princ-to-string operator)
                                                               (let ((op operator))
                                                                 (lambda (btn ev)
                                                                   (oper calculator-dfa op))))))))))
                   (list
                    (list :row
                          (list :cell :colspan 2(uniform-padding
                                       2 (calc-button "0" (digit-action 0))))
                          (list :cell (uniform-padding
                                       2 (calc-button "." (lambda (btn ev)
                                                            (point calculator-dfa)))))
                          (list :cell (uniform-padding
                                       2
                                       (calc-button "/"
                                                    (lambda (btn ev)
                                                      (oper calculator-dfa '/))))))
                    (list :row
                          (list :cell (uniform-padding
                                       2 (calc-button "CA" (lambda (btn ev)
                                                             (clear-all calculator-dfa)))))
                          (list :cell (uniform-padding
                                       2 (calc-button "C" (lambda (btn ev)
                                                            (clear calculator-dfa)))))
                          (list :cell :colspan 2
                                (uniform-padding
                                       2 (calc-button "=" (lambda (btn ev)
                                                            (res calculator-dfa)))))
                          ))))
            :height 300)
           ))
        (let ((scene (scenic:scene 300 360 calc)))
          (scenic:run-scene scene))))))
