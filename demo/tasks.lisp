(in-package :scenic.demo)

(defwidget progress-bar (scenic:widget)
  ((progress :initarg :progress
             :initform 0.0
             :accessor progress)
   (background-color :initarg :background-color
                     :initform cl-colors:+white+
                     :accessor background-color)
   (color :initarg :color
          :initform cl-colors:+blue+
          :accessor progress-bar-color)))

(defmethod scenic:paint ((progress-bar progress-bar))
  (scenic-utils::fill-rect (layout-left progress-bar)
                           (layout-top progress-bar)
                           (layout-width progress-bar)
                           (layout-height progress-bar)
                           :fill-color (background-color progress-bar))
  (let ((progress-width (round (* (progress progress-bar) (layout-width progress-bar)))))
    (when (not (zerop progress-width))
      (scenic-utils::fill-rect (layout-left progress-bar)
                               (layout-top progress-bar)
                               progress-width
                               (layout-height progress-bar)
                               :fill-color (progress-bar-color progress-bar)))))

(defmethod (setf progress) :after (value (progress-bar progress-bar))
  (scenic:invalidate progress-bar))

(defun progress-bar-demo-1 ()
  (with-bindings 
    (let* ((comp (sizer (make-instance 'progress-bar :bind progress-bar)
                        :width 300 :height 50))
           (scene (scene 500 500 comp)))
      (setf (scenic:on-scene-init scene)
          (lambda ()
            (scenic:with-thread ()
              (let ((times 20))
                (print "Progress started")
                (dotimes (x times)
                  (sleep 0.5)
                  (format t "Progress: ~f%~%" (progress progress-bar))
                  (scenic:as-ui-task ()
                    (incf (progress progress-bar) (/ 1 times))))
                (print "Progress end")))))
      (scenic:run-scene scene))))

;; (progress-bar-demo-1)

(defun progress-bar-demo-2 ()
  (with-bindings 
    (let* ((comp (simple-vertical-pack
                  5
                  (list (sizer (make-instance 'progress-bar :bind progress-bar-1)
                               :width 300 :height 50)
                        (sizer (make-instance 'progress-bar :bind progress-bar-2
                                              :color cl-colors:+red+)
                               :width 300 :height 50))))
           (scene (scene 500 500 comp)))
      (setf (scenic:on-scene-init scene)
            (lambda ()
              (scenic:with-thread ()
                (let ((times 20))
                  (dotimes (x times)
                    (sleep 0.5)
                    (scenic:as-ui-task ()
                      (incf (progress progress-bar-1) (/ 1 times))))))
              (scenic:with-thread ()
                (let ((times 100))
                  (dotimes (x times)
                    (sleep 0.2)
                    (scenic:as-ui-task ()
                      (incf (progress progress-bar-2) (/ 1 times))))))))
      (scenic:run-scene scene))))

;; (progress-bar-demo-2)

(defun progress-bar-async-demo-1 ()
  (with-bindings 
    (let* ((comp (sizer (make-instance 'progress-bar :bind progress-bar)
                        :width 300 :height 50))
           (scene (scene 500 500 comp)))
      (setf (scenic:on-scene-init scene)
          (lambda ()
            (scenic:as-async-task (:on-start (lambda (&rest args)
                                                (print "Progress started"))
                                              :on-progress (lambda (progress &rest args)
                                                             (format t "Progress: ~A~%" progress)
                                                             (setf (progress progress-bar) progress))
                                              :on-complete (lambda (result &rest args)
                                                             (print "Progress completed")))
              (let ((times 20)
                    (progress 0))
                (dotimes (x times)
                  (sleep 0.5)
                  (incf progress (/ 1 times))
                  (scenic:update-progress progress))))))
      (scenic:run-scene scene))))

;; (scenic:start-async-task-processors)
;; (progress-bar-async-demo-1)

;; (dotimes (x 10)
;;   (progress-bar-async-demo-1))
