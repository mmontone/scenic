(defpackage :scenic.doc
  (:use :cl))

(in-package :scenic.doc)

(mgl-pax:defsection @introduction (:title "Introduction" :export nil)
    "**Scenic** is a widget toolkit"
    (scenic:widget class))

(defparameter +html-tpl+
  (cl-template:compile-template (alexandria:read-file-into-string (asdf:system-relative-pathname :scenic "doc/scenic.tpl"))))

(with-open-file (f (asdf:system-relative-pathname :scenic "doc/scenic.html")
                   :direction :output
                   :if-exists :supersede
                   :if-does-not-exist :create)
  (write-string (funcall +html-tpl+
                         (list :content (first (mgl-pax:document @introduction :format :html))))
                f))


