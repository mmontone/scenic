(defpackage :nanovg 
  (:nicknames :nvg))

(autowrap:c-include "nanovg.h"
                    :spec-path '(nanovg)
                    :definition-package nanovg
                    :function-package nanovg
                    :wrapper-package nanovg
                    :accessor-package nanovg
                    :constant-package nanovg
                    :extern-package nanovg)

(defpackage :nanovg.gl
  (:nicknames :nvg.gl))

(autowrap:c-include "nanovg_gl.h"
                    :spec-path '(nanovg)
                    :definition-package nanovg.gl
                    :function-package nanovg.gl
                    :wrapper-package nanovg.gl
                    :accessor-package nanovg.gl
                    :constant-package nanovg.gl
                    :extern-package nanovg.gl)
