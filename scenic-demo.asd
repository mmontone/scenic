(asdf:defsystem #:scenic-demo
  :serial t
  :depends-on (:scenic :sdl2-image)
  :components ((:module src
                        :components
                        ((:file "sdl2-image")))
               (:module test
                        :components
                        ((:file "test")))
               (:module demo
                        :components
                        ((:file "package")
                         (:file "animation")
                         (:file "tasks")
                         (:file "experiments")
                         (:file "demo")))))
