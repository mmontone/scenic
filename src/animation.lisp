(in-package :scenic.animation)

;;-- * Animations

(defclass scenic-animation ()
  ((start-callback :initarg :on-start
                   :initform nil
                   :type (or null symbol function)
                   :accessor start-callback)
   (stop-callback :initarg :on-stop
                  :initform nil
                  :type (or null symbol function)
                  :accessor stop-callback)
   (change-callback :initarg :on-change
                    :initform nil
                    :type (or null symbol function)
                    :accessor change-callback)
   (state :initform :stopped
          :accessor animation-state
          :type keyword)
   (step-function :accessor step-function
                  :initform nil)
   (current-time :initform 0
                 :accessor current-time
                 :type integer)))

(defclass basic-animation (scenic-animation)
  ((target :initarg :target
           :initform (error "Provide the animation target")
           :accessor animation-target)
   (duration :initarg :duration
             :initform 1000
             :accessor animation-duration
             :type (or null integer))
   (speed :initarg :speed
          :type integer
          :initform 1
          :accessor animation-speed)))

(defclass variant-animation (basic-animation)
  ((easing :initarg :easing
           :type (or function symbol null)
           :accessor animation-easing
           :initform 'linear-tween
           :documentation "Easing curve interpolation function")))

(defclass animation-group (scenic-animation)
  ((animations :initarg :animations
               :accessor animations
               :type list
               :initform nil))
  (:documentation "A group of animations"))


;;-- * Animations API

(defun start-animation (animation-class &rest args)
  (start (apply #'make-instance animation-class args)))

(defgeneric prepare (animation)
  (:method ((animation scenic-animation)))
  (:documentation "Prepare the animation to start playing"))

(defgeneric start (animation)
  (:method :before ((animation scenic-animation))
           (when (start-callback animation)
             (funcall (start-callback animation) animation))
           (prepare animation))
  (:documentation "Start playing the animation"))

(defgeneric stop (animation)
  (:method :before ((animation scenic-animation))
           (when (stop-callback animation)
             (funcall (stop-callback animation) animation)))
  (:method ((animation scenic-animation))
    (setf (animation-state animation) :stopped)
    (when (step-function animation)
      (scenic::remove-step-function
       (step-function animation)
       scenic::*scene*)))
  (:documentation "Stop the animation"))

(defgeneric resume (animation)
  (:documentation "Continue playing the animation"))

(defgeneric pause (animation)
  (:documentation "Pauses an animation. Call resume to go on playing"))

(defgeneric play-step (animation)
  (:documentation "Plays the animation one step forward"))

(defmethod play-step ((animation scenic-animation))
  (set-current-time animation (current-time animation))
  (increment-current-time animation))

(defmethod set-current-time :before ((animation scenic-animation) current-time)
  (setf (current-time animation) current-time))

(defmethod animation-continue-p ((animation scenic-animation))
  (with-slots (current-time duration) animation
    (<= current-time duration)))

(defmethod set-current-time :after ((animation scenic-animation) current-time)
  (when (change-callback animation)
    (funcall (change-callback animation) animation)))

(defmethod increment-current-time ((animation scenic-animation))
  (incf (current-time animation)
        (/ (animation-duration animation)
           (/ (animation-duration animation)
              (animation-speed animation)))))

(defmethod start ((animation scenic-animation))
  (flet ((animate (&key scene target &allow-other-keys)
           (when (eql (animation-state animation) :running)
             (if (animation-continue-p animation)
                 (play-step animation)
                 (stop animation)))))
    (let ((step-function
           (scenic::step-function
            #'animate
            :target animation
            :times nil
            :while (lambda ()
                     (eql (animation-state animation) :running)))))
      (setf (step-function animation) step-function)
      (setf (animation-state animation) :running)
      (scenic::add-step-function step-function scenic::*scene*))))

;;-- ** Property animation

(defclass property-animation (variant-animation)
  ((property :initarg :property
             :type (or null function symbol)
             :initform nil
             :accessor animation-property
             :documentation "The property to update")
   (property-reader :initarg :property-reader
                    :type (or null symbol function)
                    :initform nil
                    :accessor property-reader)
   (property-writer :initarg :property-writer
                    :type (or null symbol function)
                    :initform nil
                    :accessor property-writer)
   (start-value :initarg :start-value
                :initform nil
                :accessor start-value
                :documentation "The start value of the property. If left undefined, then the current value of the property is used")
   (end-value :initarg :end-value
              :initform (error "Provide the end value")
              :accessor end-value
              :documentation "The end value of the property"))
  (:documentation "Updates a property from start-value to end-value"))

(defmethod next-value ((animation property-animation))
  (with-slots (current-time duration easing speed
                            start-value end-value) animation
    (or
     (and easing
          (funcall easing current-time start-value (- start-value end-value)
                   duration))
     (* (- end-value start-value) (/ duration current-time)))))

(defmethod set-current-time ((animation property-animation) current-time)
  (let ((next-value (next-value animation)))
    (if (property-writer animation)
        (funcall (property-writer animation) next-value (animation-target animation))
        (setf (access:access (animation-target animation)
                             (animation-property animation))
              next-value))))

;;-- ** Fade animation

(defclass fade-animation (variant-animation)
  ((from-opacity :initarg :from-opacity
                 :type integer
                 :initform 1
                 :accessor from-opacity)
   (to-opacity :initarg :to-opacity
               :type integer
               :initform 0
               :accessor to-opacity))
  (:documentation "Fade a glass surface"))

(defun next-opacity (animation)
  (with-slots (current-time duration easing
                            from-opacity to-opacity) animation
    (or
     (and easing
          (- from-opacity
             (funcall easing current-time 0 (- from-opacity to-opacity) duration)))
     (* (- to-opacity from-opacity) (/ duration current-time)))))

(defmethod set-current-time ((animation fade-animation) current-time)
  (let ((next-opacity (next-opacity animation)))
    (setf (scenic::opacity (animation-target animation)) next-opacity)
    (scenic::invalidate (animation-target animation))))

(defmethod prepare ((animation fade-animation))
  (setf (from-opacity animation)
        (or (from-opacity animation)
            (scenic::opacity (animation-target animation))))
  (format t "Opacity: ~A -> ~A~%" (from-opacity animation) (to-opacity animation)))

(defun fade (target &rest args)
  (assert (typep target 'scenic:glass) nil "Fade target has to be a GLASS")
  (start
   (apply #'make-instance 'fade-animation
          :target target
          args)))

(defun fadeout (target &rest args)
  (apply #'fade target :to-opacity 0 args))

(defun fadein (target &rest args)
  (apply #'fade target :from-opacity 0  :to-opacity 1 args))

;;-- ** Translation animation

(defclass translation-animation (variant-animation)
  ((from-x :initarg :from-x
           :type (or null integer)
           :initform nil
           :accessor from-x)
   (to-x :initarg :to-x
         :type (or null integer)
         :initform nil
         :accessor to-x)
   (from-y :initarg :from-y
           :type (or null integer)
           :initform nil
           :accessor from-y)
   (to-y :initarg :to-y
         :type (or null integer)
         :accessor to-y
         :initform nil)))

(defun next-x (animation)
  (with-slots (current-time duration easing
                            from-x to-x) animation
    (and from-x to-x
         (if (< from-x to-x)
             (or (and easing
                      (round (funcall easing current-time from-x to-x duration)))
                 (+ from-x current-time))
             (or (and easing
                      (round (funcall easing current-time to-x from-x duration)))
                 (- from-x current-time))))))


(defun next-y (animation)
  (with-slots (current-time duration easing
                            from-y to-y) animation
    (and from-y to-y
         (if (< from-y to-y)
             (or (and easing
                      (round (funcall easing current-time from-y to-y duration)))
                 (+ from-y current-time))
             (or (and easing
                      (round (funcall easing current-time to-y from-y duration)))
                 (- from-y current-time))))))

(defmethod y-direction ((animation translation-animation))
  (- (to-y animation) (from-y animation)))

(defmethod x-direction ((animation translation-animation))
  (- (to-x animation) (from-x animation)))

(defmethod animation-continue-p ((animation translation-animation))
  (let ((next-x (next-x animation))
        (next-y (next-y animation)))
    (or (and next-x
             (if (plusp (x-direction animation))
                 (< next-x (to-x animation))
                 (> next-x (to-x animation))))
        (and next-y
             (if (plusp (y-direction animation))
                 (< next-y (to-y animation))
                 (> next-y (to-y animation)))))))

(defmethod set-current-time ((animation translation-animation) current-time)
  (let ((x (next-x animation))
        (y (next-y animation)))
    (when x
      (setf (scenic::left (animation-target animation))
            x))
    (when y
      (setf (scenic::top (animation-target animation))
            y))
    (scenic::invalidate (scenic::parent (animation-target animation)))
    (scenic::mark-for-layout (animation-target animation))))

(defmethod prepare ((animation translation-animation))
  (setf (from-x animation) (scenic::left (animation-target animation))
        (from-y animation) (scenic::top (animation-target animation))))

(defun translate (widget &rest args &key to-x to-y speed duration easing &allow-other-keys)
  (check-type widget scenic:placer)
  (start (apply #'make-instance 'translation-animation
                :target widget
                :from-x (scenic::left widget)
                :from-y (scenic::top widget)
                args)))

;;-- ** Resize animation

(defclass resize-animation (variant-animation)
  ((from-width :initarg :from-width
               :type (or null integer)
               :initform nil
               :accessor from-width)
   (to-width :initarg :to-width
             :type (or null integer)
             :initform nil
             :accessor to-width)
   (from-height :initarg :from-height
                :type (or null integer)
                :initform nil
                :accessor from-height)
   (to-height :initarg :to-height
              :type (or null integer)
              :accessor to-height
              :initform nil)))

(defmethod prepare ((animation resize-animation))
  (setf (from-width animation) (round
                                (or (from-width animation)
                                    (scenic:layout-width (animation-target animation))))
        (from-height animation) (round
                                 (or (from-height animation)
                                     (scenic:layout-height (animation-target animation))))))

(defun next-width (animation)
  (with-slots (current-time duration easing
                            from-width to-width) animation
    (and from-width to-width
         (if (< from-height to-height)
             (or (and easing
                      (round (funcall easing current-time from-width to-width duration)))
                 (+ from-width current-time))
             (or (and easing
                      (abs (round (- from-width (funcall easing current-time to-width from-width duration)))))
                 (- from-width current-time))))))

(defun next-height (animation)
  (with-slots (current-time duration easing
                            from-height to-height) animation
    (and from-height to-height
         (if (< from-height to-height)
             (or (and easing
                      (round (funcall easing current-time from-height to-height duration)))
                 (+ from-height current-time))
             (or (and easing
                      (abs (round (- from-height (funcall easing current-time to-height from-height duration)))))
                 (- from-height current-time))))))

(defmethod set-current-time ((animation resize-animation) current-time)
  (let ((width (next-width animation))
        (height (next-height animation)))
    (when width
      (setf (scenic::width (animation-target animation)) height))
    (when height
      (setf (scenic::height (animation-target animation)) height))
    (scenic::invalidate (scenic::parent (animation-target animation)))
    (scenic::mark-for-layout (animation-target animation))))

(defun resize (widget &rest args &key to-width to-height speed duration easing &allow-other-keys)
  (check-type widget scenic:sizer)
  (start (apply #'make-instance 'resize-animation
                :target widget
                :from-width (round (scenic:layout-width (scenic:child widget)))
                :from-height (round (scenic:layout-height (scenic:child widget)))
                args)))

;;-- ** Rotate animation

(defun set-rotation-angle (angle object)
  (setf (scenic::angle object) angle)
  (scenic:invalidate object))

(defclass rotate-animation (property-animation)
  ()
  (:default-initargs
   :property-reader #'scenic::angle
    :property-writer #'set-rotation-angle))

(defun rotate (widget &rest args &key from-angle to-angle &allow-other-keys)
  (check-type widget scenic:rotator)
  (start (apply #'make-instance 'rotate-animation
                :target widget
                :start-value (or from-angle (scenic::angle widget))
                :end-value to-angle
                (alexandria:remove-from-plist args :from-angle :to-angle))))

;;--
;;-- ** Sequential animation group
;;--

(defclass sequential-animation-group (animation-group)
  ((current-animation :initform nil
                      :accessor current-animation)
   (loop :initarg :loop
      :initform nil
      :accessor animation-loop-p))
  (:documentation "Runs animations one after another"))

(defmethod prepare ((animation sequential-animation-group))
  (prepare (current-animation animation)))

(defmethod initialize-instance :after ((animation sequential-animation-group) &rest initargs)
  (setf (current-animation animation)
        (first (animations animation))))

(defmethod start :before ((animation sequential-animation-group))
  (setf (current-animation animation)
        (first (animations animation)))
  (prepare (current-animation animation)))

(defmethod start ((animation sequential-animation-group))
  (flet ((animate (&key scene target &allow-other-keys)
           (when (eql (animation-state animation) :running)
             (if (animation-continue-p animation)
                 (play-step animation)
                 (stop animation)))))
    (let ((step-function
           (scenic::step-function
            #'animate
            :target animation
            :times nil
            :while (lambda ()
                     (eql (animation-state animation) :running)))))
      (setf (step-function animation) step-function)
      (setf (animation-state animation) :running)
      (scenic::add-step-function step-function scenic::*scene*))))

(defmethod play-step ((animation sequential-animation-group))
  (play-step (current-animation animation))
  (when (not (animation-continue-p (current-animation animation)))
    (let ((next-animation (nth (1+ (position (current-animation animation)
                                             (animations animation)))
                               (animations animation))))
      (if next-animation
          (progn
            (setf (current-animation animation) next-animation)
            (prepare (current-animation animation)))
          ;; else
          (if (animation-loop-p animation)
              (setf (current-animation animation)
                    (first (animations animation)))
              (stop animation))))))

(defmethod animation-continue-p ((animation sequential-animation-group))
  (or (not (eql (car (last (animations animation)))
                (current-animation animation)))
      (animation-continue-p (current-animation animation))))

(defmethod set-current-time ((animation sequential-animation-group) time)
  (set-current-time (current-animation animation) time))

(defmethod increment-current-time ((animation sequential-animation-group))
  (when (not (animation-continue-p (current-animation animation)))
    (let ((next-animation (nth (1+ (position (current-animation animation)
                                             (animations animation)))
                               (animations animation))))
      (if next-animation
          (progn
            (setf (current-animation animation) next-animation)
            (prepare (current-animation animation)))
          ;; else
          (if (animation-loop-p animation)
              (setf (current-animation animation)
                    (first (animations animation)))
              (stop animation))))))

(defmacro in-sequence (args &body animations)
  `(start
    (make-instance 'sequential-animation-group
                   :animations (list ,@animations)
                   ,@args)))

;;--
;;-- ** Parallel animation
;;--


(defmacro in-parallel (args &body animations)
  `(start
    (make-instance 'parallel-animation-group
                   :animations (list ,@animations)
                   ,@args)))

(defclass parallel-animation-group (animation-group)
  ((current-animation :initform nil
                      :accessor current-animation))
  (:documentation "Runs animations in parallel"))

(defmethod prepare ((animation parallel-animation-group))
  (dolist (anim (animations animation))
    (prepare anim)))

(defmethod play-step ((animation parallel-animation-group))
  (dolist (anim (animations animation))
    (when (animation-continue-p anim)
      (play-step anim))))

(defmethod animation-continue-p ((animation parallel-animation-group))
  (every (lambda (anim)
           (animation-continue-p anim))
         (animations animation)))

;; Easing functions
;; http://gizma.com/easing/
;; http://upshots.org/actionscript/jsas-understanding-easing

(defun linear-tween (current-time start delta duration)
  (+ (/ (* delta current-time) duration) start))

(defun ease-in-quad (current-time start delta duration)
  (let ((current-time (/ current-time duration)))
    (+ (* current-time current-time delta) start)))

(defun ease-out-quad (current-time start delta duration)
  (assert (<= current-time duration))
  (let ((current-time (/ current-time duration)))
    (+ (* (- delta) current-time (- current-time 2)) start)))

(defun ease-in-out-quad (current-time start delta duration)
  (let ((current-time (/ current-time duration 2)))

    (if (< current-time 1)
        (+ start
           (* (/ delta 2)
              current-time
              current-time
              current-time
              current-time))
        (let ((current-time (- current-time 2)))
          (+
           (* (/ (- delta) 2)  (-
                                (* current-time
                                   current-time current-time
                                   current-time) 2)) start)))))

(defun ease-in-sin (current-time start delta duration)
  (+
   (* (- delta) (cos (* (/ current-time duration) (/ pi 2))))
   delta
   start))

(defun ease-out-sin (current-time start delta duration)
  (+ (* delta (sin (* (/ current-time duration) (/ pi 2)))) start))

(defun ease-in-out-sin (current-time start delta duration)
  (+ (* (/ (- delta) 2)
        (- (cos (* pi (/ current-time duration)))
           1)) start))
