; drawing a line with alpha
(cairo:move-to (layout-left object) (layout-top object))
(cairo:line-to (+ (layout-left object) (layout-width object))
                   (+ (layout-top object) (layout-height object)))
(cairo:set-source-rgba 0 0 0 0.5)
(cairo:set-line-width 1)
(cairo:stroke)
