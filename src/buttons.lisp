(in-package :scenic)

;;; CLICKABLE class.

(defwidget clickable (container1)
  ((click-state :accessor click-state
                :initarg :click-state
                :initform :neutral)))

(defun change-clickable-state (clickable new-state)
  (setf (click-state clickable) new-state))

(defmethod initialize-instance :after ((instance clickable) &rest initargs)
  (declare (ignore initargs))
  (let ((is-inside nil))
    (add-event-handler instance :mouse-button-down :bubble
                       (lambda (clickable event)
                         (declare (ignore))
                         (when (= 1 (mouse-button event))
                           (setf is-inside t)
                           (change-clickable-state clickable :half-click)
                           (capture-mouse instance))))
    (add-event-handler instance :mouse-enter :bubble
                       (lambda (clickable event)
                         (declare (ignore clickable event))
                         (setf is-inside t)
                         (change-clickable-state clickable :over)))
    (add-event-handler instance :mouse-leave :bubble
                       (lambda (clickable event)
                         (declare (ignore clickable event))
                         (setf is-inside nil)
                         (change-clickable-state clickable nil)))
    (add-event-handler instance :mouse-button-up :bubble
                       (lambda (clickable event)
                         (declare (ignore))
                         (when (= 1 (mouse-button event))
                           (release-mouse instance)
                           (when (and (eql :half-click (click-state clickable))
                                      is-inside)
                             (on-event clickable :click event nil))
                           (change-clickable-state clickable :over))))))

;;; BUTTON class.

(defwidget button (clickable)
  ())

(defmethod initialize-instance :after ((button button) &key on-click)
  (when on-click
    (add-event-handler button :click nil on-click)))

(defmethod (setf click-state) :after (value (instance button))
  ;;(layout instance
  ;;        (layout-left instance) (layout-top instance)
  ;;        (layout-width instance) (layout-height instance))
  (invalidate instance)
  (scenic::mark-for-layout instance)
  )

(defmethod measure ((object button) available-width available-height)
  (multiple-value-bind (width height)
      (measure (child object) available-width available-height)
    (set-measured object (+ 3 width) (+ 3 height))))

(defmethod layout ((object button) left top width height)
  "TODO: improve this implementation"
  (case (click-state object)
    (:half-click (layout (child object)
                         (+ 2 left) (+ 2 top)
                         (- width 3) (- height 3)))
    (t (layout (child object)
               (+ 1 left) (+ 1 top)
               (- width 3) (- height 3))))
  (set-layout object left top width height))

(defun draw-button (button pressed)
  (draw-button-raw (layout-left button)
                   (layout-top button)
                   (layout-width button)
                   (layout-height button)
                   pressed))

(defmethod paint ((object button))
  (draw-button object (eql (click-state object) :half-click)))

;;; STATEFUL class.

(defclass stateful ()
  ((state :accessor state :initarg :state :initform nil)))

;;; STATEFUL-BUTTON class.

(defwidget stateful-button (button stateful)
  ())

(defmethod initialize-instance :after ((instance stateful-button) &rest initargs)
  (declare (ignore initargs))
  (add-event-handler instance :click :bubble
                     (lambda (instance event)
                       (declare (ignore event))
                       (setf (state instance)
                             (not (state instance))))))

(defmethod (setf state) :after (new-value (object stateful-button))
  (on-event object :state-changed (make-instance 'event) nil)
  (invalidate object))

;;; TOGGLE-BUTTON class.

(defwidget toggle-button (stateful-button)
  ())

(defmethod paint ((object toggle-button))
  (draw-button object (or (state object)
                          (eql :half-click (click-state object)))))

;;; CHECKBOX class.

(defwidget checkbox (stateful-button)
  ())

(defmethod measure ((object checkbox) available-width available-height)
  (set-measured object 16 16))

(defmethod layout ((object checkbox) left top width height)
  (set-layout object left top 16 16))

(defmethod paint ((object checkbox))
  (scenic.draw:set-source-rgba 1 1 1 1)
  (scenic.draw:rectangle (layout-left object)
                       (layout-top object)
                       (layout-width object)
                       (layout-height object))
  (scenic.draw:fill-path)
  (scenic.draw:set-source-rgba 0 0 0 1)
  (scenic.draw:set-line-width 1)
  (scenic.draw:rectangle (+ 0.5 (layout-left object))
                       (+ 0.5 (layout-top object))
                       (layout-width object)
                       (layout-height object))
  (scenic.draw:stroke)
  (when (state object)
    (scenic.draw:move-to (+ 3 (layout-left object))
                       (+ 3 (layout-top object)))
    (scenic.draw:line-to (+ 13 (layout-left object))
                       (+ 13 (layout-top object)))
    (scenic.draw:stroke)
    (scenic.draw:move-to (+ 13 (layout-left object))
                       (+ 3 (layout-top object)))
    (scenic.draw:line-to (+ 3 (layout-left object))
                       (+ 13 (layout-top object)))
    (scenic.draw:stroke)))

;;; RADIO-BUTTON class.

(defwidget radio-button (stateful-button)
  ())

(defmethod measure ((object radio-button) available-width available-height)
  (set-measured object 16 16))

(defmethod layout ((object radio-button) left top width height)
  (set-layout object left top 16 16))

(defmethod paint ((object radio-button))
  (scenic.draw:set-source-rgba 1 1 1 1)
  (scenic.draw:rectangle (layout-left object)
                       (layout-top object)
                       (layout-width object)
                       (layout-height object))
  (scenic.draw:fill-path)
  (scenic.draw:set-source-rgba 0 0 0 1)
  (scenic.draw:set-line-width 1)
  (scenic.draw:arc (+ 8 (layout-left object)) (+ 8 (layout-top object))
                 6 0 (* 2 pi))
  (scenic.draw:stroke)
  (when (state object)
    (scenic.draw:arc (+ 8 (layout-left object)) (+ 8 (layout-top object))
                   3
                   0 (* 2 pi))
    (scenic.draw:fill-path)))

