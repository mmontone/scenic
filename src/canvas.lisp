(in-package :scenic)

(defclass drawing-canvas ()
  ())

(defclass cairo-canvas (drawing-canvas)
  ())

(defclass nvg-canvas (drawing-canvas)
  ())
