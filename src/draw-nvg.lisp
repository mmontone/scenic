(in-package :scenic.draw)

(defvar *nvg-context*)

(defun call-with-nvg (window function)
  (let ((*nvg-context* (nvg:make-context :stencil-strokes :antialias :debug)))
    (multiple-value-bind (width height)
        (sdl2:get-window-size window)
      (%nvg:begin-frame *nvg-context*
                        width height
                        1.0)
      (funcall function)
      (%nvg:end-frame *nvg-context*)
      (nvg:destroy-context *nvg-context*))))

(defmacro with-nvg ((window) &body body)
  `(call-with-nvg ,window (lambda () ,@body)))

(defmacro with-drawing-context (window &body body)
  `(call-with-nvg ,window (lambda () ,@body)))

(defun set-source-rgb (red green blue &optional (ctx *nvg-context*))
  (%nvg:bge-stroke-color ctx
                         (float red)
                         (float green)
                         (float blue)
                         (float 1))
  (%nvg:bge-fill-color ctx
                       (float red)
                       (float green)
                       (float blue)
                       (float 1)))

(defun new-path (&optional (ctx *nvg-context*))
  (%nvg:begin-path ctx))

(defun close-path (&optional (ctx *nvg-context*))
  (%nvg:close-path ctx))

(defun move-to (x y &optional (ctx *nvg-context*))
  (%nvg:move-to ctx (float x) (float y)))

(defun line-to (x y &optional (ctx *nvg-context*))
  (%nvg:line-to ctx x y))

(defun set-line-width (width &optional (ctx *nvg-context*))
  (%nvg:stroke-width ctx (float width)))

(defun fill-path (&optional (ctx *nvg-context*))
  (%nvg:fill ctx))

(defun stroke (&optional (ctx *nvg-context*))
  (%nvg:stroke ctx))

(defun rectangle (x y width height &optional (ctx *nvg-context*))
  (%nvg:rect ctx (coerce x 'single-float) (coerce y 'single-float) (float width) (float height)))

(defmethod set-color ((color cl-colors:rgb))
  (%nvg:bge-stroke-color *nvg-context*
                         (float (cl-colors:rgb-red color))
                         (float (cl-colors:rgb-green color))
                         (float (cl-colors:rgb-blue color))
                         (float 1))
  (%nvg:bge-fill-color *nvg-context*
                       (float (cl-colors:rgb-red color))
                         (float (cl-colors:rgb-green color))
                         (float (cl-colors:rgb-blue color))
                         (float 1)))

(defmethod set-color ((color alpha-color))
  (let ((c (alpha-color-color color)))
    (%nvg:bge-stroke-color *nvg-context*
     (float (cl-colors:rgb-red c))
     (float (cl-colors:rgb-green c))
     (float (cl-colors:rgb-blue c))
     (float (alpha-color-alpha color)))
    (%nvg:bge-fill-color *nvg-context*
     (float (cl-colors:rgb-red c))
     (float (cl-colors:rgb-green c))
     (float (cl-colors:rgb-blue c))
     (float (alpha-color-alpha color)))))

(defmethod set-color ((color list))
  (ecase (length color)
    (3 (let ((cl (mapcar 'float (append color (list 1)))))
         (apply #'%nvg:bge-stroke-color *nvg-context* cl)
         (apply #'%nvg:bge-fill-color *nvg-context* cl)))
    (4 (let ((cl (mapcar 'float color)))
         (apply #'%nvg:bge-stroke-color *nvg-context* cl)
         (apply #'%nvg:bge-fill-color *nvg-context* cl)))))
