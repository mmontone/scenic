(in-package :scenic-helpers)

(defun background (color child &rest initargs)
  (apply #'make-instance 'background
         :fill-color color
         :child child
         initargs))

(defun border (color width child &rest initargs)
  (apply #'make-instance 'border
         :stroke-color color
         :stroke-width width
         :child child
         initargs))

(defun scene (width height widget &rest initargs)
  (apply #'make-instance 'scene
         :width width
         :height height
         :widget widget
         initargs))

(defun placeholder (width height)
  (make-instance 'placeholder
                 :width width
                 :height height))

(defun padding (left top right bottom child)
  (make-instance 'padding
                 :padding-left left
                 :padding-top top
                 :padding-right right
                 :padding-bottom bottom
                 :child child))

(defun uniform-padding (padding child &rest initargs)
  (apply #'make-instance 'padding
         :padding-left padding
         :padding-top padding
         :padding-right padding
         :padding-bottom padding
         :child child
         initargs))

(defun stack (&rest children)
  (make-instance 'stack
                 :children children))

(defun filler ()
  (make-instance 'filler))

(defun label (text &key
              (face "Helvetica") (size 12) (color '(0 0 0))
              (slant :normal) (weight :normal) name)
  (make-instance 'label
                 :text text
                 :font-face face
                 :font-size size
                 :font-color color
                 :font-slant slant
                 :font-weight weight
                 :name name))

(defun button (child &rest args)
  (apply #'make-instance 'button
         :child child
         args))

(defun button-text (text)
  (button (uniform-padding 8 (aligner (label text :size 15)))))

(defun toggle (text)
  (make-instance 'toggle-button
                 :state nil
                 :child (uniform-padding 8 (label text :size 12))))

(defun horizontal-slider (min max page)
  (make-instance 'slider
                 :orientation :horizontal
                 :min-value min
                 :max-value max
                 :page-size page
                 :current-min-position min))

(defun vertical-slider (min max page)
  (make-instance 'slider
                 :orientation :vertical
                 :min-value min
                 :max-value max
                 :page-size page
                 :current-min-position min))

(defun sizer (child &key min-width min-height max-width max-height height width)
  (make-instance 'sizer
                 :child child
                 :min-width (or min-width width)
                 :min-height (or min-height height)
                 :max-width (or max-width width)
                 :max-height (or max-height height)))

(defun arrow (direction)
  (make-instance 'arrow :direction direction))

(defun horizontal-scrollbar (min max page &rest initargs)
  (apply #'make-instance 'scrollbar
         :orientation :horizontal
         :min-value min
         :max-value max
         :page-size page
         :current-min-position min
         initargs))

(defun vertical-scrollbar (min max page)
  (make-instance 'scrollbar
                 :orientation :vertical
                 :min-value min
                 :max-value max
                 :page-size page
                 :current-min-position min))

(defun image (image-path)
  (make-instance 'image
                 :image-surface (get-image image-path)))

(defun grid (column-layout-options row-layout-options children-descriptions)
  (make-instance 'grid
                 :column-layout-options column-layout-options
                 :row-layout-options row-layout-options
                 :children-descriptions children-descriptions))

(defun vertical-pack (space layout-options children)
  (if (zerop space)
      (grid '(:auto) layout-options
            `((:column ,@(mapcar (lambda (child) (list :cell child))
                                 children))))
      (grid '(:auto)
            (intersperse layout-options (list space :px))
            `((:column ,@(mapcar (lambda (child) (list :cell child))
                                 (intersperse children
                                              (placeholder 0 space))))))))

(defun vertical-pack* (space layout-options &rest children)
  (vertical-pack space layout-options children))

(export 'vertical-pack*)

(defun horizontal-pack (space layout-options children)
  (if (zerop space)
      (grid layout-options '(:auto)
            `((:row ,@(mapcar (lambda (child) (list :cell child))
                              children))))
      (grid (intersperse layout-options (list space :px))
            '(:auto)
            `((:row ,@(mapcar (lambda (child) (list :cell child))
                              (intersperse children
                                           (placeholder space 0))))))))

(defun horizontal-pack* (space layout-options &rest children)
  (horizontal-pack space layout-options children))

(export 'horizontal-pack*)

(defun aligner (child &key (horizontal :center) (vertical :center))
  (make-instance 'aligner
                 :child child
                 :horizontal horizontal
                 :vertical vertical))

(defun clipper (child)
  (make-instance 'clipper
                 :child child))

(defun O-clipper (child)
  (make-instance 'circular-clipper
                 :child child))

(defun glass (opacity child)
  (make-instance 'glass
                 :opacity opacity
                 :child child))

(defun henchman (children-locations children)
  (make-instance 'henchman
                 :children children
                 :children-locations children-locations))

(defun scroll-view (child &key (inside-width (expt 10 6)) (inside-height (expt 10 6)))
  (make-instance 'scroll-view
                 :child child
                 :inside-width inside-width
                 :inside-height inside-height))

(defun scroll-view-auto (child &key
                         (inside-width (expt 10 6))
                         (inside-height (expt 10 6))
                         (always-horizontal-scrollbar t)
                         (always-vertical-scrollbar t))
  (let* ((scroll-view (scroll-view child
                                   :inside-width inside-width
                                   :inside-height inside-height))
         (hscroll (horizontal-scrollbar 0 100 10))
         (vscroll (vertical-scrollbar 0 100 10))
         (grid (grid '(:auto (1 :auto))
                     '(:auto (1 :auto))
                     `((:row (:cell ,scroll-view)
                             (:cell ,vscroll))
                       (:row (:cell ,hscroll))))))
    (add-event-handler scroll-view :scroll-view-measured :bubble
                       (lambda (o evt)
                         (declare (ignore o))
                         (labels ((set-hscroll-visibility ()
                                    (when (measured-width grid)
                                      (setf (visible hscroll)
                                            (or always-horizontal-scrollbar
                                                (< (- (measured-width grid)
                                                      (if (visible vscroll) 19 0))
                                                   (inner-width evt))))))
                                  (set-vscroll-visibility ()
                                    (when (measured-height grid)
                                      (setf (visible vscroll)
                                            (or always-vertical-scrollbar
                                                (< (- (measured-height grid)
                                                      (if (visible hscroll) 19 0))
                                                   (inner-height evt)))))))
                           (setf (page-size hscroll) (outer-width evt))
                           (setf (max-value hscroll) (inner-width evt))
                           (setf (page-size vscroll) (outer-height evt))
                           (setf (max-value vscroll) (inner-height evt))
                           (set-hscroll-visibility)
                           (set-vscroll-visibility)
                           (set-hscroll-visibility))))
    (add-event-handler hscroll :position-changed nil
                       (lambda (o evt)
                         (declare (ignore o evt))
                         (setf (horizontal-offset scroll-view)
                               (current-min-position hscroll))
                         (invalidate scroll-view)))
    (add-event-handler vscroll :position-changed nil
                       (lambda (o evt)
                         (declare (ignore o evt))
                         (setf (vertical-offset scroll-view)
                               (current-min-position vscroll))
                         (invalidate scroll-view)))
    (add-event-handler scroll-view :scroll-view-offset-changed nil
                       (lambda (o evt)
                         (declare (ignore o evt))
                         (setf (current-min-position vscroll)
                               (vertical-offset scroll-view))
                         (setf (current-min-position hscroll)
                               (horizontal-offset scroll-view))))
    (values grid scroll-view)))

(defun textbox (text cursor-position
                &key (selection-start 0) (caret-color (list 0.0 0.0 0.0))
                (selection-color (list 0.3 0.3 1.0)))
  (make-instance 'textbox
                 :text text
                 :cursor-position cursor-position
                 :selection-start selection-start
                 :caret-color caret-color
                 :selection-color selection-color
                 :background-color (list 1.0 1.0 1.0)
                 :font-face "Courier"
                 :font-size 14))

(defun wrap-stateful-button (stateful-button text)
  (let (label combination)
    (setf combination
          (horizontal-pack
           2 '(:auto :auto)
           (list (aligner stateful-button)
                 (setf label
                       (make-instance 'clickable
                                      :child (uniform-padding
                                              2
                                              (aligner (label text :size 18))))))))
    (add-event-handler label :click nil
                       (lambda (o e)
                         (declare (ignore o e))
                         (setf (state stateful-button)
                               (not (state stateful-button)))))
    (values combination stateful-button)))

(defun checkbox (text &key state)
  (wrap-stateful-button (make-instance 'checkbox :state state)
                        text))

(defun radio-button (text &key state)
  (wrap-stateful-button (make-instance 'radio-button :state state)
                        text))

(defun group-stateful-buttons (default buttons)
  (labels ((all-unchecked ()
             (every (lambda (btn) (arrows:-> btn state not)) buttons)))
    (dolist (b buttons)
      (scenic:add-event-handler b :state-changed nil
                                (lambda (obj e)
                                  (declare (ignore e))
                                  (when (state obj)
                                    (dolist (b buttons)
                                      (unless (eq obj b)
                                        (setf (state b) nil))))
                                  (when (all-unchecked)
                                    (setf (state obj) t)))))
    (setf (state default) t)))

(defun simple-horizontal-pack (space children)
  (make-instance 'pack
                 :space-between-cells space
                 :orientation :horizontal
                 :children children))

(defun simple-vertical-pack (space children)
  (make-instance 'pack
                 :space-between-cells space
                 :orientation :vertical
                 :children children))

(defmacro pack ((&rest args &key (space-between-cells 0) (orientation :horizontal) &allow-other-keys) &rest children)
  `(make-instance 'pack
                  :children (list ,@children)
                  ,@args))

;;-- ** Bindings

(defun collect-replace-bindings (form)
  (let (bindings)
    (let ((new-form (%collect-replace-bindings
                     form
                     (lambda (binding) (push binding bindings)))))
      (values new-form bindings))))

(defun %collect-replace-bindings (form collect-binding)
  (if (atom form)
      form
      (let ((bind-pos (position :bind form)))
        (if bind-pos
            (let ((binding (nth (1+ bind-pos) form)))
              (funcall collect-binding binding)
              (let ((new-form
                     (append (subseq form 0 bind-pos)
                             (subseq form (+ bind-pos 2)))))
                `(setf ,binding
                       ,(loop for part in new-form
                           collect
                             (%collect-replace-bindings part collect-binding)))))
            (loop for part in form
               collect
                 (%collect-replace-bindings part collect-binding))))))

(collect-replace-bindings '2)
(collect-replace-bindings 'nil)
(collect-replace-bindings '(let ((x 22))
                            (print x)))
(collect-replace-bindings '(let ((x 22))
                            (button :bind my-button lala)))

(defmacro with-bindings (&body body)
  ;; Walkes the code and creates bindings for :bind arguments
  ;; Example:
  ;; (with-bindings
  ;;   (button :bind my-button)
  ;;   (return my-button))
  (multiple-value-bind (new-body bindings)
      (collect-replace-bindings body)
    `(let ,bindings
       ,@new-body)))

;;-- *** Examples

#+scenic-test(with-bindings
               (button :bind my-button (label "Hello"))

               (run-component my-button :title "Button"))

;;-- ** Apply

(defun apply* (f x &rest args)
  (if (listp x)
      (mapcar (lambda (xi)
                (apply f xi args))
              x)
      (apply f x args)))

(defun apply*-flipped (x f &rest args)
  (if (listp x)
      (mapcar (lambda (xi)
                (apply f xi args))
              x)
      (apply f x args)))

(defmacro ->* (obj &rest forms)
  "Arrow-like applying macro"
  (let ((fresh-arg (gensym)))
    (let ((trans-forms (mapcar (lambda (form)
                                 `(apply*-flipped
                                   (lambda (,fresh-arg)
                                     ,(arrows::insert-first fresh-arg form))))
                               forms)))
      (reduce (arrows::simple-inserter #'arrows::insert-first)
              trans-forms
              :initial-value obj))))
