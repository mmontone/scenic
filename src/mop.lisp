(in-package :scenic)

(defclass widget (eventful)
  ((widget-class :initarg widget-class :initform nil :accessor widget-class)
   (measured-width :initarg measured-width :initform nil :accessor measured-width)
   (measured-height :initarg measured-height :initform nil :accessor measured-height)
   (layout-left :initarg layout-left :initform nil :accessor layout-left)
   (layout-top :initarg layout-top :initform nil :accessor layout-top)
   (layout-width :initarg layout-width :initform nil :accessor layout-width)
   (layout-height :initarg layout-height :initform nil :accessor layout-height)
   (parent :initarg parent :initform nil :accessor parent)
   (name :initarg :name :initform nil :accessor name)
   (auto-name :initarg auto-name :initform nil :accessor auto-name)
   (paint-order-number :initarg paint-order-number :initform -1 :accessor paint-order-number)
   (affected-rect :initarg affected-rect :initform nil :accessor affected-rect)
   (visible :initarg visible :initform t :accessor visible)
   (styled :initarg styled :initform nil :accessor styled)
   (%builtp :accessor %builtp
            :initform nil)
   (%slot-change-updates :accessor %slot-change-updates
                         :initform t)))

(defclass widget-class (standard-class)
  ((%rebuild-on-change :writer (setf %rebuild-on-change)
                       :initarg :rebuild-on-change
                       :initform nil
                       ;; Hack. REmove.
                       ;:type boolean
                       :documentation "If T, then the widget is rebuilt when a change occurs")))

;; Hack. Remove
;; Problem is :rebuild-on-change value is given by mop as a list of boolean and not a boolean
(defmethod %rebuild-on-change ((class widget-class))
  (let ((slot-value (slot-value class '%rebuild-on-change)))
    (if (listp slot-value)
        (first slot-value)
        slot-value)))

(defmethod closer-mop:validate-superclass
    ((class widget-class)
     (superclass standard-class))
  t)

(defclass widget-slot-definition ()
  ((child :initarg :child
          :accessor child-slot-p
          :initform nil
          :type boolean
          :documentation "Whether this slot holds a child widget")
   (state :initarg :state
          :accessor state-slot-p
          :initform nil
          :type boolean
          :documentation "Whether this slot is part of the widget's state")
   (prop :initarg :prop
         :accessor prop-slot-p
         :initform nil
         :type boolean
         :documentation "Whether this slot holds a widget property")
   (value-model :initarg :vmodel
                :accessor value-model-slot-p
                :initform nil
                :type boolean
                :documentation "Whether this slot can hold a value model")
   (test :initarg :test
         :accessor slot-test
         :initform 'eql
         :documentation "Test function for testing equality on value models")         
   (on-change :initarg :on-change
              :accessor slot-on-change
              :initform nil
              :documentation "What to do when the slot changes its value. A list of :invalidate :layout :rebuild")))

(defmethod child-slot-p (slot)
  nil)

(defmethod state-slot-p (slot)
  nil)

(defmethod prop-slot-p (slot)
  nil)

(defclass widget-direct-slot-definition
    (widget-slot-definition closer-mop:standard-direct-slot-definition)
  ())

(defclass widget-effective-slot-definition
    (widget-slot-definition closer-mop:standard-effective-slot-definition)
  ((computed-readers
    :initform nil
    :type list
    :accessor computed-readers-of
    :initarg :computed-readers)
   (computed-writers
    :initform nil
    :type list
    :accessor computed-writers-of
    :initarg :computed-writers)))

(defmethod closer-mop:direct-slot-definition-class
    ((class widget-class)
     &rest initargs)
  (declare (ignore initargs))
  (find-class 'widget-direct-slot-definition))

(defmethod closer-mop:effective-slot-definition-class
    ((class widget-class)
     &rest initargs)
  (declare (ignore initargs))
  (find-class 'widget-effective-slot-definition))

(defmethod closer-mop:compute-effective-slot-definition ((class widget-class) name direct-slots)
  (let ((effective-slot (call-next-method)))
    (when (typep effective-slot 'widget-slot-definition) 
      (setf (child-slot-p effective-slot)
            (some #'child-slot-p direct-slots))
      (setf (state-slot-p effective-slot)
            (some #'state-slot-p direct-slots))
      (setf (prop-slot-p effective-slot)
            (some #'prop-slot-p direct-slots))
      (setf (value-model-slot-p effective-slot)
            (some (lambda (slot)
                    (and (typep slot 'widget-slot-definition)
                         (value-model-slot-p slot)))
                  direct-slots))

      (dolist (slot direct-slots)
        (when (typep slot 'widget-slot-definition)
          (setf (slot-on-change effective-slot) 
                (slot-on-change slot)
                (slot-test effective-slot)
                (slot-test slot))
          (return)))

      ;; Collect and copy the readers and writers to the effective-slot, so we can access it
      ;; later when generating custom accessors.

      (setf (computed-readers-of effective-slot)
            (remove-duplicates (loop for direct-slot :in direct-slots
                                       appending (c2mop:slot-definition-readers direct-slot))
                               :test #'equal))
      (setf (computed-writers-of effective-slot)
            (remove-duplicates (loop for direct-slot :in direct-slots
                                  appending (c2mop:slot-definition-writers direct-slot))
                               :test #'equal))
      )
    
    effective-slot))

#+nil(defmethod shared-initialize :around ((class widget-class) slot-names &rest args &key direct-superclasses)
  "Ensures we inherit from widget."
  (let* ((widget-class (find-class 'widget))
         (not-already-widget
          (loop for superclass in direct-superclasses
             never (subtypep superclass widget-class))))
    (if (and (not (eq class widget-class)) not-already-widget)
        (apply #'call-next-method class slot-names
               :direct-superclasses (append direct-superclasses (list widget-class))
               args)
        (call-next-method))))

(defmethod initialize-instance :around ((class widget-class) &rest args &key direct-superclasses)
  "Ensures we inherit from widget."
  (let* ((widget-class (find-class 'widget))
         (not-already-widget
          (loop for superclass in direct-superclasses
             never (subtypep superclass widget-class))))
    (if (and (not (eq class widget-class)) not-already-widget)
        (apply #'call-next-method class
               :direct-superclasses (append direct-superclasses (list widget-class))
               args)
        (call-next-method))))

(defmethod reinitialize-instance :around ((class widget-class) &rest args &key direct-superclasses)
  "Ensures we inherit from widget."
  (let* ((widget-class (find-class 'widget))
         (not-already-widget
          (loop for superclass in direct-superclasses
             never (subtypep superclass widget-class))))
    (if (and (not (eq class widget-class)) not-already-widget)
        (apply #'call-next-method class
               :direct-superclasses (append direct-superclasses (list widget-class))
               args)
        (call-next-method))))

(defmethod initialize-instance :around ((comp widget) &rest initargs)
  "Disable slot updates on initialization"
  (setf (%slot-change-updates comp) nil)
  (call-next-method)
  (setf (%slot-change-updates comp) t))

(defmethod child-slots ((obj widget))
  (child-slots (class-of obj)))

(defmethod child-slots ((class widget-class))
  (remove-if-not #'child-slot-p (closer-mop:class-slots class)))

(defmethod state-slots ((obj widget))
  (state-slots (class-of obj)))

(defmethod state-slots ((class widget-class))
  (remove-if-not #'state-slot-p (closer-mop:class-slots class)))

(defmethod prop-slots ((obj widget))
  (prop-slots (class-of obj)))

(defmethod prop-slots ((class widget-class))
  (remove-if-not #'prop-slot-p (closer-mop:class-slots class)))

(defmethod (setf closer-mop:slot-value-using-class) :after
    (new-value
     (class widget-class)
     (object widget)
     (slot widget-slot-definition))
  (when (%slot-change-updates object)
    (when (%rebuild-on-change class)
      (setf (%builtp object) nil))
    (when (member :invalidate (slot-on-change slot))
      (scenic::invalidate object))
    (when (member :layout (slot-on-change slot))
      (scenic::mark-for-layout object))
    (when (member :rebuild (slot-on-change slot))
      (setf (%builtp object) nil)
      (scenic::mark-for-layout object)
      (scenic::invalidate object))
    (when (child-slot-p slot)
      (when (typep new-value 'scenic:widget)
        (setf (scenic::parent new-value) object)))
    (when (state-slot-p slot)
      (scenic::invalidate object))))

(defgeneric build (widget)
  (:method ((widget widget))
    "Do nothing by default, but return TRUE, so that PAINT-ORDER-WALK doesn't stop"
    t)
  (:method :around ((widget widget))
           (when (not (%builtp widget))
             (call-next-method)))
  (:method :after ((widget widget))
           (setf (%builtp widget) t)))


(defmethod rebuild ((widget widget) &key (now nil))
  ;; remove-all-children: Should this be optional? A metaclass option?
  (remove-all-children widget)
  (setf (%builtp widget) nil)
  (when now
    (build widget)))

;; Default implementations of measure and layout??
;; (defmethod scenic:measure ((widget widget) available-width available-height)
;;   (multiple-value-bind (width height)
;;       (scenic:measure (%child tab-view) available-width available-height)
;;     (set-measured tab-view width height)))

;; (defmethod scenic::layout ((object widget) left top width height)
;;   (scenic::layout (%child object)
;;           left
;;           top
;;           width
;;           height)
;;   (scenic::set-layout object left top width height))

(defmethod scenic:measure ((widget widget) available-width available-height)
  (error "Implement scenic:measure for ~A" widget))

(defmethod scenic:layout ((widget widget) left top width height)
  (error "Implement scenic:layout for ~A" widget))                           

(defmethod remove-all-children ((comp widget))
  (dolist (slot (child-slots (class-of comp)))
    (let ((slot-name (closer-mop:slot-definition-name slot)))
      (setf (slot-value comp slot-name) nil))))

(defmethod widget-children ((comp widget))
  (remove nil
          (mapcar (lambda (slot)
                    (let ((slot-name (closer-mop:slot-definition-name slot)))
                      (slot-value comp slot-name)))
                  (child-slots (class-of comp)))))

(defmethod scenic::paint-order-walk ((object widget) callback &key (after-callback nil))
  (when (funcall callback object)
    (dolist (child (widget-children object))
      (when child
        (scenic::paint-order-walk child callback :after-callback after-callback))))
  (when after-callback
    (funcall after-callback object)))

(defmethod scenic::paint ((comp widget))
  )

(defmethod scenic::fill-props progn ((comp widget) values)
  (dolist (slot (prop-slots comp))
    (let ((prop (alexandria:make-keyword (closer-mop:slot-definition-name slot))))
    (scenic::apply-style-property
     prop
     (access:access values prop)
     comp))))

(defmacro defwidget (name superclasses slots &rest options)
  `(defclass ,name ,(remove 'widget superclasses)
     ,slots
     ,@(cons (list :metaclass 'widget-class) options)))      

#+test
(defwidget my-widget ()
  ((active-item :initform nil
                :accessor active-item
                :child t
                :on-change (:invalidate :layout)))
  (:rebuild-on-change t))

(defun visible-bounding-box (widget)
  "The visible area of a widget. The visible rect is set in AFFECTED-RECT slot, after the widget tree is walked."
  (with-slots (affected-rect) widget
    (list (left affected-rect)
          (top affected-rect)
          (right affected-rect)
          (bottom affected-rect))))

;; Accessors with value model support

(defmethod c2mop:finalize-inheritance :after ((class widget-class))
  (ensure-vm-accessor-wrappers class))

(defun ensure-vm-accessor-wrappers (class)
  (dolist (effective-slot (c2mop:class-slots class))
    (when (and (typep effective-slot 'widget-slot-definition)
               (value-model-slot-p effective-slot))
      (dolist (reader (computed-readers-of effective-slot))
        (ensure-vm-reader-for-slot effective-slot reader class))
      (dolist (writer (computed-writers-of effective-slot))
        (ensure-vm-writer-for-slot effective-slot writer class)))))

(defun ensure-vm-reader-for-slot (slot reader class)
  (let* ((gf (c2mop:ensure-generic-function reader :lambda-list '(object)))
         (specializers (list class))
         (current-method (find-method gf '() specializers nil))
         (slot-name (c2mop:slot-definition-name slot)))
    (warn "Ensuring VM reader ~A for class ~A" reader class)
    (c2mop:ensure-method
     gf
     `(lambda (object)
        (with-slots (,slot-name) object
          (if (scenic-utils::value-model-p ,slot-name)
              (scenic-utils::value ,slot-name)
              ,slot-name)))
     :specializers specializers)))

(defun ensure-vm-writer-for-slot (slot writer class)
  (let* ((gf (c2mop:ensure-generic-function writer :lambda-list '(new-value object)))
         (specializers (list (find-class t) class))
         (current-method (find-method gf '() specializers nil))
         (slot-name (c2mop:slot-definition-name slot))
         (slot-test (slot-test slot)))
    (warn "Ensuring VM writer ~A for class ~A" writer class)
    (c2mop:ensure-method
     gf
     `(lambda (new-value object)
        (with-slots (,slot-name) object
          (if (scenic-utils::value-model-p ,slot-name)
              (when (not (,slot-test ,slot-name new-value))
                (setf (scenic-utils::value ,slot-name) new-value)
                ;; Hack. Trigger change
                (setf ,slot-name ,slot-name))
              (setf ,slot-name new-value))))
     :specializers specializers)))

(defgeneric children (obj))

(defmethod children ((widget widget))
  (loop for slot in (child-slots widget)
     collect (slot-value widget (c2mop:slot-definition-name slot))))
