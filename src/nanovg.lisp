;;(ql:quickload :bodge-blobs)
(ql:quickload :cffi)
(push #p"/usr/local/lib/" cffi:*foreign-library-directories*)
(ql:quickload :bodge-nanovg)
(ql:quickload :sdl2kit)

(defpackage sdl2nvg
  (:use :cl))

(in-package :sdl2nvg)

(defvar *nvg-context*)

(defclass nvg-window (sdl2.kit:gl-window)
  ((nvg-context :accessor nvg-context)))

(defmethod initialize-instance :after ((window nvg-window) &rest initargs)
  (setf (sdl2.kit:idle-render window) t))

(kit.sdl2:define-start-function nvg-test (&key (w 1000) (h 600))
  (sdl2:gl-set-attr :context-major-version 3)
  (sdl2:gl-set-attr :context-minor-version 3)
  (sdl2:gl-set-attr :context-profile-mask 1)
  (sdl2:gl-set-attr :stencil-size 8)
  (make-instance 'nvg-window :w w :h h))

(defun call-with-nvg (window function)
  (let ((*nvg-context* (nvg:make-context :stencil-strokes :antialias :debug)))
    (%nvg:begin-frame *nvg-context*
                      (kit.sdl2:window-width window)
                      (kit.sdl2:window-height window)
                      1.0)
    (funcall function)
    (%nvg:end-frame *nvg-context*)
    (nvg:destroy-context *nvg-context*)))

(defmacro with-nvg ((window) &body body)
  `(call-with-nvg ,window (lambda () ,@body)))

(defmethod kit.sdl2:render ((window nvg-window))
  (with-nvg (window)
    (gl:clear-color 0.3 0.3 0.3 1.0)
    (gl:clear :color-buffer-bit
              :depth-buffer-bit
              :stencil-buffer-bit)
    (%nvg:begin-path *nvg-context*)
    (%nvg:move-to *nvg-context* 100.0 100.0)
    (%nvg:line-to *nvg-context* 150.0 150.0)
    (%nvg:close-path *nvg-context*)
    (%nvg:bge-stroke-color *nvg-context* 1.0 1.0 1.0 1.0)    
    (%nvg:stroke-width *nvg-context* 5.0)
    (%nvg:stroke *nvg-context*)

    (%nvg:begin-path *nvg-context*)
    (%nvg:rect *nvg-context* 100.0 100.0 150.0 150.0)
    (%nvg:close-path *nvg-context*)
    (%nvg:bge-stroke-color *nvg-context* 1.0 1.0 1.0 1.0)    
    (%nvg:stroke-width *nvg-context* 5.0)
    (%nvg:stroke *nvg-context*)
    )
  (sdl2:delay 10))

(defun make-rect (&key x y width height x-direction y-direction)
  (let ((x (or x (float (random 500))))
        (y (or y (float (random 500))))
        (width (or width (float 50)))
        (height (or width (float 50)))
        (x-direction (or x-direction (nth (random 2) (list :left :right))))
        (y-direction (or y-direction (nth (random 2) (list :down :up)))))
    (list :x x :y y
          :width width
          :height height
          :x-direction x-direction
          :y-direction y-direction)))

(let ((rects
       (loop repeat 30
          collect (make-rect))))
  (defmethod kit.sdl2:render ((window nvg-window))
    (flet ((update-rect-pos (rect)
             (when (<= (getf rect :x) 0)
               (setf (getf rect :x-direction) :right))
             (when (>= (getf rect :x) 500)
               (setf (getf rect :x-direction) :left))
             (ecase (getf rect :x-direction)
               (:right (incf (getf rect :x) 5))
               (:left (decf (getf rect :x) 5)))
             (when (<= (getf rect :y) 0)
               (setf (getf rect :y-direction) :down))
             (when (>= (getf rect :y) 500)
               (setf (getf rect :y-direction) :up))
             (ecase (getf rect :y-direction)
               (:up (decf (getf rect :y) 5))
               (:down (incf (getf rect :y) 5)))))
      (flet ((render-rect (rect)
               (%nvg:begin-path *nvg-context*)
               (%nvg:rect *nvg-context* (getf rect :x)
                          (getf rect :y)
                          (getf rect :width)
                          (getf rect :height))
               (%nvg:close-path *nvg-context*)
               (%nvg:bge-stroke-color *nvg-context* 1.0 1.0 1.0 1.0)    
               (%nvg:stroke-width *nvg-context* 5.0)
               (%nvg:stroke *nvg-context*)))
        (with-nvg (window)
          (gl:clear-color 0.3 0.3 0.3 1.0)
          (gl:clear :color-buffer-bit
                    :depth-buffer-bit
                    :stencil-buffer-bit)
          (dolist (rect rects)
            (render-rect rect)
            (update-rect-pos rect)
            ))))
    (sdl2:delay 10)
    ))
