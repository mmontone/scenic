;;-- * NUI Button
(in-package :scenic.nui)

;;-- ** Test

;;-- * Definition

(defwidget nui-button (nui-widget clickable)
  ((button-type :initarg :type
                :accessor button-type
                :initform :secondary
                :type (member :secondary :primary))
   (button-style :initarg :style
                 :accessor button-style
                 :initform :normal
                 :type (member :normal :flat))
   (button-shadow :initarg :shadow
                  :initform nil
                  :accessor button-shadow
                  :documentation "Button shadow")))

;;-- * Initialization

(defmethod (setf scenic::click-state) :after (value (instance nui-button))
  ;; We need cairo context to measure text
  #+nil(scenic::layout instance
                       (scenic:layout-left instance) (scenic:layout-top instance)
                       (scenic:layout-width instance) (scenic:layout-height instance))

  (scenic:invalidate instance))

;;-- * Drawing

;;-- This is the generic function used to draw buttons

(defun draw-nui-button (button)
  "Generic function to draw buttons"
  (scenic.style:with-widget-styles button
    (let* ((left (layout-left button))
           (top (layout-top button))
           (width (layout-width button))
           (height (layout-height button))
           (pressed (eql (scenic::click-state button) :half-click))
           (over (eql (scenic::click-state button) :over))
           (color (mk-color
                   (cond
                    (pressed (style-val :color :status :pushed))
                    (over (or (style-val :color :status :hover)
                              (scenic.draw:brighten (style-val :color) 0.04)))
                    (t (style-val :color)))
                   :width width :height height))
           (border (cond
                     (pressed (style-val :border :status :pushed))
                     (over (style-val :border :status :hover))
                     (t (style-val :border))))
           (border-width (or (not border)
                             (scenic.style::border-width border)))
           (border-color (or (not border)
                             (scenic.style::border-color border)))
           (corner-radius (style-val :corner-radius)))

      (when (and (eql (button-style button) :flat) (not over))
        (return-from draw-nui-button))

      (scenic-utils::draw-rect
       left top width height
       :stroke-width border-width
       :stroke-color border-color
       :fill-color color
       :corner-radius corner-radius))))

(defmethod renderer-measure (renderer (object nui-button) available-width available-height)
  (multiple-value-bind (width height)
      (measure (child object) available-width available-height)
    (set-measured object (+ 3 width) (+ 3 height))))

(defmethod renderer-layout (renderer (object nui-button) left top width height)
  "TODO: improve this implementation"
  (case (scenic::click-state object)
    (:half-click (layout (child object)
                         (+ 2 left) (+ 2 top)
                         (- width 3) (- height 3)))
    (t (layout (child object)
                      (+ 1 left) (+ 1 top)
                      (- width 3) (- height 3))))
  (set-layout object left top width height))

(defmethod renderer-paint ((renderer scenic.renderer:gtk-renderer) (object nui-button))
  (draw-nui-button object))

(defmethod renderer-paint ((renderer scenic.renderer::rect-renderer) (object nui-button))
  (draw-nui-button object))

(defmethod renderer-paint ((renderer scenic.renderer:wnd7-renderer) (button nui-button))
  (scenic.style:with-widget-styles button
    (let* ((left (layout-left button))
           (top (layout-top button))
           (width (layout-width button))
           (height (layout-height button))
           (pressed (eql (scenic::click-state button) :half-click))
           (over (eql (scenic::click-state button) :over))
           (radius 2)
           (line-width 3)
           (color (mk-color
                   (if (eql (button-type button) :primary)
                       (style-val :primary-color)
                       (style-val :color))
                   :width width :height height))
           (border (if (eql (button-type button) :primary)
                       (style-val :primary-border)
                       (style-val :border))))

      (scenic.draw:new-path)

      (scenic.draw:set-source-color (scenic.style:border-color border))

      (scenic.draw:set-line-width line-width)

      (scenic.draw:arc (+ left radius)
                       (+ top radius)
                       radius
                       (/ (* 2 pi) 2)
                       (/ (* 3 pi) 2))

      (scenic.draw:arc (- (+ left width) radius)
                       (+ top radius)
                       radius
                       (/ (* 3 pi) 2)
                       (/ (* 4 pi) 2))

      (scenic.draw:arc (- (+ left width) radius)
                       (- (+ top height) radius)
                       radius
                       (/ (* 0 pi) 2)
                       (/ (* 1 pi) 2))

      (scenic.draw:arc (+ left radius)
                       (- (+ top height) radius)
                       radius
                       (/ (* 1 pi) 2)
                       (/ (* 2 pi) 2))

      (scenic.draw:close-path)
      (scenic.draw:stroke-preserve)

      (when (or (not (eql (button-style button) :flat))
                over)
        (mk-color (if pressed
                      (style-val :color :status :pushed)
                      (style-val :color))
                  :width width :height height))

      (scenic.draw:fill-path))))

(defmethod renderer-paint ((renderer scenic.renderer:material-renderer) (button nui-button))
  (scenic.style:with-widget-styles button
    (let* ((left (layout-left button))
           (top (layout-top button))
           (width (layout-width button))
           (height (layout-height button))
           (pressed (eql (scenic::click-state button) :half-click))
           (over (eql (scenic::click-state button) :over))
           (radius 2)
           (line-width 3)
           (color (mk-color
                   (if (eql (button-type button) :primary)
                       (scenic.renderer::button-primary-color renderer)
                       (scenic.renderer::button-primary-color renderer))
                   :width width :height height))
           (border (if (eql (button-type button) :primary)
                       (scenic.renderer::button-primary-border renderer)
                       (scenic.renderer::button-primary-border renderer))))

      (draw-shadow (list left top width height)
                   :corner-radius radius
                   :shift '(1 1 1 1))

      (scenic.draw:new-path)

      (scenic.draw:set-source-color (scenic.style:border-color border))

      (scenic.draw:set-line-width line-width)

      (scenic.draw:arc (+ left radius)
                       (+ top radius)
                       radius
                       (/ (* 2 pi) 2)
                       (/ (* 3 pi) 2))

      (scenic.draw:arc (- (+ left width) radius)
                       (+ top radius)
                       radius
                       (/ (* 3 pi) 2)
                       (/ (* 4 pi) 2))

      (scenic.draw:arc (- (+ left width) radius)
                       (- (+ top height) radius)
                       radius
                       (/ (* 0 pi) 2)
                       (/ (* 1 pi) 2))

      (scenic.draw:arc (+ left radius)
                       (- (+ top height) radius)
                       radius
                       (/ (* 1 pi) 2)
                       (/ (* 2 pi) 2))

      (scenic.draw:close-path)
      (scenic.draw:stroke-preserve)

      (flet ((get-color (prop &optional status)
               (mk-color (style-val prop :status status)
                         :height height :width width)))
        (when (or (not (eql (button-style button) :flat))
                  over)
          (if pressed
              (get-color :color :pushed)
              (get-color :color))))

      (scenic.draw:fill-path))))

;; * API

(defun nui-button-labelled (label &rest initargs)
  (apply #'nui-button
         (uniform-padding
          8
          (aligner
           (label label :size 15)))
         initargs
         ))

(defun nui-button (child &rest initargs)
  (apply #'make-instance 'nui-button
         :child child
         initargs))

(export 'nui-button)

;;-- ** Examples

(defun button-ex-1 ()
  (scenic.renderer:with-renderer scenic.renderer:rect-renderer
    (let ((button (flow-row ()
                    (background "#f3f3f3"
                                (uniform-padding 50
                                                 (nui-button
                                                  (uniform-padding 10
                                                                   (label "ACCEPT" :color cl-colors:+white+
                                                                          :slant :normal))))))))
      (let ((scene (scenic:scene 200 200
                                 (uniform-padding 10 button))))
        (scenic:run-scene scene :window-options '(:resizable t))))))

;; (button-ex-1)


(defun button-ex-2 ()
  (let ((button (nui-button-labelled "Accept")))
    (let ((scene (scenic:scene 200 200
                               (uniform-padding 5 button))))
      (scenic:run-scene scene :window-options '(:resizable t)))))

;; (button-ex-2)

(defun button-ex-3 ()
  (scenic.renderer:with-renderer scenic.renderer:wnd7-renderer
    (let ((button (flow-row ()
                    (background "#f3f3f3"
                                (uniform-padding 50
                                                 (nui-button-labelled "Accept"))))))
      (let ((scene (scenic:scene 200 200
                                 (uniform-padding 10 button))))
        (scenic:run-scene scene :window-options '(:resizable t))))))

;; (button-ex-3)

(defun button-ex-4 ()
  (let ((button (nui-button
                 (uniform-padding
                  8
                  (aligner
                   (vertical-pack 10 '(:auto :auto)
                                 (list
                                  (label "Hello" :size 12 :face "Sans")
                                  (label "World" :size 15 :face "Sans")
                                  )))))))
    (let ((scene (scenic:scene 200 100
                               (background (scenic.style:get-style-property-value
                                            (scenic.renderer::renderer-stylesheet  scenic.renderer::*renderer*)
                                            :color 'scenic.renderer::panel)
                                           (uniform-padding 10
                                                            (vertical-pack 5 '(:auto :auto)
                                                                          (list
                                                                           (separation-line)
                                                                           button)))))))
      (scenic:run-scene scene :window-options '(:resizable t :double-buffer t)))))

;; (button-ex-4)
