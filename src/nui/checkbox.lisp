;;-- * Checkbox

(in-package :scenic.nui)

;;-- ** Definition

(defwidget nui-checkbox (nui-widget scenic::stateful-button)
  ())

;;-- ** Initialization

;; Implement for any renderer (default implementation)
(defmethod renderer-measure (renderer (checkbox nui-checkbox) available-width available-height)
  (scenic:set-measured checkbox 14 16))

;; Implement for any renderer (default implementation)
(defmethod renderer-layout (renderer (checkbox nui-checkbox) left top width height)
  (scenic:set-layout checkbox left top width height))

;;-- ** Rendering

(defmethod renderer-paint (renderer (checkbox nui-checkbox))
  (format t "Checkbox state: ~A~%" (scenic:state checkbox))
  (scenic.draw:set-color (ascendant-fill-color checkbox cl-colors:+white+))
  (scenic.draw:rectangle (layout-left checkbox)
                       (layout-top checkbox)
                       (layout-width checkbox)
                       (layout-height checkbox))
  (scenic.draw:fill-path)

  (scenic.draw:draw-svg (scenic::fa-icon-path
                         (if (scenic:state checkbox)
                             "check_sign"
                             "check_empty"))
                        (layout-left checkbox)
                        (layout-top checkbox)
                        (layout-width checkbox)
                        (layout-height checkbox)))

;;-- ** API

(defun wrap-stateful-button* (stateful-button comp)
  (let (label combination)
    (setf combination
          (horizontal-pack
           2 '(:auto :auto)
           (list (aligner stateful-button)
                 (setf label
                       (make-instance 'clickable
                                      :child (uniform-padding
                                              2
                                              (aligner comp)))))))
    (add-event-handler label :click nil
                       (lambda (o e)
                         (declare (ignore o e))
                         (setf (state stateful-button)
                               (not (state stateful-button)))))
    (values combination stateful-button)))

(defun nui-checkbox (comp &key state)
  (wrap-stateful-button*
   (make-instance 'nui-checkbox :state state)
   comp))

;;-- * Radio Button

;;-- ** Definition

(defwidget nui-radio-button (nui-widget scenic::stateful-button)
  ())

;;-- ** Rendering

;; Implement for any renderer (default implementation)
(defmethod renderer-measure (renderer (radio-button nui-radio-button) available-width available-height)
  (scenic:set-measured radio-button 14 16))

;; Implement for any renderer (default implementation)
(defmethod renderer-layout (renderer (radio-button nui-radio-button) left top width height)
  (scenic:set-layout radio-button left top width height))

(defmethod renderer-paint (renderer (radio-button nui-radio-button))
  (format t "Radio-Button state: ~A~%" (scenic:state radio-button))
  (scenic.draw:set-color (ascendant-fill-color radio-button cl-colors:+white+))
  (scenic.draw:rectangle (layout-left radio-button)
                       (layout-top radio-button)
                       (layout-width radio-button)
                       (layout-height radio-button))
  (scenic.draw:fill-path)

  (scenic.draw:draw-svg (scenic::fa-icon-path
                         (if (scenic:state radio-button)
                             "dot_circle_alt"
                             "circle_blank"))
                        (layout-left radio-button)
                        (layout-top radio-button)
                        (layout-width radio-button)
                        (layout-height radio-button)))

;;-- ** API

(defun nui-radio-button (comp &key state)
  (wrap-stateful-button*
   (make-instance 'nui-radio-button :state state)
   comp))

;;-- * Frame box

(defwidget nui-frame (nui-widget scenic::container1)
  ((title :initarg :title
          :initform nil
          :accessor frame-title
          :child t
          :on-change (:invalidate :layout))
   (title-position :initarg :title-position
                   :initform :top-left
                   :accessor title-position
                   :on-change (:invalidate :layout))
   (border-color :initarg :border-color
                 :initform cl-colors:+darkgray+
                 :accessor border-color
                 :on-change (:invalidate))
   (border-width :initarg :border-width
                 :initform 2
                 :accessor border-width
                 :on-change (:invalidate :layout))
   (padding :initarg :padding
            :initform 5
            :accessor frame-padding
            :on-change (:invalidate :layout))))

(defmethod initialize-instance :after ((widget nui-frame) &rest initargs)
  (setf (frame-title widget)
        (adjuster
         (background cl-colors:+lightgray+
                     (uniform-padding 3 (frame-title widget))))))

(defmethod renderer-measure (renderer (widget nui-frame) available-width available-height)
  (with-slots (title padding border-width) widget
    (measure title available-width available-height)
    (multiple-value-bind (child-width child-height)
        (measure (child widget)
                 (- available-width (* padding 2) (* border-width 2))
                 (- available-height (* padding 2) (* border-width 2)))
      #+nil(set-measured widget
                         (+ child-width (* padding 2) (* border-width 2))
                         (+ child-height (* padding 2) (* border-width 2)))
      (set-measured widget
                    available-width available-height)

      )))

(defmethod renderer-layout (renderer (widget nui-frame) left top width height)
  (with-slots (title padding border-color border-width) widget
    (layout title
            (+ left padding 10) (- top (/ padding 2))
            (- width 15) 10)
    (layout (child widget)
            (+ left (* 2 padding))
            (+ top (* 2 padding))
            (- width (* 2 padding))
            (- height (* 2 padding)))
    (set-layout widget left top width height)))

(defmethod renderer-paint (renderer (widget nui-frame))
  (with-slots (border-color border-width padding) widget
    (scenic-utils::draw-rect
     (+ (layout-left widget) (/ padding 2))
     (+ (layout-top widget) (/ padding 2))
     (- (layout-width widget) padding)
     (- (layout-height widget) padding)
     :stroke-width border-width
     :stroke-color border-color)))

(defmethod scenic::paint-order-walk ((object nui-frame) callback &key (after-callback nil))
  (when (funcall callback object)
    (dolist (child (list (frame-title object) (child object)))
      (when child
        (scenic::paint-order-walk child callback :after-callback after-callback))))
  (when after-callback
    (funcall after-callback object)))

(defun nui-frame (title child &rest args)
  (apply #'make-instance 'nui-frame
         :title title
         :child child
         args))

;;-- * Examples

(defun frame-ex-1 ()
  (let ((frame (nui-frame (label "Hello")
                              (adjuster (flow-col (:spacing 5)
                                          (label "Bye")
                                          (nui-checkbox (label "Quit now"))))
                              :padding 10)))
    (run-component (stack (background cl-colors:+lightgray+ (filler))
                          frame))))

(defun frame-ex-2 ()
  (flet ((frame ()
           (nui-frame (label "Hello")
                          (adjuster (flow-col (:spacing 5)
                                      (label "Bye")
                                      (nui-checkbox (label "Quit now"))))
                          :padding 10)))
    (run-component (stack (background cl-colors:+lightgray+ (filler))
                          (splitter-container-2
                           (expander (frame))
                           (splitter)
                           (expander (frame)))))
    ))

(defun frame-ex-3 ()
  (flet ((frame ()
           (nui-frame (label "Hello")
                          (adjuster (flow-col (:spacing 5)
                                      (label "Bye")
                                      (nui-checkbox (label "Quit now"))))
                          :padding 10)))
    (run-component (stack (background cl-colors:+lightgray+ (filler))
                          (splitter-container-2
                           (adjuster (frame))
                           (splitter)
                           (adjuster (frame)))))))

(defun checkbox-ex-1 ()
  (let ((comp (background cl-colors:+lightgray+
                          (uniform-padding
                           10
                           (flow-col (:spacing 10)
                             (label "Cambios aplicados" :size 14 :weight :bold)
                             (separation-line)
                             (nui-checkbox (label "Quit now"))
                             (flow-row (:spacing 10)
                               (nui-button (uniform-padding 4 (label "Aceptar")))
                               (nui-button (uniform-padding 4 (label "Cancelar")))))))))
    (run-component comp)))

(defun checkbox-ex-2 ()
  (let ((comp (background cl-colors:+white+
                          (uniform-padding
                           10
                           (flow-col ()
                             (nui-checkbox (label "test") :state nil)
                             (nui-checkbox (label "hello") :state t)
                             (nui-radio-button (label "foo") :state t)
                             (nui-radio-button (label "bar"))
                             (nui-radio-button (label "baz")))))))
    (run-component comp)))

;; (checkbox-ex-1)
;; (checkbox-ex-2)
