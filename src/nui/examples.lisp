;; * Examples
(in-package :scenic.nui)

;; * Scrollbars

(defun scrollbars-2 ()
  (let (horizontal-scrollbar vertical-scrollbar cmp)
    (setf cmp (stack
               (background (list 1.0 1.0 1.0)
                           (filler))
               (uniform-padding 5
                                (horizontal-pack
                                 0
                                 '(:auto (1 :auto))
                                 (list
                                  (vertical-pack
                                   0
                                   '(:auto (1 :auto))
                                   (list (background
                                          (list 0.3 0.4 0.5)
                                          (placeholder 200 200))
                                         (sizer (setf horizontal-scrollbar
                                                      (horizontal-scrollbar 0 50 30))
                                                :max-height 19
                                                :max-width 200)))
                                  (sizer (setf vertical-scrollbar
                                               (vertical-scrollbar 0 50 30))
                                         :max-width 19
                                         :max-height 200))))))
    (scenic:add-event-handler horizontal-scrollbar :position-changed nil
                              (lambda (object event)
                                (declare (ignore event))
                                (scenic:test-channel-write
                                 (list (scenic:current-min-position object)
                                       (scenic:page-size object)
                                       (scenic:min-value object)
                                       (scenic:max-value object)))))
    (scenic:add-event-handler vertical-scrollbar :position-changed nil
                              (lambda (object event)
                                (declare (ignore event))
                                (scenic:test-channel-write
                                 (list (scenic:current-min-position object)
                                       (scenic:page-size object)
                                       (scenic:min-value object)
                                       (scenic:max-value object)))))
    cmp))

(run-component (scrollbars-2))

(defun scrollbars-2 ()
  (let (horizontal-scrollbar vertical-scrollbar cmp)
    (setf cmp (stack
               (background (list 1.0 1.0 1.0)
                           (filler))
               (uniform-padding
                5
                (row ()
                  (:auto (col ()
                           (:auto (col ()
                                    ((1 :ext)
                                     (background
                                      (scenic::color cl-colors:+blue+)
                                      (placeholder 200 100)))
                                    ((1 :ext)
                                     (background
                                      (scenic::color cl-colors:+red+)
                                      (placeholder 200 100)))))
                           ((1 :auto)
                            (sizer (setf horizontal-scrollbar
                                         (horizontal-scrollbar 0 50 30))
                                   :max-height 19
                                   :max-width 200))))
                  ((1 :auto)
                   (sizer (setf vertical-scrollbar
                                (vertical-scrollbar 0 50 30))
                          :max-width 19
                          :max-height 200))))))
    (-> horizontal-scrollbar
        (on :position-changed
            (lambda (object event)
              (declare (ignore event))
              (scenic:test-channel-write
               (list (scenic:current-min-position object)
                     (scenic:page-size object)
                     (scenic:min-value object)
                     (scenic:max-value object))))))
    (-> vertical-scrollbar
        (on :position-changed
            (lambda (object event)
              (declare (ignore event))
              (scenic:test-channel-write
               (list (scenic:current-min-position object)
                     (scenic:page-size object)
                     (scenic:min-value object)
                     (scenic:max-value object))))))
    cmp))

(run-component (scrollbars-2))

(defun scrollbars-2 ()
  (with-bindings
    (stack :bind scrollbars
           (background (list 1.0 1.0 1.0)
                       (filler))
           (uniform-padding
            5
            (row ()
              (:auto (col ()
                       (:auto (col ()
                                ((1 :ext)
                                 (background
                                  (scenic::color cl-colors:+blue+)
                                  (placeholder 200 100)))
                                ((1 :ext)
                                 (background
                                  (scenic::color cl-colors:+red+)
                                  (placeholder 200 100)))))
                       ((1 :auto)
                        (sizer (horizontal-scrollbar :bind horizontal-scrollbar
                                                     0 50 30
                                                     :name "hs")
                               :max-height 19
                               :max-width 200))))
              ((1 :auto)
               (sizer  (vertical-scrollbar :bind vertical-scrollbar
                                           0 50 30)
                       :max-width 19
                       :max-height 200)))))
    (-> horizontal-scrollbar
        (on :position-changed
            (lambda (object event)
              (declare (ignore event))
              (scenic:test-channel-write
               (list (scenic:current-min-position object)
                     (scenic:page-size object)
                     (scenic:min-value object)
                     (scenic:max-value object))))))
    (-> vertical-scrollbar
        (on :position-changed
            (lambda (object event)
              (declare (ignore event))
              (scenic:test-channel-write
               (list (scenic:current-min-position object)
                     (scenic:page-size object)
                     (scenic:min-value object)
                     (scenic:max-value object))))))
    (print ($ "scrollbar" scrollbars))
    (->* ($ "scrollbar" scrollbars)
         (on :position-changed
             (lambda (obj ev)
               (declare (ignore obj ev))
               (format t "Scrollbar changed!!"))))
    (->* ($ "scrollbar#hs" scrollbars)
         (on :position-changed
             (lambda (obj ev)
               (declare (ignore obj ev))
               (format t "Horizontal scrollbar changed!!"))))
    scrollbars))

(run-component (scrollbars-2))

(defun scroll-view-test ()
  (with-bindings
    (scroll-view-auto
     :bind scroll-view
     (stack
      (background (list 1.0 1.0 1.0)
                  (filler))
      (uniform-padding
       5
       (col ()
         ((1 :ext)
          (background
           (scenic::color cl-colors:+blue+)
           (placeholder 200 100)))
         ((1 :ext)
          (background
           (scenic::color cl-colors:+red+)
           (placeholder 200 100))))))
     :inside-width 200 :inside-height 400
     :always-horizontal-scrollbar nil
     :always-vertical-scrollbar nil)
    scroll-view))

(run-component (scroll-view-test))

;; * Simple boxes

(with-bindings
  (background :bind comp
              cl-colors:+lightgray+
              (progn
                (uniform-padding :bind h-buttons
                                 5
                                 (flow-row (:spacing 5)
                                   (nui-button-labelled "Accept")
                                   (nui-button-labelled "Cancel")))

                (uniform-padding :bind v-buttons
                                 5
                                 (flow-col (:spacing 5)
                                   (nui-button-labelled "Accept" :style :flat)
                                   (nui-button-labelled "Cancel" :style :flat)))
                (flow-col
                    (:spacing 5)
                  h-buttons v-buttons)))
  (run-component comp))

