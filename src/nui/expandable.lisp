(in-package :scenic.nui)

;;-- * Expandable

;;-- ** Definition

(scenic::defwidget expandable (scenic::container1)
  ((label :initarg :label :accessor expandable-label :initform nil)
   (content :initarg :content :accessor expandable-content :initform nil)
   (expanded :initarg :expanded
             :accessor expanded
             :initform nil
             :type boolean
             :on-change (:rebuild :layout :invalidate))))

;;-- ** Initialization/construction

(defmethod scenic::build ((expandable expandable))
  (if (not (expanded expandable))
      (with-bindings 
        (setf (child expandable)
              (uniform-padding
               5
               (flow-row (:spacing 5)
                 (expandable-label expandable)
                 (make-instance 'clickable :bind expand-button
                                :child
                                (sizer 
                                 (fa-icon "caret_right")
                                 :height 10 :width 10)))))
        (add-event-handler expand-button :click nil
                           (lambda (button event)
                             (setf (expanded expandable) t))))
      ;; else, it is expanded
      (with-bindings
        (setf (child expandable)
              (flow-col (:spacing 2)
                (uniform-padding
                 5
                 (flow-row (:spacing 5)
                   (expandable-label expandable)
                   (make-instance 'clickable :bind collapse-button
                                  :child
                                  (sizer
                                   (fa-icon "caret_down")
                                   :width 10 :height 15))))
                (expandable-content expandable)))
        (add-event-handler collapse-button :click nil
                           (lambda (button event)
                             (setf (expanded expandable) nil))))))

;;-- ** API

(defun expandable (label content &key expanded)
  (make-instance 'expandable :label label
                 :content content
                 :expanded expanded))

(export 'expandable)

;;-- ** Examples

(defun expandable-ex-1 ()
  (let ((comp (background cl-colors:+white+
                          (flow-col ()
                                    (expandable
                                     (label "Hello")
                                     (background  cl-colors:+red+
                                                  (placeholder 100 100)))
                                    (expandable
                                     (label "Bye")
                                     (background  cl-colors:+blue+
                                                  (placeholder 100 100)))
                                    (label "World")))))
    (scenic:run-scene (scenic:scene 500 500 comp))))

;; (expandable-ex-1)
