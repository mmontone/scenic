;; montage balls.gif -tile x1 -geometry '1x1+0+0<' -alpha On -background "rgba(0,0,0,0.0)" -quality 100 balls.png

(ql:quickload "sdl2")
(ql:quickload "sdl2-image")

(defun run (file)
  (sdl2:with-init (:everything)
    (sdl2:with-window (win :flags '(:shown :resizable))
      (sdl2:with-renderer (renderer win :flags '(:accelerated))
        (let* ((surface-frames 20)
               (surface (sdl2-image:load-image file))
               (texture (sdl2:create-texture-from-surface renderer surface))
               (frame-width (/ (sdl2:texture-width texture) surface-frames))
               (frame-height (sdl2:texture-height texture))
               (frame 0))
          (sdl2:with-event-loop (:method :poll)
            (:keyup
             (:keysym keysym)
             (when (sdl2:scancode= (sdl2:scancode-value keysym) :scancode-escape)
               (sdl2:push-event :quit)))
            (:idle
             ()
             (let ((source-rect
                   (sdl2:make-rect (round (* frame frame-width))
                                   0
                                   (round frame-width)
                                   (round frame-height))))
               
               (sdl2:render-copy renderer texture :source-rect source-rect)
               (setf frame (mod (1+ frame) surface-frames))
               (sdl2:delay 30))
             (sdl2:render-present renderer))
            (:quit () t)))))))

(run (asdf:system-relative-pathname :scenic "icons/balls.png"))
(run (asdf:system-relative-pathname :scenic "icons/rolling.png"))
