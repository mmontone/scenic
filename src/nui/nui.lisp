;;-- * NUI widgets
;;-- NUI widgets are a set of widgets which are themable.
;;-- NUI looks can be customized in two ways. Using renderers for drawing a widget in an specific way. Also,
;;-- accessing renderer properties for colors, border sizes, and other options. And finally, they support
;;-- the application of stylesheets.

(in-package :scenic.nui)

(setf scenic.renderer:*renderer* (scenic.renderer:find-renderer 'scenic.renderer:gtk-renderer))

;;-- ** Utility functions
;;-- *** General

(defun degrees (x)
  (* x (/ pi 180)))

(defun ascendant-fill-color (widget &optional default)
  (let ((ascendant (find-if
                    (lambda (w)
                      (slot-exists-p w 'fill-color))
                    (scenic::get-widget-chain (list widget)))))
    (or (and ascendant (slot-value ascendant 'fill-color))
        default)))


(defun make-linear-gradient (direction length color-stops)
  (let* ((dir (case direction
                  (:right (list 0 0 length 0))
                  (:left (list length 0 0 0))
                  (:bottom (list 0 0 0 length))
                  (:top (list 0 length 0 0))
                  (t direction)))
           (pat (apply #'cairo:create-linear-pattern
                       dir)))
      (let ((delta (/ 1 (1- (length color-stops)))))
        (loop
           :for offset := 0 :then (+ offset delta)
           :for color-stop :in color-stops
           :do
           (cairo:pattern-add-color-stop
            pat offset
            (scenic.draw::parse-color-or-pattern color-stop))))      
      pat))
  
(defmethod linear-gradient ((gradient-spec list) length)

  (destructuring-bind (_ direction &rest color-stops) gradient-spec
    (declare (ignore _))
    (make-linear-gradient direction length color-stops)))

(defmethod linear-gradient ((gradient scenic.draw::linear-gradient) length)
  (make-linear-gradient
   (scenic.draw::linear-gradient-direction gradient)
   length
   (scenic.draw::linear-gradient-color-stops gradient)))

(defmethod mk-color (color &rest args)
  )

(defmethod mk-color (color &rest args)
  (mk-color (scenic.draw:parse-color-or-pattern color)))

(defmethod mk-color ((color cl-colors:rgb) &rest args)
  color)

(defmethod mk-color ((gradient scenic.draw::linear-gradient) &rest args)
  (linear-gradient gradient
                   (let ((direction (scenic.draw::linear-gradient-direction gradient)))
                     (cond ((member direction '(:bottom :top))
                            (or (getf args :height) (error "Provide height for linear gradient")))
                           ((member direction
                                    '(:right :left))
                            (or (getf args :width) (error "Provide width for linear gradient")))
                           (t (error "Invalid gradient direction: ~A" direction))))))

(defun draw-shadow (rect &key (color (scenic.draw::make-alpha-color
                                      :color cl-colors:+gray+
                                      :alpha 0.5))
                           corner-radius
                           (shift '(2 2 2 2)))
  (destructuring-bind (x0 y0 w h) rect
    (scenic-utils::fill-rect (+ x0 (nth 3 shift))
                             (+ y0 (first shift))
                             (+ w (second shift))
                             (+ h (third shift))
                             :fill-color color
                             :corner-radius corner-radius)))

;;-- ** Events

(defun on (object event callback &optional propagation)
  (scenic:add-event-handler object event propagation callback))

;;-- ** Selectors

(defun $ (selector scene-or-widget)
  (scenic.style:find-widgets selector scene-or-widget))

;;-- *** Layout and rendering

(defgeneric renderer-paint (renderer widget)
  (:documentation "Themed widget rendering")
  (:method (renderer widget)
    (error "Cannot paint widget: ~A with renderer: ~A" widget renderer)))

(defgeneric renderer-measure (renderer widget available-width available-height)
  (:documentation "Measure widget using renderer")
  (:method (renderer widget available-width available-height)
    (set-measured widget available-width available-height)
    (values available-width available-height)))

(defgeneric renderer-layout (renderer widget left top width height)

  (:method (renderer widget left top width height)
    (set-layout widget left top width height)))

;;-- ** Definition

(defwidget nui-widget ()
  ((stylesheet :initarg :style
               :accessor widget-stylesheet
               :initform nil
               :documentation "An inline-stylesheet that applies to the widget"))
  (:documentation "NUI widgets support theming via renderers and styling through stylesheets"))

;;-- *** Layout and rendering

(defmethod scenic:paint ((widget nui-widget))
  ;; TODO: referencing renderer like this is very inefficient
  (renderer-paint (scenic::renderer (scenic::get-scene widget))
                  widget))

(defmethod scenic::measure ((widget nui-widget) available-width available-height)
  ;; TODO: referencing renderer like this is very inefficient
  (renderer-measure (scenic::renderer (scenic::get-scene widget))
                    widget available-width available-height))

(defmethod scenic::layout ((widget nui-widget) left top width height)

  ;; TODO: referencing renderer like this is very inefficient
  (renderer-layout (scenic::renderer (scenic::get-scene widget))
                   widget left top width height))

;;-- *** Styles

(defmethod scenic.style::get-widget-style-property-value
    ((widget scenic.nui::nui-widget)
     prop-name &key status default (error-p t))
  (flet ((ret-if-found (p-val)
           (when p-val
             (return-from scenic.style::get-widget-style-property-value p-val))))
    (when status
      (alexandria:when-let (sheet (widget-stylesheet widget))
        (ret-if-found (scenic.style::get-style-property-value sheet prop-name widget
                                                :status status)))
      (alexandria:when-let (sheet (scenic::scene-stylesheet (scenic::get-scene widget)))
        (ret-if-found (scenic.style::get-style-property-value sheet prop-name widget
                                                :status status)))
      (alexandria:when-let (sheet (scenic.renderer::renderer-stylesheet scenic.renderer:*renderer*))
        (ret-if-found (scenic.style::get-style-property-value sheet prop-name widget
                                                :status status))))
    
    (alexandria:when-let (sheet (widget-stylesheet widget))
      (ret-if-found (scenic.style::get-style-property-value sheet prop-name widget)))
    (alexandria:when-let (sheet (scenic::scene-stylesheet (scenic::get-scene widget)))
      (ret-if-found (scenic.style::get-style-property-value sheet prop-name widget)))
    (alexandria:when-let (sheet (scenic.renderer::renderer-stylesheet scenic.renderer:*renderer*))
      (ret-if-found (scenic.style::get-style-property-value sheet prop-name widget)))

    (or default
        (and error-p (error "Property not found: ~A for: ~A" prop-name widget)))))

;;-- * Separation line widget

(defwidget separation-line (nui-widget)
  ((orientation :initarg :orientation
                :initform :horizontal
                :accessor orientation)))

(defun separation-line (&optional (orientation :horizontal))
  (make-instance 'separation-line :orientation orientation))

(defmethod renderer-measure (renderer (line separation-line) available-width available-height)
  ;; This is a trick. We set the width/height to 0 as this is not going to be the final measure. Think of measuring as the "required mesure", or "ideal measure", not the final measure, which is defined by layout method, and which considers the containers. We set it to 0, so that it does not take space in containers calculations.
  (if (eql (orientation line) :horizontal)
      (set-measured line 0  2)
      (set-measured line 2 0)))

(defmethod renderer-paint ((renderer scenic.renderer::rect-renderer) (line separation-line))
  (scenic.draw:new-path)

  (scenic.draw:set-line-width 1)

  (scenic.draw:set-source-color (scenic.draw:parse-color-or-pattern "#c5c5c5"))
  (scenic.draw:move-to (layout-left line) (layout-top line))
  (scenic.draw:line-to (+ (layout-left line) (layout-width line))
                       (layout-top line))
  (scenic.draw:stroke)

  (scenic.draw:set-source-color (scenic.draw:parse-color-or-pattern "#e4e4e4"))
  (scenic.draw:move-to (layout-left line) (1+ (layout-top line)))
  (scenic.draw:line-to (+ (layout-left line) (layout-width line))
                       (1+ (layout-top line)))
  (scenic.draw:stroke))

(defmethod renderer-paint ((renderer scenic.renderer:gtk-renderer) (line separation-line))
  (scenic.draw:new-path)

  (scenic.draw:set-line-width 1)

  (scenic.draw:set-source-color (scenic.draw:parse-color-or-pattern "#c5c5c5"))
  (scenic.draw:move-to (layout-left line) (layout-top line))
  (scenic.draw:line-to (+ (layout-left line) (layout-width line))
                       (layout-top line))
  (scenic.draw:stroke)

  (scenic.draw:set-source-color (scenic.draw:parse-color-or-pattern "#e4e4e4"))
  (scenic.draw:move-to (layout-left line) (1+ (layout-top line)))
  (scenic.draw:line-to (+ (layout-left line) (layout-width line))
                       (1+ (layout-top line)))
  (scenic.draw:stroke))

;;-- * Examples

(defun scene-ex-1 ()
  (let ((scene (scenic:scene 120 45
                             (background (scenic::color cl-colors:+gray+)
                                         (uniform-padding 8
                                                          (separation-line))))))
    (scenic:run-scene scene :window-options '(:resizable t :double-buffer t))))

;; (scene-ex-1)

(defun run-component (comp &key redraw-freq width height
                             (title "Scenic")
                             (resizable t)
                             (borderless nil))
  "This function is for quick testing of widgets only"
  (let ((scene (scenic:scene (or width 200)
                             (or height 200)
                             comp)))

    #+nil(when redraw-freq
           (flet ((redraw-scene (&rest args)
                    (scenic::invalidate-scene scene)
                    (scenic.sdl2::render-scene scene t)))
             (scenic::add-step-function
              (scenic::step-function #'redraw-scene
                                     :target scene
                                     :by redraw-freq
                                     :times nil)
              scene)))

    (scenic:run-scene scene :window-options
                      (list :resizable resizable :title title
                            :borderless borderless))))
