(in-package :scenic.nui)

;;-- * Popup

(defun show-overlay (&optional color alpha)
  (scenic::add-child
   (widget scenic::*scene*)
   (background (scenic.draw::make-alpha-color
                :color
                (or color cl-colors:+black+)
                :alpha (or alpha 0.4))
               (filler))))

;;-- * Simple dialog

(defun simple-dialog (title body actions)
  (scenic::adjuster 
   (rbox-1
    (flow-col (:spacing 10)
      (uniform-padding
       5
       title)
      (separation-line)
      body
      (separation-line)
      actions)
    :fill-color cl-colors:+lightgray+
    :padding-left 10
    :padding-right 10
    :padding-top 10
    :padding-bottom 10)))

;;-- * Tooltip

(defun open-tooltip (widget scene event &key position after)
  (let ((tooltip (scenic::placer (mouse-x event) (mouse-y event) widget)))
    (scenic::add-child
     (widget scene)
     tooltip)

    (setf (scenic::layedout scenic::*scene*) nil)
    (invalidate-scene scene)
    tooltip))

(defun close-tooltip (tooltip)
  (let ((scene (scenic::get-scene tooltip)))
    (format t "Closing tooltip: ~A~%" tooltip)
    (scenic::remove-child
     (widget scene)
     tooltip)

    (setf (scenic::layedout scene) nil)
    (invalidate-scene scene)
    (invalidate (widget scene))))

(defun setup-tooltip (target content &rest options)
  (let ((tooltip-ontheway)
        (tooltip-open)
        tooltip
        (scene (scenic::get-scene target)))
    (scenic:add-event-handler target
                              :mouse-enter
                              :bubble
                              (lambda (w event)
                                (when (not (or tooltip-ontheway tooltip-open))
                                  (setf tooltip (apply #'open-tooltip content scene event options))
                                  (setf tooltip-open t))))
    (scenic:add-event-handler target
                              :mouse-leave
                              :bubble (lambda (w event)
                                        (when tooltip-open
                                          (close-tooltip tooltip))
                                        (setf tooltip-ontheway nil)
                                        (setf tooltip-open nil)
                                        ))))


;;-- * Examples

(defun popup-ex-1 ()
  (with-bindings
    (let ((comp (stack
                 (background cl-colors:+white+
                             (placeholder 500 500))

                 (sizer :bind label
                        (background (scenic.draw:parse-color-or-pattern
                                     '(:rgba 1 0 0 1))
                                    (uniform-padding 50
                                                     (label "Click me")))
                        :height 100 :width 100))))
      (let ((scene (scenic:scene 500 500
                                 comp)))
        (scenic:add-event-handler label
                                  :mouse-button-down
                                  :bubble
                                  (lambda (label event)
                                    (scenic::popup
                                     (background cl-colors:+black+
                                                 (rbox-1
                                                  (uniform-padding 5
                                                                   (label "This is a popup" :color cl-colors:+white+)))) :overlay t)))
        (scenic:run-scene scene :window-options '(:resizable t))))))

;; (popup-ex-1)


(defun popup-ex-2 ()
  (with-bindings
    (let ((comp (stack
                 (background cl-colors:+white+
                             (placeholder 500 500))

                 (sizer :bind label
                        (background (scenic.draw:parse-color-or-pattern
                                     '(:rgba 1 0 0 1))
                                    (uniform-padding 50
                                                     (label "Click me")))
                        :height 100 :width 100))))
      (let ((scene (scenic:scene 500 500
                                 comp)))
        (scenic:add-event-handler label
                                  :mouse-button-down
                                  :bubble
                                  (lambda (label event)
                                    (scenic::popup
                                     (simple-dialog (label "Are you sure?" :weight :bold)
                                                    (padding 0 10 0 10
                                                             (label "Are you sure you want to do that?"))
                                                    (aligner
                                                     (padding 0 10 0 0
                                                              (flow-col ()
                                                                (flow-row (:spacing 10)
                                                                  (nui-button (uniform-padding 5 (label "Yes")))
                                                                  (nui-button (uniform-padding 5 (label "No"))))))
                                                     :horizontal :right))
                                     :overlay t)))
        (scenic:run-scene scene :window-options '(:resizable t))))))

;; (popup-ex-2)

(defun popup-ex-3 ()
  (with-bindings
    (let ((comp (stack
                 (background cl-colors:+white+
                             (placeholder 500 500))

                 (sizer :bind label
                        (background (scenic.draw:parse-color-or-pattern
                                     '(:rgba 1 0 0 1))
                                    (uniform-padding 50
                                                     (label "Click me")))
                        :height 100 :width 100))))
      (let ((scene (scenic:scene 500 500
                                 comp)))
        (scenic:add-event-handler label
                                  :mouse-button-down
                                  :bubble
                                  (lambda (label event)
                                    (scenic::popup
                                     (simple-dialog (label "Are you sure?" :weight :bold)
                                                    (padding 0 10 0 10
                                                             (label "Are you sure you want to do that?"))
                                                    (aligner
                                                     (padding 0 10 0 0
                                                              (flow-col ()
                                                                (flow-row (:spacing 10)
                                                                  (nui-button (uniform-padding 5 (label "Yes")))
                                                                  (nui-button (uniform-padding 5 (label "No"))))))
                                                     :horizontal :right))
                                     :sdl-window t
                                     :overlay t)))
        (scenic:run-scene scene :window-options '(:resizable t))))))

;; (popup-ex-3)


(defun tooltip-ex-1 ()
  (with-bindings
    (let ((comp (stack
                 (background cl-colors:+white+
                             (placeholder 500 500))

                 (sizer :bind label
                        (background (scenic.draw:parse-color-or-pattern
                                     '(:rgba 1 0 0 1))
                                    (uniform-padding 50
                                                     (label "Hover me")))
                        :height 100 :width 100))))
      (let ((scene (scenic:scene 500 500
                                 comp)))
        ;; This should work. sizer shouldn't be needed here.

        #+nil(setup-tooltip label (rbox-1 (label "This is a tooltip")
                                                    :fill-color cl-colors:+yellow+
                                                    :padding-left 5
                                                    :padding-right 5
                                                    :padding-top 5
                                                    :padding-bottom 5
                                                    :corner-radius nil))

        (setup-tooltip label (sizer (rbox-1 (label "This is a tooltip")
                                                      :fill-color cl-colors:+yellow+
                                                      :padding-left 5
                                                      :padding-right 5
                                                      :padding-top 5
                                                      :padding-bottom 5
                                                      :corner-radius 2)
                                    :width 120 :height 30))

        (scenic:run-scene scene :window-options '(:resizable t))))))

;; (tooltip-ex-1)

;;-- * Common dialogs

(defparameter +question-icon+ (scenic::fa-icon-path "_627"))
(defparameter +ban-icon+ (scenic::fa-icon-path "ban_circle"))
(defparameter +comment-icon+ (scenic::fa-icon-path "comment_alt"))
(defparameter +warning-icon+ (scenic::fa-icon-path "warning_sign"))
(defparameter +minus-icon+ (scenic::fa-icon-path "minus_sign"))
(defparameter +info-icon+ (scenic::fa-icon-path "info_sign"))

(defun accept-dialog (message &key icon on-accept button-label)
  (let (dialog-window)
    (flet ((dialog-accept (object event)
             (declare (ignore object event))
             (scenic::close-modal dialog-window)
             (when on-accept
               (funcall on-accept))))
      (let ((dialog (stack
                     (background cl-colors:+lightgray+
                                 (filler))
                     (uniform-padding
                      5
                      (simple-horizontal-pack
                       5
                       (list
                        (aligner 
                         (uniform-padding
                          10
                          (sizer 
                           icon
                           :width 45 :height 50)))
                        (uniform-padding
                         10
                         (simple-vertical-pack
                          5
                          (list 
                           (aligner
                            (label message))
                           (button
                            (uniform-padding
                             5
                             (label (or button-label "Ok")))
                            :on-click #'dialog-accept))))))))))
        (setf dialog-window
              (scenic::open-modal (scene 300 100 dialog)))))))

(defun info-dialog (message &key icon on-accept button-label)
  (accept-dialog message
                 :icon (or icon (svg-image +info-icon+))
                 :on-accept on-accept
                 :button-label button-label))

(defun warning-dialog (message &key icon on-accept button-label)
  (accept-dialog message
                 :icon (or icon (svg-image +warning-icon+))
                 :on-accept on-accept
                 :button-label button-label))

(defun question-dialog (question &key icon on-yes on-no)
  (let (dialog-window)
    (flet ((dialog-action (func)
             (lambda (object event)
               (declare (ignore object event))
               (scenic::close-modal dialog-window)
               (when func
                 (funcall func)))))
      (let ((dialog (stack
                     (background cl-colors:+lightgray+
                                 (filler))
                     (uniform-padding
                      5
                      (simple-horizontal-pack
                       5
                       (list
                        (aligner 
                         (uniform-padding
                          10
                          (sizer 
                           (or icon (svg-image +question-icon+))
                           :width 45 :height 50)))
                        (uniform-padding
                         10
                         (simple-vertical-pack
                          5
                          (list 
                           (aligner
                            (label question))
                           (simple-horizontal-pack
                            5
                            (list
                             (button
                              (uniform-padding
                               5
                               (label "Yes"))
                              :on-click (dialog-action on-yes))
                             (button
                              (uniform-padding
                               5
                               (label "No"))
                              :on-click (dialog-action on-no)))))))))))))
        (setf dialog-window
              (scenic::open-modal (scene 300 100 dialog)))))))
  
(defun info-dialog-test ()
  "(info-dialog-test)"
  (let ((out *standard-output*))
    (info-dialog "This is information"
                 :on-accept (lambda () (format out "Info accepted")))))

(defun warning-dialog-test ()
  "(warning-dialog-test)"
  (let ((out *standard-output*))
    (warning-dialog "This is a warning"
                    :on-accept (lambda () (format out "Warning accepted")))))

(defun question-dialog-test ()
  "(question-dialog-test)"
  (let ((out *standard-output*))
    (question-dialog "Are you sure?"
                     :on-yes (lambda () (format out "Yes!"))
                     :on-no (lambda () (format out "No!")))))
