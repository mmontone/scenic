;;-- * Scrollbars

(in-package :scenic.nui)

;; ** Definition

(defwidget nui-scrollbar (scenic:scrollbar)
  ((width :initarg :width
          :accessor scrollbar-width
          :initform (scenic.renderer::scrollbar-width
                     scenic.renderer:*renderer*))))

(defwidget nui-slider (nui-widget scenic:slider clickable)
  ((color :initarg :color
          :initform (scenic.renderer::slider-color
                     scenic.renderer:*renderer*)
          :accessor slider-color)
   (pressed-color :initarg :pressed-color
                  :initform (scenic.renderer::slider-color[pushed]
                             scenic.renderer:*renderer*)
                  :accessor slider-pressed-color)
   (hover-color :initarg :hover-color
                :initform (scenic.renderer::slider-color[hover]
                           scenic.renderer:*renderer*)
                :accessor slider-hover-color)))

;;-- ** Rendering

(defmethod renderer-paint ((renderer scenic.renderer::rect-renderer)
                           (object nui-slider))
  (let*
      ((over (eql (scenic::click-state object) :over))
       (pressed (eql (scenic::click-state object) :half-click))
       (color (cond
                (pressed (slider-pressed-color object))
                (over (or (slider-hover-color object)
                          (scenic.draw:brighten (slider-color object))))
                (t (slider-color object)))))
    (scenic-utils::draw-rect
     (layout-left object) (layout-top object)
     (layout-width object) (layout-height object)
     :stroke-width 1
     :stroke-color color
     :fill-color color
     :corner-radius nil)))

(defmethod renderer-paint ((renderer scenic.renderer:gtk-renderer) (object nui-slider))
  (scenic.draw:rectangle (layout-left object) (layout-top object)
                   (layout-width object)
                   (layout-height object))
  (scenic.draw:set-source-rgb 0.6 0.6 0.6)
  (scenic.draw:fill-path)
  (multiple-value-bind (left top width height)
      (scenic::get-walker-coordinates object)
    (let ((radius 2) (over nil))

      (scenic.draw:new-path)

      (scenic.draw:set-source-color cl-colors:+lightgray+)

      (scenic.draw:set-line-width 1)

      (scenic.draw:arc (+ left radius)
                 (+ top radius)
                 radius
                 (/ (* 2 pi) 2)
                 (/ (* 3 pi) 2))

      (scenic.draw:arc (- (+ left width) radius)
                 (+ top radius)
                 radius
                 (/ (* 3 pi) 2)
                 (/ (* 4 pi) 2))

      (scenic.draw:arc (- (+ left width) radius)
                 (- (+ top height) radius)
                 radius
                 (/ (* 0 pi) 2)
                 (/ (* 1 pi) 2))

      (scenic.draw:arc (+ left radius)
                 (- (+ top height) radius)
                 radius
                 (/ (* 1 pi) 2)
                 (/ (* 2 pi) 2))

      (scenic.draw:close-path)
      (scenic.draw:stroke-preserve)

      (let ((color (scenic.renderer::button-color scenic.renderer:*renderer*)))
        (when over
          (setf color (scenic.draw:brighten color)))
        (scenic.draw:set-fill-extent color height)
        (scenic.draw:set-color color))

      (scenic.draw:fill-path))))


;;-- ** Initialization

(defmethod initialize-instance :after ((instance nui-scrollbar) &rest initargs)
  (declare (ignore initargs))
  (let* ((slider (make-instance
                  'nui-slider
                  :orientation (orientation instance)
                  :min-value (min-value instance)
                  :max-value (max-value instance)
                  :page-size (page-size instance)
                  :current-min-position
                  (current-min-position instance)))
         (btn-left (make-instance
                    'nui-button
                    :child (make-instance 'arrow
                                          :direction (scenic::ifhorizontal instance :left :up))))
         (btn-right (make-instance
                     'nui-button
                     :child (make-instance
                             'arrow
                             :direction (scenic::ifhorizontal instance :right :down))))
         (grid (scenic::ifhorizontal instance
                                     (make-instance 'scenic-grid:grid
                                                    :column-layout-options '(:auto (1 :ext) :auto)
                                                    :row-layout-options '((16 :px))
                                                    :children-descriptions `((:row (:cell ,btn-left)
                                                                               (:cell ,slider)
                                                                               (:cell ,btn-right))))
                                     (make-instance
                                      'scenic-grid:grid
                                      :column-layout-options '((16 :px))
                                      :row-layout-options '(:auto (1 :ext) :auto)
                                      :children-descriptions `((:column (:cell ,btn-left)
                                                                        (:cell ,slider)
                                                                        (:cell ,btn-right)))))))
    (setf (child instance) grid)
    (setf (slider instance) slider)
    (add-event-handler slider :position-changed :bubble
                       (lambda (object event)
                                        ; Using slot-value to prevent
                                        ; passing down the value to
                                        ; the slider via setf (thus
                                        ; entering an infinite loop).
                         (setf (slot-value instance 'current-min-position)
                               (current-min-position object))
                         (on-event instance :position-changed event nil)))
    (add-event-handler btn-left :click :bubble
                       (lambda (object event)
                         (declare (ignore object event))
                         (setf (current-min-position instance)
                               (- (current-min-position slider)
                                  (scenic::scroll-delta slider)))))
    (add-event-handler btn-right :click :bubble
                       (lambda (object event)
                         (declare (ignore object event))
                         (setf (current-min-position instance)
                               (+ (current-min-position slider)
                                  (scenic::scroll-delta instance)))))))

(scenic::pass-to-child nui-scrollbar slider min-value)
(scenic::pass-to-child nui-scrollbar slider max-value)
(scenic::pass-to-child nui-scrollbar slider page-size)
(scenic::pass-to-child nui-scrollbar slider current-min-position)
(scenic::pass-to-child nui-scrollbar slider scenic::scroll-delta)

(defwidget scrollable-area (scenic::container1)
  ()
  (:documentation "Holds an area and activates scrollbars when needed"))

(defun scrollable-area (child)
  (make-instance 'scrollable-area :child child))


;;-- ** API

(defun vertical-nui-scrollbar (min max page)
  (make-instance 'nui-scrollbar
                 :orientation :vertical
                 :min-value min
                 :max-value max
                 :page-size page
                 :current-min-position min))

(defun horizontal-nui-scrollbar (min max page &rest initargs)
  (apply #'make-instance 'nui-scrollbar
         :orientation :horizontal
         :min-value min
         :max-value max
         :page-size page
         :current-min-position min
         initargs))

;;-- ** Examples

(defun nui-scrollbars-test ()
  (with-bindings
    (stack :bind scrollbars
           (background (list 1.0 1.0 1.0)
                       (filler))
           (uniform-padding
            5
            (row ()
              (:auto (col ()
                       (:auto (col ()
                                ((1 :ext)
                                 (background
                                  (scenic::color cl-colors:+blue+)
                                  (placeholder 200 100)))
                                ((1 :ext)
                                 (background
                                  (scenic::color cl-colors:+red+)
                                  (placeholder 200 100)))))
                       ((1 :auto)
                        (sizer (horizontal-nui-scrollbar :bind horizontal-scrollbar
                                                         0 50 30
                                                         :name "hs")
                               :max-height 19
                               :max-width 200))))
              ((1 :auto)
               (sizer  (vertical-nui-scrollbar :bind vertical-scrollbar
                                               0 50 30)
                       :max-width 19
                       :max-height 200)))))
    (arrows:-> horizontal-scrollbar
                  (on :position-changed
                      (lambda (object event)
                        (declare (ignore event))
                        (scenic:test-channel-write
                         (list (scenic:current-min-position object)
                               (scenic:page-size object)
                               (scenic:min-value object)
                               (scenic:max-value object))))))
    (arrows:-> vertical-scrollbar
                  (on :position-changed
                      (lambda (object event)
                        (declare (ignore event))
                        (scenic:test-channel-write
                         (list (scenic:current-min-position object)
                               (scenic:page-size object)
                               (scenic:min-value object)
                               (scenic:max-value object))))))
    (print ($ "scrollbar" scrollbars))
    (->* ($ "scrollbar" scrollbars)
         (on :position-changed
             (lambda (obj ev)
               (declare (ignore obj ev))
               (format t "Scrollbar changed!!"))))
    (->* ($ "scrollbar#hs" scrollbars)
         (on :position-changed
             (lambda (obj ev)
               (declare (ignore obj ev))
               (format t "Horizontal scrollbar changed!!"))))
    scrollbars))

(defun scrollbar-ex-1 ()
  (scenic.renderer:with-renderer scenic.renderer:rect-renderer
    (run-component (nui-scrollbars-test) :title "Scrollbar")))

;; (scrollbar-ex-1)

(defun scrollable (child &key
                           (inside-width (expt 10 6))
                           (inside-height (expt 10 6))
                           (always-horizontal-scrollbar t)
                           (always-vertical-scrollbar t))
  (let* ((scroll-view (scroll-view child
                                   :inside-width inside-width
                                   :inside-height inside-height))
         (hscroll (horizontal-nui-scrollbar 0 100 10))
         (vscroll (vertical-nui-scrollbar 0 100 10))
         (grid (grid '(:auto (1 :auto))
                     '(:auto (1 :auto))
                     `((:row (:cell ,scroll-view)
                         (:cell ,vscroll))
                       (:row (:cell ,hscroll))))))
    (add-event-handler scroll-view :scroll-view-measured :bubble
                       (lambda (o evt)
                         (declare (ignore o))
                         (labels ((set-hscroll-visibility ()
                                    (when (measured-width grid)
                                      (setf (visible hscroll)
                                            (or always-horizontal-scrollbar
                                                (< (- (measured-width grid)
                                                      (if (visible vscroll) 19 0))
                                                   (inner-width evt))))))
                                  (set-vscroll-visibility ()
                                    (when (measured-height grid)
                                      (setf (visible vscroll)
                                            (or always-vertical-scrollbar
                                                (< (- (measured-height grid)
                                                      (if (visible hscroll) 19 0))
                                                   (inner-height evt)))))))
                           (setf (page-size hscroll) (outer-width evt))
                           (setf (max-value hscroll) (inner-width evt))
                           (setf (page-size vscroll) (outer-height evt))
                           (setf (max-value vscroll) (inner-height evt))
                           (set-hscroll-visibility)
                           (set-vscroll-visibility)
                           (set-hscroll-visibility))))
    (add-event-handler hscroll :position-changed nil
                       (lambda (o evt)
                         (declare (ignore o evt))
                         (setf (horizontal-offset scroll-view)
                               (current-min-position hscroll))
                         (invalidate scroll-view)))
    (add-event-handler vscroll :position-changed nil
                       (lambda (o evt)
                         (declare (ignore o evt))
                         (setf (vertical-offset scroll-view)
                               (current-min-position vscroll))
                         (invalidate scroll-view)))
    (add-event-handler scroll-view :scroll-view-offset-changed nil
                       (lambda (o evt)
                         (declare (ignore o evt))
                         (setf (current-min-position vscroll)
                               (vertical-offset scroll-view))
                         (setf (current-min-position hscroll)
                               (horizontal-offset scroll-view))))
    (values grid scroll-view)))

(defun scrollbar-ex-2 ()
  (let ((comp (stack
                 (background (list 1.0 1.0 1.0)
                             (filler))
                 (uniform-padding
                  0
                  (horizontal-pack
                   0 '(:auto)
                   (list
                    (vertical-pack 0
                                   '(:auto :auto :auto :auto :auto)
                                   (list
                                    (label "lala" :size 10)
                                    (label "lalal" :size 20)
                                    (label "lala" :size 30)
                                    (label "lala" :size 40)
                                    (label "lala" :size 50)
                                    (label "lalalal" :size 80)))))))))
    (scenic.renderer:with-renderer scenic.renderer:rect-renderer
      (run-component (scrollable comp) :title "Scrollbar"))))

;; (scrollbar-ex-2)

(defun scrollbar-ex-3 ()
  (let ((comp (stack
               (background (list 1.0 1.0 1.0)
                           (filler))
               (uniform-padding
                0
                (horizontal-pack
                 0 '(:auto)
                 (list
                  (vertical-pack 0
                                 '(:auto :auto :auto :auto :auto)
                                 (list
                                  (label "lala" :size 10)
                                  (label "lalal" :size 20)
                                  (label "lala" :size 30)
                                  (label "lala" :size 40)
                                  (label "lala" :size 50)
                                  (label "lalalal" :size 80)))))))))
    (scenic.renderer:with-renderer scenic.renderer:rect-renderer
      (run-component (scrollable comp
                                 :always-horizontal-scrollbar nil
                                 :always-vertical-scrollbar nil)
                     :title "Scrollbar"))))

;; (scrollbar-ex-3)
