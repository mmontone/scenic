;; * Stylesheets

(in-package :scenic.nui)

;; ** Examples

(scenic.style:defstylesheet styles-test
  ("background"
   :background-color "blue")
  ("background#background-1"
   :background-color "blue")
  ("#bg1" :background-color "blue")
  ("label#label1" :font-size 20 :font-color "white"))

#+scenic-test(progn
(let ((component (background cl-colors:+red+ (uniform-padding 10 (label "Hello" :name "label1")) :name "bg1")))
  (scenic.style:find-widgets "label#label1" component)
  (scenic.style:find-widgets "stack button" component)
  (scenic.style:find-widgets "grid > button" component)
  ;;(scenic.style:apply-stylesheet (scenic.style:find-stylesheet 'styles-demo) component)
  (let ((scene (scenic:scene 200 200 component :stylesheet (scenic.style:find-stylesheet 'styles-test))))
    (scenic:run-scene scene)))

(let ((component (glass 0.3 (background cl-colors:+blue+
                                        (uniform-padding 10
                                                         (sizer 
                                                          (label "Hello" :name "label1" :color cl-colors:+white+ :face "Serif" :weight :bold)
                                                          :min-width 100
                                                          :min-height 100))))))
  (let ((scene (scenic:scene 200 200 component)))
    (scenic:run-scene scene)))

(let ((component (glass 1 (background cl-colors:+blue+
                                        (uniform-padding 10
                                                         (label "Hello world!!" :name "label1" :color cl-colors:+white+ :face "Purisa" :weight :bold))))))
  (let ((scene (scenic:scene 200 200 component)))
    (scenic:run-scene scene)))



(let* ((component (glass 0 (background cl-colors:+blue+
                                      (uniform-padding 10
                                                       (label "Hello world!!" :name "label1" :color cl-colors:+white+ :face "Purisa" :weight :bold)))))
       (scene (scenic:scene 500 500
                            component)))
  
  (flet ((fadein (&key scene target &allow-other-keys)
           (incf (scenic::opacity target) 0.1)
           (scenic::invalidate target))
         (fadeout (&key scene target &allow-other-keys)
           (setf (scenic::opacity target) 0.1)
           ;;(decf (scenic::opacity target) 0.1)
           (format t "~A~%" (scenic::opacity target))
           (scenic::invalidate target)))
         
    (scenic::add-step-function
     (scenic::step-function #'fadeout
                            :target component
                            :after nil
                            :by 1000
                            :times 100)
     scene)
    (scenic:run-scene scene))))
