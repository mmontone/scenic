;;-- * Tab view

(in-package :scenic.nui)

;;-- ** Definition

(defwidget tab-button (nui-widget clickable scenic::container1)
  ((button-type :initarg :type
                :accessor button-type
                :initform :secondary
                :type (member :secondary :primary))
   (button-style :initarg :style
                 :accessor button-style
                 :initform :normal
                 :type (member :normal :flat))
   (button-shadow :initarg :shadow
                  :initform nil
                  :accessor button-shadow
                  :documentation "Button shadow")
   (active :accessor tab-active-p :initarg :active :initform nil)))

(defwidget tab-view (nui-widget)
  ((tabs :initarg :tabs :initform (error "Provide the tabs") :accessor tabs)
   (active-tab :initarg :active-tab :initform 0 :accessor active-tab)
   (%tab-buttons)
   (%tab-content)))

;;-- ** Rendering

(defmethod renderer-paint ((renderer scenic.renderer::rect-renderer) (button tab-button))
  (scenic.style:with-widget-styles button
    (let* ((left (layout-left button))
           (top (layout-top button))
           (width (layout-width button))
           (height (layout-height button))
           (pressed (eql (scenic::click-state button) :half-click))
           (over (eql (scenic::click-state button) :over))
           (radius 3)
           (line-width 2)
           (color (mk-color
                   (if (tab-active-p button)
                       (style-val :color :status :active)
                       (style-val :color))
                   :width width :height height)))
      (scenic-utils::draw-rect
       left top width height
       :stroke-width nil
       :stroke-color nil
       :fill-color color
       :corner-radius nil))))

(defmethod renderer-paint ((renderer scenic.renderer:gtk-renderer) (button tab-button))
  (scenic.style:with-widget-styles button
    (let* ((left (layout-left button))
           (top (layout-top button))
           (width (layout-width button))
           (height (layout-height button))
           (pressed (eql (scenic::click-state button) :half-click))
           (over (eql (scenic::click-state button) :over))
           (radius 3)
           (line-width 2)
           (color (mk-color
                   (if (tab-active-p button)
                       (style-val :color :status :active)
                       (style-val :color))
                   :width width :height height)))

      (scenic.draw:new-path)

      (scenic.draw:set-source-color cl-colors:+white+)

      (scenic.draw:set-line-width line-width)

      (scenic.draw:arc (+ left radius)
                       (+ top radius)
                       radius
                       (/ (* 2 pi) 2)
                       (/ (* 3 pi) 2))

      (scenic.draw:arc (- (+ left width) radius)
                       (+ top radius)
                       radius
                       (/ (* 3 pi) 2)
                       (/ (* 4 pi) 2))

      (scenic.draw:line-to (+ left width)
                           (+ top height))

      (scenic.draw:line-to left
                           (+ top height))

      (scenic.draw:close-path)
      (scenic.draw:stroke-preserve)

      (scenic.draw:set-source-color color)

      (scenic.draw:fill-path)

      )))


(defmethod scenic::paint ((tab-view tab-view))
  )

(defmethod scenic::paint-order-walk ((object tab-view) callback &key (after-callback nil))
  (when (funcall callback object)
    (scenic::paint-order-walk (slot-value object '%tab-buttons) callback :after-callback after-callback)
    (scenic::paint-order-walk (slot-value object '%tab-content) callback :after-callback after-callback))
  (when after-callback
    (funcall after-callback object)))

;;-- ** Initialization

(defmethod initialize-instance :after ((tab-view tab-view) &rest initargs)
  (setf (slot-value tab-view '%tab-buttons)
        (simple-horizontal-pack
         0
         (loop
            for i from 0 to (length (tabs tab-view))
            for tab in (tabs tab-view)
            for tab-active-p := (eql i (active-tab tab-view))
            collect (make-instance 'tab-button
                                   :child
                                   (uniform-padding 5
                                                    (aligner (label (first tab))))))))
  (setf (scenic:parent (slot-value tab-view '%tab-buttons)) tab-view)

  (setf (slot-value tab-view '%tab-content)
        (background cl-colors:+lightgray+
                    (uniform-padding 20
                                     (second (nth (active-tab tab-view) (tabs tab-view))))))
  (setf (scenic:parent (slot-value tab-view '%tab-content)) tab-view))

;;-- ** Layout

(defmethod renderer-measure (renderer (object tab-button) available-width available-height)
  (multiple-value-bind (width height)
      (measure (child object) available-width available-height)
    (set-measured object width height)))

(defmethod renderer-layout (renderer (object tab-button) left top width height)
  (layout (child object) left top width height)
  #+nil(case (scenic::click-state object)
    (:neutral (layout (child object)
                      (+ 1 left) (+ 1 top)
                      (- width 3) (- height 3)))
    (:half-click (layout (child object)
                         (+ 2 left) (+ 2 top)
                         (- width 3) (- height 3))))
  (set-layout object left top width height))

(defmethod scenic:measure ((tab-view tab-view) available-width available-height)
  (measure (slot-value tab-view '%tab-buttons) available-width 50)
  (measure (slot-value tab-view '%tab-content) available-width (- available-height 50))
  (set-measured tab-view available-width available-height))

(defmethod scenic:layout ((tab-view tab-view) left top width height)
  (layout (slot-value tab-view '%tab-buttons) left top width 50)
  (layout (slot-value tab-view '%tab-content) left (+ top 50) width (- height 50))
  (set-layout tab-view left top width height))

(defwidget tab-view-2 (scenic::container1)
  ((tabs :initarg :tabs :initform (error "Provide the tabs") :accessor tabs)
   (active-tab :initarg :active-tab :initform 0 :accessor active-tab)
   (%tab-buttons)
   (%tab-content)))

(defmethod initialize-instance :after ((tab-view tab-view-2) &rest initargs)
  (let ((tab-buttons (simple-horizontal-pack
                      0
                      (loop
                         for i from 0 to (length (tabs tab-view))
                         for tab in (tabs tab-view)
                         for tab-active-p := (eql i (active-tab tab-view))
                         collect (make-instance 'tab-button
                                                :active tab-active-p
                                                :child
                                                (uniform-padding 10
                                                                 (aligner (label (first tab))))))))
        (tab-content (background cl-colors:+lightgray+
                                 (uniform-padding 20
                                                  (second (nth (active-tab tab-view)
                                                               (tabs tab-view)))))))


    (setf (slot-value tab-view '%tab-buttons)
          tab-buttons)

    (setf (slot-value tab-view '%tab-content)
          tab-content)

    (setf (scenic:child tab-view)
          (col ()
            ((1 :auto) tab-buttons)
            ((1 :ext) tab-content)))))

(defmethod scenic::paint ((tab-view tab-view-2))
  )



(scenic::defwidget tab-view-3 ()
  ((tabs :initarg :tabs
         :initform (error "Provide the tabs")
         :accessor tabs
         :on-change (:rebuild))
   (active-tab :initarg :active-tab
               :initform 0
               :accessor active-tab
               :on-change (:rebuild))
   (%child :accessor %child :initform nil :child t)))

(defmethod scenic::build ((tab-view tab-view-3))
  (let ((tab-buttons (simple-horizontal-pack
                      0
                      (loop
                         for i from 0 to (1- (length (tabs tab-view)))
                         for tab in (tabs tab-view)
                         for tab-active-p := (eql i (active-tab tab-view))
                         collect
                           (let ((tab-button (make-instance 'tab-button
                                                            :active tab-active-p
                                                            :child
                                                            (uniform-padding 10
                                                                             (aligner (label (first tab)))))))
                             (on tab-button :click
                                 (let ((j i))
                                   (lambda (&rest args)
                                     (setf (active-tab tab-view) j))))
                             tab-button))))
        (tab-content (background cl-colors:+lightgray+
                                 (uniform-padding 20
                                                  (second (nth (active-tab tab-view)
                                                               (tabs tab-view)))))))

    (setf (%child tab-view)
          (col ()
            ((1 :auto) tab-buttons)
            ((1 :ext) tab-content)))
    ))

(defmethod scenic:measure ((tab-view tab-view-3) available-width available-height)
  (multiple-value-bind (width height)
      (scenic:measure (%child tab-view) available-width available-height)
    (set-measured tab-view width height)))

(defmethod scenic::layout ((object tab-view-3) left top width height)
  (scenic::layout (%child object)
                  left
                  top
                  width
                  height)
  (scenic::set-layout object left top width height))

(defun tabview-ex-1 ()

  (let ((tab-view (make-instance 'tab-view-3
                                 :tabs (list (list "Foo" (label "Foo"))
                                             (list "Bar" (label "Bar"))
                                             (list "Baz" (label "Baz"))))))
    (run-component tab-view)))

(defun tabview-ex-2 ()
  (let ((tab-view (make-instance 'tab-view
                                 :tabs (list (list "Foo" (label "Foo"))
                                             (list "Bar" (label "Bar"))
                                             (list "Baz" (label "Baz"))))))
    (run-component tab-view)))

(defun tabview-ex-3 ()
  (let ((tab-view (make-instance 'tab-view-2
                                 :tabs (list (list "Foo" (label "Foo"))
                                             (list "Bar" (label "Bar"))
                                             (list "Baz" (label "Baz"))))))
    (run-component tab-view)))

;; (tabview-ex-1)
