(in-package :scenic.renderer)

;;-- * Renderers

;;-- ** Overview
;;-- Renderers are in charge of drawing widgets. They contain default options.

;;-- The current renderer is bound to this variable:
(defvar *renderer*)

(defvar *renderers* (make-hash-table))

(defun find-renderer (name &optional (error-p t))
  (or (gethash name *renderers*)
      (and error-p
           (error "Renderer not defined: ~A" name))))

;;-- ** Using
;;-- Use a specific renderer via the macro *with-renderer*:

(defmacro with-renderer (name &body body)
  `(let ((*renderer* (find-renderer ',name)))
     ,@body))

(defun set-renderer (name-or-renderer)
  (check-type name-or-renderer (or symbol renderer))
  "Set the renderer globally"
  (setf *renderer* (if (symbolp name-or-renderer)
                       (find-renderer name-or-renderer)
                       name-or-renderer)))

;;-- ** Definition

;;-- The top-level renderer class. Nothing interesting here:

(defclass renderer ()
  ((stylesheet :initarg :stylesheet
               :accessor renderer-stylesheet
               :initform nil)))

(defmacro defrenderer (name super slots &rest options)
  `(progn
     (defclass ,name ,super
       ,slots
       ,@options)
     (setf (gethash ',name *renderers*) (make-instance ',name))))

;;-- ** Default renderer

(scenic.style::deftheme default-theme ()
  (panel (color (color "#d6d6d6")))
  (scenic.nui::nui-button
   (padding '(5 10))
   (color (color "#f3f3f3")
          :pushed (color "#b8b8b8")
          :hover (color "#b1b1b1"))
   (font (font "Helvetica" :regular 13 cl-colors:+black+))
   (border (border 1 :solid (color "#b1b1b1"))))
  (scenic.nui::listview
      (background-color cl-colors:+white+)
    (border nil :focus (border 1 :solid cl-colors:+blue+)))
  (scenic.nui::li (background-color cl-colors:+white+)
      (padding (padding 5))
      (background-color cl-colors:+white+
                        :selected cl-colors:+lightblue+)
      (font-color cl-colors:+black+
                  :selected cl-colors:+white+)))

(defrenderer default-renderer (renderer)
  ()
  (:default-initargs :stylesheet (scenic.style::find-theme 'default-theme))
  (:documentation "Default renderer"))

;;-- ** GTK renderer
;;-- This is a GTK-like renderer

(scenic.style::deftheme gtk-theme ()
  (scenic.nui::nui-button
   (:padding (padding 5 10))
   (:corner-radius 3)
   (:color (linear-gradient :bottom
                            (color "#f3f3f3")
                            (color "#dadada"))
           :pushed (color "#b8b8b8")
           :hover (color "#b1b1b1")
           :disabled cl-colors:+lightgrey+)
   (:font (font "Helvetica" :regular 13 cl-colors:+black+))
   (:border (border 1 :solid (color "#b1b1b1"))))
  (panel 
    (:color (color "#d6d6d6")))
  (scenic.nui::tab-button
   (:color (color "#d7d7d7")
           :active (color "#ececec"))))

(defrenderer gtk-renderer (renderer)
  ()
  (:default-initargs :stylesheet (scenic.style::find-theme 'gtk-theme))
  (:documentation "GTK like renderer"))

;;-- ** Windows renderer

(scenic.style:deftheme wnd7-theme ()
  (scenic.nui::nui-button
   (:padding (padding 5 10))
   (:corner-radius 3)
   (:color (linear-gradient :bottom
                            (color "#f3f3f3") (color "#dadada"))
           :pushed (color "#b8b8b8")
           :hover (color "#b1b1b1"))
   (:font (font "Helvetica" :regular 13 cl-colors:+black+))
   (:border (border 1 :solid (color "#b1b1b1")))
   (:primary-color (color "#bde6fb"))
   (:primary-border (border 1 :solid (color "#7fa2ba"))))
  (panel
   (:color (color "#d6d6d6"))))

(defrenderer wnd7-renderer (renderer)
  ()
  (:default-initargs :stylesheet (scenic.style:find-theme 'wnd7-theme)))

;;-- ** Material renderer

(deftheme material-theme ()
  (scenic.nui::nui-button
   (:padding (padding 5 10))
   (:corner-radius 3)
   (:color (list :linear-gradient :bottom
                 (color "#f3f3f3") (color "#dadada"))
           :pushed (color "#b8b8b8")
           :hover (color "#b1b1b1"))
   (:font (font "Helvetica" :regular 13 cl-colors:+black+))
   (:border (border 1 :solid (color "#b1b1b1")))
  (:primary-border (border 1 :solid (color "#7fa2ba")))
  (:primary-color (color "#bde6fb")))
  (panel
   (:color (color cl-colors:+white+))))

(defrenderer material-renderer (renderer)
  ()
  (:default-initargs :stylesheet (scenic.style:find-theme 'material-theme)))

;;-- ** Rect renderer
;;-- This is a very simple squared and flat renderer. The idea is to render widgets as simply and plain as possible (using just rectangles, nothing fancy)

(deftheme rect-theme (renderer)
  (scenic.nui::nui-button
   (:padding (padding 5 10))
   (:color (color "#dadada")
           :pushed (color "#b8b8b8")
           :hover (color "#b1b1b1"))
   (:font (font "Helvetica" :regular 13 cl-colors:+black+))
   (:border (border 1 :solid (color "#b1b1b1")))
   (:corner-radius nil)
   (:primary-border (border 1 :solid (color "#7fa2ba")))
   (:primary-color (color "#bde6fb")))
  (panel (:color cl-colors:+lightgray+))
  (scrollbar (:width 4))
  (slider (:color (color "#dadada")
                  :pushed (color "#b8b8b8")
                  :hover (color "#b1b1b1")))
  (scenic.nui::tab-button
   (:color (color "#b8b8b8")
           :active (color "#dadada")
           :hover (color "#b1b1b1"))))

(defrenderer rect-renderer (renderer)
  ()
  (:default-initargs :stylesheet (find-theme 'rect-theme)))

(setf *renderer* (make-instance 'default-renderer))
