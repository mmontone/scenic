(in-package :sdl2-ffi)

(cl:eval-when (:compile-toplevel :load-toplevel :execute)
  (cl:defconstant +sdl2-window-skip-taskbar+ #x00010000)
  (cl:defconstant +sdl2-window-popup-menu+ #x00080000)
  (cl:defconstant +sdl2-window-tooltip+ #x00040000)

  (AUTOWRAP:DEFINE-FOREIGN-FUNCTION
      '(SDL-SET-WINDOW-MODAL-FOR "SDL_SetWindowModalFor") ':INT
    '((MODAL_WINDOW (:POINTER SDL-WINDOW))
      (PARENT_WINDOW (:POINTER SDL-WINDOW))))
  
  (AUTOWRAP:DEFINE-CFUN SDL-SET-WINDOW-MODAL-FOR
      SDL2-FFI.FUNCTIONS))
