(defpackage :scenic.sdl2-nvg
  (:use :cl)
  (:export #:run-scene))

(in-package :scenic.sdl2-nvg)

(defun render-scene (scene &optional force-repaint-all)
  (when (scenic::dirty scene)
    (setf (scenic::dirty scene) nil)
    (scenic.draw:with-drawing-context (scenic::sdl-window scene)
      (scenic::prepare-scene scene)
      (when force-repaint-all
        (scenic::invalidate-scene scene))
      (scenic::paint-scene scene))))
    

(defun translated-mods ()
                                        ;;(mapcar #'sdl-translate-key (sdl:mods-down-p))
  )

(defun init-scene (scene)
  (scenic::calculate-focusables scene)
  (scenic::reset-session-record)
  (scenic::reset-tasks)
  (scenic::reset-threads)
  (when (scenic::on-scene-init scene)
    (funcall (scenic::on-scene-init scene))))

(defclass scenic-window (sdl2.kit:gl-window)
  ((scene :initarg :scene :accessor scene)
   (event-queue :initform nil :accessor event-queue)
   (stop-sdl-on-close :initform nil :accessor stop-sdl-on-close
                      :initarg :stop-sdl-on-close :type boolean))
  (:default-initargs :title "Scenic"))

(defmethod initialize-instance :after ((window scenic-window) &rest initargs)
  (setf (scenic::sdl-window (scene window))
        (sdl2.kit:sdl-window window))
  (setf (sdl2.kit:idle-render window) t)
  (init-scene (scene window)))

(defmethod sdl2.kit:other-event :around ((window scenic-window) event)
  (let ((scenic::*scene* (scene window)))
    (call-next-method)))

(defmethod sdl2.kit:other-event ((window scenic-window) event)
  (with-slots (scene) window
    (scenic::handle-events scene (nreverse (event-queue window)) #'render-scene)
    (setf (event-queue window) nil)
    (scenic::run-step-functions scene)
    (scenic::execute-ui-tasks scene)
    (render-scene scene)))

(defmethod sdl2.kit:render :around ((window scenic-window))
  (let ((scenic::*scene* (scene window)))
    (call-next-method)))

(defmethod sdl2.kit:render :after ((window scenic-window))
  (with-slots (scene) window
    (with-slots (scene) window
      (scenic::handle-events scene (nreverse (event-queue window)) #'render-scene)
      (setf (event-queue window) nil)
      (scenic::run-step-functions scene)
      (scenic::execute-ui-tasks scene)
      (render-scene scene)

      ;; The following delay seems to have an effect!!
      ;; Less CPU usage and not noticiable delay in UI
      ;; This is dynamic. Set delay only when there are no step functions to execute
      (when (not (scenic::step-functions scene))
        (sdl2:delay 10)))))

(defmethod sdl2.kit:window-event ((window scenic-window) type timestamp w h)
  (cond
    ((member type (list :exposed))
     (with-slots (scene) window
       (setf (scenic::layedout scene) nil)
       (scenic::invalidate-scene scene)
       (setf (scenic::rectangle-to-redraw scene) nil)
       (render-scene scene t)))
    ((eql type :resized)
     (with-slots (scene) window
       (setf (scenic:width scene) w)
       (setf (scenic:height scene) h)
       (gl:viewport 0 0 w h) 
       (setf (scenic::sdl-surface scene) nil)
       (setf (scenic::layedout scene) nil)
       (scenic::invalidate-scene scene)
       (setf (scenic::rectangle-to-redraw scene) nil)
       (render-scene scene t)))
    ((eql type :close)
     (sdl2.kit:close-window window)
     (when (stop-sdl-on-close window)
       (sdl2.kit:quit))
     )))

(defmethod sdl2.kit:mousebutton-event ((window scenic-window) state timestamp button x y)
  (push (make-instance
         'scenic::mouse-button-event
         :mouse-x x
         :mouse-y y
         :mouse-button button
         :button-state (ecase state
                         (:mousebuttondown :down)
                         (:mousebuttonup :up))
         :modifiers (translated-mods))
        (event-queue window)))

(defmethod sdl2.kit:mousemotion-event ((window scenic-window) timestamp button-mask x y xrel yrel)
  (push (make-instance 'scenic::mouse-move-event
                       :mouse-x x
                       :mouse-y y
                       :mouse-rel-x xrel
                       :mouse-rel-y yrel
                       :modifiers (translated-mods))
        (event-queue window)))

(defmethod sdl2.kit:mousewheel-event ((window scenic-window) timestamp x y)
  (push (make-instance 'scenic::mouse-button-event
                       :mouse-x x
                       :mouse-y y
                       :mouse-button (if (plusp y)
                                         4
                                         5)
                       :button-state :down
                       :modifiers (translated-mods))
        (event-queue window)))

;; (defmethod textinput-event ((window window) timestamp text))
;; (defmethod keyboard-event ((window window) state timestamp repeat-p keysym))

;; (defmethod controller-added-event ((window window) c))
;; (defmethod controller-removed-event ((window window) c))
;; (defmethod controller-axis-motion-event ((window window) c ts axis value))
;; (defmethod controller-button-event ((window window) c state ts button))

(defun sdl-translate-key (key)
  (intern (subseq (symbol-name key) 9) "KEYWORD"))

(defun sdl-translate-key-state (state)
  (ecase state
      (:keyup :up)
      (:keydown :down)))

(defun sdl-translate-key-mods (key-mods)
  (flet ((translate-key-mod (key-mod)
           (intern (format nil "MOD-~A" key-mod) :keyword)))
    (mapcar #'translate-key-mod (sdl2:mod-keywords key-mods))))

(defmethod sdl2.kit:keyboard-event ((window scenic-window) state timestamp repeat-p keysym)
  (push (make-instance
         'scenic::key-event
         :key (sdl-translate-key (sdl2:scancode keysym))
         :modifiers (sdl-translate-key-mods (sdl2:mod-value keysym)) ;(mapcar #'sdl-translate-key mod-key)
         :key-state (sdl-translate-key-state state)
         :unicode (ignore-errors (code-char (sdl2:sym-value keysym)))
         #+nil(if (= 0 unicode)
                  nil
                  (code-char unicode)))
        (event-queue window)))

(defun reset-sdl-cursor ()
  (sdl2-ffi.functions:sdl-set-cursor
   (sdl2-ffi.functions:sdl-create-system-cursor
    sdl2-ffi:+sdl-system-cursor-arrow+)))


(defun run-scene (scene &key position window-options (with-start nil))
  ;;(sdl-cffi:sdl-enable-key-repeat 80 80)
  ;;(setf (sdl:frame-rate) 100)
  ;;(scenic::init-scene scene #'render-scene)
  ;;(lispbuilder-sdl:enable-unicode)
  (flet ((make-window ()
           (apply #'make-instance 'scenic-window
                         :scene scene
                         :stop-sdl-on-close with-start
                         :w (scenic::width scene)
                         :h (scenic::height scene)
                         :x (or (and position (car position))
                                :centered)
                         :y (or (and position (cdr position))
                                :centered)
                         window-options)))
    (KIT.SDL2:INIT)
    (SDL2:IN-MAIN-THREAD NIL
      (SDL2:GL-SET-ATTR :CONTEXT-MAJOR-VERSION 3)
      (SDL2:GL-SET-ATTR :CONTEXT-MINOR-VERSION 3)
      (SDL2:GL-SET-ATTR :CONTEXT-PROFILE-MASK 1)
      (SDL2:GL-SET-ATTR :STENCIL-SIZE 8))
    (if with-start
        (kit.sdl2:with-start (:this-thread-p nil)
          (reset-sdl-cursor)
          (make-window))
        (progn
          (sdl2.kit:start)
          (reset-sdl-cursor)
          (make-window)))))

(defclass scenic-popup-window (scenic-window)
  ())

(defmethod sdl2.kit::additional-window-flags append ((window scenic-popup-window))
  (list sdl2-ffi::+sdl2-window-popup-menu+))

(defun popup-scene (scene &key position window-options (with-start nil))
  (flet ((make-window ()
          (apply #'make-instance 'scenic-popup-window
                         :scene scene
                         :stop-sdl-on-close with-start
                         :w (scenic::width scene)
                         :h (scenic::height scene)
                         :x (or (and position (car position))
                                :centered)
                         :y (or (and position (cdr position))
                                :centered)
                         window-options)))
    (if with-start
        (kit.sdl2:with-start (:this-thread-p nil)
          (make-window))
        (progn
          (sdl2.kit:start)
          (make-window)))))

(defun open-modal (scene &key position window-options)
  (let ((current-window (sdl2.kit:focused-window)))
    (flet ((make-window ()
             (let ((wnd
                    (apply #'make-instance 'scenic-window
                           :scene scene
                           ;;:stop-sdl-on-close nil
                           :w (scenic::width scene)
                           :h (scenic::height scene)
                           :x (or (and position (car position))
                                  :centered)
                           :y (or (and position (cdr position))
                                  :centered)
                           window-options)))
               (when current-window
                 (sdl2-ffi.functions::SDL-SET-WINDOW-MODAL-FOR
                  (sdl2.kit:sdl-window wnd)
                  (sdl2.kit:sdl-window current-window)))
               wnd)))
      (sdl2.kit:start)
      (make-window))))

(defun scenic::run-scene (&rest args)
  (apply #'run-scene args))

(defun modal-test ()
  (let ((button (scenic-helpers:button
                 (scenic-helpers:uniform-padding
                  5
                  (scenic-helpers:label "Open dialog"))))
        (dialog
         (scenic-helpers:stack
          (scenic-helpers:background cl-colors:+lightgray+
                                     (scenic-helpers:filler))
          (scenic-helpers:label "This is a modal dialog"))))
    (scenic:add-event-handler button :click :bubble
                              (lambda (&rest args)
                                (let ((scene (scenic:scene 300 100 dialog)))
                                  (open-modal scene))))
    (scenic:run-scene
     (scenic:scene 100 100 button))))
