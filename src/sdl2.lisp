(defpackage :scenic.sdl2
  (:use :cl)
  (:export #:run-scene))

(in-package :scenic.sdl2)

(defvar *sdl-renderer*)

(defun render-scene (scene &optional force-repaint-all)
  (unless (scenic::sdl-surface scene)
    (setf (scenic::sdl-surface scene)
          (sdl2:create-rgb-surface
           (scenic:width scene)
           (scenic:height scene)
           32
           :r-mask #x00ff0000
           :g-mask #x0000ff00
           :b-mask #x000000ff
           :a-mask 0)))
  (when (scenic::dirty scene)
    ;;(format t "Rendering scene~%")
    (setf (scenic::dirty scene) nil)
    (scenic.draw:with-drawing-context (scenic::sdl-surface scene)
      (scenic::prepare-scene scene)
      (when force-repaint-all
        (scenic::invalidate-scene scene))
      (scenic::paint-scene scene))
    (if (and (scenic::rectangle-to-redraw scene) nil)
        (let* ((scene-rect (mapcar (lambda (value)
                                     (the fixnum (round value)))
                                   (scenic::rectangle-to-redraw scene)))
               (left (first scene-rect))
               (top (second scene-rect))
               (width (1+ (- (third scene-rect) (first scene-rect))))
               (height (1+ (- (fourth scene-rect) (second scene-rect))))
               (sdl-rect (sdl2:make-rect left top
                                         width height)))

          (sdl2-ffi.functions:sdl-render-set-clip-rect
           (sdl2:get-renderer (scenic::sdl-window scene))
           sdl-rect)
                                        ;(sdl:set-clip-rect sdl-rect :surface lispbuilder-sdl:*default-display*)
                                        ;(sdl2:blit-surface (sdl-surface scene) )
          (sdl2:free-rect sdl-rect)

          ;;(sdl:clear-clip-rect lispbuilder-sdl:*default-display*)
          (setf (scenic::rectangle-to-redraw scene) nil)

                                        ;(sdl-cffi::sdl-update-rect (sdl::fp lispbuilder-sdl:*default-display*)
                                        ;                           left top width height)

          )
        ;; else
        (let ((from-rect (sdl2:make-rect 0 0
                                         (scenic:width scene)
                                         (scenic:height scene)))
              (to-rect (sdl2:make-rect 0 0
                                       (scenic:width scene)
                                       (scenic:height scene))))
                                        ;(format t "Updating display~%")
          (let ((texture (sdl2:create-texture-from-surface
                          *sdl-renderer*
                          (scenic::sdl-surface scene))))
            (sdl2:render-clear *sdl-renderer*)
            (sdl2:render-copy *sdl-renderer* texture :source-rect from-rect :dest-rect to-rect)
            (sdl2:render-present *sdl-renderer*))))))

(defun translated-mods ()
                                        ;(mapcar #'sdl-translate-key (sdl:mods-down-p))
  )

(defun init-scene (scene)
  (scenic::calculate-focusables scene)
  (scenic::reset-session-record)
  (scenic::reset-ui-tasks)
  ;; We should deactivate many of these resets for SDL2, as multiple concurrent
  ;; scenes are supported.
  ;;(scenic::reset-threads)
  (when (scenic::on-scene-init scene)
    (funcall (scenic::on-scene-init scene))))

(defclass scenic-window (sdl2.kit:window)
  ((scene :initarg :scene :accessor scene)
   (event-queue :initform nil :accessor event-queue)
   (sdl-renderer :initform nil :accessor sdl-renderer)
   (stop-sdl-on-close :initform nil :accessor stop-sdl-on-close
                      :initarg :stop-sdl-on-close :type boolean))
  (:default-initargs :title "Scenic"))

(defmethod initialize-instance :after ((window scenic-window) &rest initargs)
  (setf (scenic::sdl-window (scene window))
        (sdl2.kit:sdl-window window))
  (setf (sdl2.kit:idle-render window) t)
  (setf (sdl-renderer window)
        (sdl2:create-renderer (sdl2.kit:sdl-window window)
                              nil
                              '(:accelerated :presentvsync)))
  (init-scene (scene window)))

(defmethod sdl2.kit:other-event :around ((window scenic-window) event)
  (let ((*sdl-renderer* (sdl-renderer window))
        (scenic::*scene* (scene window)))
    (call-next-method)))

(defmethod sdl2.kit:other-event ((window scenic-window) event)
  (with-slots (scene) window
    (scenic::handle-events scene (nreverse (event-queue window))
                           ;;#'render-scene
                           ;; We avoid passing the actual renderer here
                           ;; Handling events with the renderer makes events handling slower
                           #'identity)
    (setf (event-queue window) nil)
    (scenic::run-step-functions scene)
    (scenic::execute-ui-tasks scene)
    (render-scene scene)))

(defmethod sdl2.kit:render :around ((window scenic-window))
  (let ((*sdl-renderer* (sdl-renderer window))
        (scenic::*scene* (scene window)))
    (call-next-method)))

(defmethod sdl2.kit:render :after ((window scenic-window))
  (with-slots (scene) window
    (with-slots (scene) window
      (scenic::handle-events scene (nreverse (event-queue window))
                             ;;#'render-scene
                             ;; We avoid passing the actual renderer here
                             ;; Handling events with the renderer makes events handling slower
                             #'identity)
      (setf (event-queue window) nil)
      (scenic::run-step-functions scene)
      (scenic::execute-ui-tasks scene)
      (render-scene scene)
      ;; The following delay seems to have an effect!!
      ;; Less CPU usage and not noticiable delay in UI
      ;; This is dynamic. Set delay only when there are no step functions to execute
      ;;(when (not (scenic::step-functions scene))
      ;;  (sdl2:delay 10))
      )))

(defmethod sdl2.kit:window-event :around ((window scenic-window) type timestamp data1 data2)
  (let ((*sdl-renderer* (sdl-renderer window)))
    (call-next-method)))

(defmethod sdl2.kit:window-event ((window scenic-window) type timestamp w h)
  (cond
    ((member type (list :exposed))
     (with-slots (scene) window
       (setf (scenic::layedout scene) nil)
       (scenic::invalidate-scene scene)
       (setf (scenic::rectangle-to-redraw scene) nil)
       (render-scene scene t)))
    ((eql type :resized)
     (with-slots (scene) window
       (setf (scenic:width scene) w)
       (setf (scenic:height scene) h)
       (setf (scenic::sdl-surface scene) nil)
       (setf (scenic::layedout scene) nil)
       (scenic::invalidate-scene scene)
       (setf (scenic::rectangle-to-redraw scene) nil)
       (render-scene scene t)))
    ((eql type :close)
     (sdl2.kit:close-window window)
     (sdl2:destroy-renderer (sdl-renderer window))
     (when (stop-sdl-on-close window)
       (sdl2.kit:quit)))))

(defmethod sdl2.kit:mousebutton-event ((window scenic-window) state timestamp button x y)
  (push (make-instance
         'scenic::mouse-button-event
         :mouse-x x
         :mouse-y y
         :mouse-button button
         :button-state (ecase state
                         (:mousebuttondown :down)
                         (:mousebuttonup :up))
         :modifiers (translated-mods))
        (event-queue window)))

(defmethod sdl2.kit:mousemotion-event ((window scenic-window) timestamp button-mask x y xrel yrel)
  (push (make-instance 'scenic::mouse-move-event
                       :mouse-x x
                       :mouse-y y
                       :mouse-rel-x xrel
                       :mouse-rel-y yrel
                       :modifiers (translated-mods))
        (event-queue window)))

(defmethod sdl2.kit:mousewheel-event ((window scenic-window) timestamp x y)
  (multiple-value-bind (rx ry) (sdl2:mouse-state)
    (push (make-instance 'scenic::mouse-button-event
                         :mouse-x rx
                         :mouse-y ry
                         :mouse-button (if (plusp y)
                                           4
                                           5)
                         :button-state :down
                         :modifiers (translated-mods))
          (event-queue window))))

;; (defmethod textinput-event ((window window) timestamp text))
;; (defmethod keyboard-event ((window window) state timestamp repeat-p keysym))

;; (defmethod controller-added-event ((window window) c))
;; (defmethod controller-removed-event ((window window) c))
;; (defmethod controller-axis-motion-event ((window window) c ts axis value))
;; (defmethod controller-button-event ((window window) c state ts button))

(defun sdl-translate-key (key)
  (intern (subseq (symbol-name key) 9) "KEYWORD"))

(defun sdl-translate-key-state (state)
  (ecase state
    (:keyup :up)
    (:keydown :down)))

(defun sdl-translate-key-mods (key-mods)
  (flet ((translate-key-mod (key-mod)
           (intern (format nil "MOD-~A" key-mod) :keyword)))
    (mapcar #'translate-key-mod (sdl2:mod-keywords key-mods))))

(defmethod sdl2.kit:keyboard-event ((window scenic-window) state timestamp repeat-p keysym)
  (push (make-instance
         'scenic::key-event
         :key (sdl-translate-key (sdl2:scancode keysym))
         :modifiers (sdl-translate-key-mods (sdl2:mod-value keysym)) ;(mapcar #'sdl-translate-key mod-key)
         :key-state (sdl-translate-key-state state)
         :unicode (ignore-errors (code-char (sdl2:sym-value keysym)))
         #+nil(if (= 0 unicode)
                  nil
                  (code-char unicode)))
        (event-queue window)))

(defun reset-sdl-cursor ()
  (sdl2-ffi.functions:sdl-set-cursor
   (sdl2-ffi.functions:sdl-create-system-cursor
    sdl2-ffi:+sdl-system-cursor-arrow+)))


(defun run-scene (scene &key position window-options (with-start nil))
  ;;(sdl-cffi:sdl-enable-key-repeat 80 80)
  ;;(setf (sdl:frame-rate) 100)
  ;;(scenic::init-scene scene #'render-scene)
  ;;(lispbuilder-sdl:enable-unicode)
  (flet ((make-window ()
           (apply #'make-instance 'scenic-window
                  :scene scene
                  :stop-sdl-on-close with-start
                  :w (scenic::width scene)
                  :h (scenic::height scene)
                  :x (or (and position (car position))
                         :centered)
                  :y (or (and position (cdr position))
                         :centered)
                  window-options)))
    (if with-start
        (kit.sdl2:with-start (:this-thread-p nil)
          (reset-sdl-cursor)
          (make-window))
        (progn
          (sdl2.kit:start)
          (reset-sdl-cursor)
          (make-window)))))

(defclass scenic-popup-window (scenic-window)
  ())

(defmethod sdl2.kit::additional-window-flags append ((window scenic-popup-window))
  (list sdl2-ffi::+sdl2-window-popup-menu+))

(defun popup-scene (scene &key position window-options (with-start nil))
  (flet ((make-window ()
           (apply #'make-instance 'scenic-popup-window
                  :scene scene
                  :stop-sdl-on-close with-start
                  :w (scenic::width scene)
                  :h (scenic::height scene)
                  :x (or (and position (car position))
                         :centered)
                  :y (or (and position (cdr position))
                         :centered)
                  window-options)))
    (if with-start
        (kit.sdl2:with-start (:this-thread-p nil)
          (make-window))
        (progn
          (sdl2.kit:start)
          (make-window)))))

(defun scenic::open-modal (scene &key position window-options)
  (let ((current-window (sdl2.kit:focused-window)))
    (flet ((make-window ()
             (let ((wnd
                    (apply #'make-instance 'scenic-window
                           :scene scene
                           ;;:stop-sdl-on-close nil
                           :w (scenic::width scene)
                           :h (scenic::height scene)
                           :x (or (and position (car position))
                                  :centered)
                           :y (or (and position (cdr position))
                                  :centered)
                           window-options)))
               (when current-window
                 (sdl2-ffi.functions::SDL-SET-WINDOW-MODAL-FOR
                  (sdl2.kit:sdl-window wnd)
                  (sdl2.kit:sdl-window current-window)))
               wnd)))
      (sdl2.kit:start)
      (make-window))))

(defun scenic::close-modal (window)
  (sdl2.kit:close-window window))

(defun scenic::run-scene (&rest args)
  (apply #'run-scene args))

(defun modal-test ()
  (let ((button (scenic-helpers:button
                 (scenic-helpers:uniform-padding
                  5
                  (scenic-helpers:label "Open dialog"))))
        (dialog
         (scenic-helpers:stack
          (scenic-helpers:background cl-colors:+lightgray+
                                     (scenic-helpers:filler))
          (scenic-helpers:label "This is a modal dialog"))))
    (scenic:add-event-handler button :click :bubble
                              (lambda (&rest args)
                                (let ((scene (scenic:scene 300 100 dialog)))
                                  (open-modal scene))))
    (scenic:run-scene
     (scenic:scene 100 100 button))))

(defun popup-sdl-window (widget &key position width height)
  (let ((scene (scenic:scene
                (or width 500)
                (or height 500)
                widget)))
    ;; Hack
    (unless (scenic::sdl-surface scene)
      (setf (scenic::sdl-surface scene)
            (sdl2:create-rgb-surface
             (scenic:width scene)
             (scenic:height scene)
             32
             :r-mask #x00ff0000
             :g-mask #x0000ff00
             :b-mask #x000000ff
             :a-mask 0)))
    (scenic.draw:with-drawing-context (scenic::sdl-surface scene)
      (scenic::prepare-scene scene))
    (let ((width (round (scenic:measured-width (scenic:widget scene))))
          (height (round (scenic:measured-height (scenic:widget scene)))))
      (setf (scenic:width scene) width)
      (setf (scenic:height scene) height)
      (popup-scene
       scene
       :position position))))

(defmethod scenic::close-popup ((popup scenic-window))
  (sdl2.kit:close-window popup))

(defmethod scenic::close-popup ((popup scenic:widget))
  (scenic::remove-child
     (scenic::widget scenic::*scene*)
     popup)
  (setf (scenic::layedout scenic::*scene*) nil)
  (scenic:invalidate-scene scenic::*scene*)
  (scenic:invalidate (scenic::widget scenic::*scene*)))

(defun scenic::popup (widget &key overlay position sdl-window)
  (if (and sdl-window (member :scenic-sdl2 *features*))
      (popup-sdl-window widget :position position)
      (popup-mdi widget
                 :position position
                 :overlay overlay)))

(defun show-overlay (&optional color alpha)
  (scenic::add-child
   (scenic:widget scenic::*scene*)
   (scenic:background
    (scenic.draw::make-alpha-color
     :color
     (or color cl-colors:+black+)
     :alpha (or alpha 0.4))
    (scenic:filler))))

(defun popup-mdi (widget &key overlay position)
  (let ((popup (if position
                   (scenic:placer
                    (car position)
                    (cdr position)
                    widget)
                   (scenic:aligner widget))))
    (when overlay
      (show-overlay))
    (scenic::add-child
     (scenic::widget scenic::*scene*)
     ;;(scenic::placer left top widget)
     popup

     )

    ;; (scenic::capture-mouse instance)

    ;; (add-event-handler instance :mouse-button-down :bubble
    ;;                   (lambda (instance event)
    ;;                     (when (= 1 (mouse-button event))

    ;;                             )))
    ;;      (add-event-handler instance :mouse-button-up :bubble
    ;;                         (lambda (instance event)
    ;;                           (when (= 1 (mouse-button event))
    ;;                             (setf dragging nil)
    ;;                             (release-mouse instance))))
    ;;      (add-event-handler instance :mouse-move :bubble
    ;;                         (lambda (instance event)
    ;;                           (when dragging
    ;;                             (setf (layout-left instance) (max 0 (- (mouse-x event) inside-x)))
    ;;                             (setf (layout-top instance) (max 0 (- (mouse-y event) inside-y)))
    ;;                             (scenic::mark-for-layout (scenic:parent instance))
    ;;                             (scenic::invalidate (scenic:parent instance)))))))


    ;; This is VERY bad: why is it not working without this? (invalidations above are not taking effect)
    (setf (scenic::layedout scenic::*scene*) nil)
    (scenic:invalidate-scene scenic::*scene*)
    ;;(scenic::render-scene scene t)
    popup
    ))

(defun scenic::set-cursor (cursor)
  (sdl2-ffi.functions:sdl-set-cursor
   (sdl2-ffi.functions:sdl-create-system-cursor
    (symbol-value
     (intern (string-upcase (format nil  "+SDL-SYSTEM-CURSOR-~A+" cursor))
             'sdl2-ffi)))))

(defun scenic::window-position (window)
  (sdl2.kit:window-position window))

(defun scenic::focused-window ()
  (sdl2.kit:focused-window))

;; SDL2KIT patches

;; https://github.com/lispgames/sdl2kit/issues/8

(defun kit.sdl2:window-position (window)
  (with-slots (kit.sdl2:sdl-window) window
    (sdl2:get-window-position kit.sdl2:sdl-window)))

;; Custom main loop

(defun kit.sdl2::main-loop ()
  (let (kit.sdl2::*main-loop-quit*)
    (sdl2:with-sdl-event (ev)
      (loop as method = (if (> (hash-table-count kit.sdl2::*idle-render-windows*) 0)
                            :poll :wait)
            as rc = (sdl2:next-event ev method)
            as idle-p = (and (= 0 rc) (eq :poll method))
            do (handler-case
                   (progn
                     (main-loop-function (unless idle-p ev) idle-p)
                     (when kit.sdl2::*main-loop-quit*
                       (return-from kit.sdl2::main-loop)))
                 (sdl2:sdl-continue (c) (declare (ignore c))))))))

(defun main-loop-function (ev idle-p)
  (kit.sdl2::main-loop-function ev idle-p)
  ;; Apply delay only when no window has any step functions to execute
  (when (and (null ev)
             (not (some (lambda (window)
                     (scenic::step-functions (scene window)))
                        (kit.sdl2:all-windows))))
    (sdl2:delay 10)))
