(in-package :scenic)

;; To convert from gif to png sprite:
;; montage balls.gif -tile x1 -geometry '1x1+0+0<' -alpha On -background "rgba(0,0,0,0.0)" -quality 100 balls.png

(defwidget sprite (widget)
  ((file :initarg :file
         :accessor file
         :type pathname)
   (frames :initarg :frames
           :accessor frames
           :initform 20
           :type integer)
   (current-frame :accessor current-frame
                  :initform 0
                  :type integer)
   (speed :initarg :speed
          :accessor sprite-speed
          :initform 10)
   (image-surface :accessor image-surface)))

(defmethod initialize-instance :after ((widget sprite) &rest initargs)
  (setf (image-surface widget) (sdl2-image:load-image (file widget))))

(defmethod prepare :after ((widget sprite) scene)
  (setf (image-surface widget) (sdl2-image:load-image (file widget))))

(defmethod measure ((widget sprite) available-width available-height)
  (let ((frame-width (/ (sdl2:surface-width (image-surface widget))
                        (frames widget)))
        (frame-height (sdl2:surface-height (image-surface widget))))
    (set-measured widget (round frame-width) frame-height)))

(defun next-frame (&key target &allow-other-keys)
  (setf (current-frame target)
        (mod (1+ (current-frame target))
             (frames target)))
  (invalidate target))

(defmethod layout :after ((widget sprite) left top width height)
  (add-step-function 
   (scenic::step-function
    #'next-frame
    :target widget
    :by (sprite-speed widget)
    :times nil)
   (get-scene widget)))

(defmethod paint ((instance sprite))
  (let ((frame-width (/ (sdl2:surface-width (image-surface instance))
                        (frames instance)))
        (frame-height (sdl2:surface-height (image-surface instance))))
    (sdl2:blit-surface
     (image-surface instance)
     (sdl2:make-rect
      (* (current-frame instance) frame-width)
      0
      frame-width
      frame-height)
     (sdl-surface (get-scene instance))
     (sdl2:make-rect (layout-left instance)
                     (layout-top instance)
                     (layout-width instance)
                     (layout-height instance)))))

(defun sprite (file &rest args)
  (apply #'make-instance 'sprite :file file args))

(defun sprite-test (&optional (speed 60))
  (flet ((loader (file)
           (adjuster
            (sprite (asdf:system-relative-pathname
                     :scenic (format nil "icons/~A" file))
                    :speed speed))))
  (let ((sprites (background cl-colors:+white+
                             (scenic-helpers:uniform-padding
                              20
                              (pack (:orientation :vertical
                                                  :space-between-cells 20)
                                    (loader "balls.png")
                                    (loader "spin.png")
                                    (loader "spinner.png")
                                    (loader "rolling.png"))))))
    (run-scene (scene 500 500 sprites)))))
