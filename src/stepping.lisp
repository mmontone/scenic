(in-package :scenic)

;;-- ** Stepping functions

;;-- *** Overview

;;-- Stepping functions are used for animations. They are registered by widgets and invoked repeatidly in the future.
;;-- They have several scheduling options, like a fixed scheduled-time, times repetition, while conditions, etc

(defstruct step-function
  target
  (on-start nil :type (or null function))
  (on-end nil :type (or null function))
  (step (error "Provide the step function") :type function)
  (scheduled-time 0 :type integer)
  (by nil :type (or null integer))
  (times nil :type (or null integer))
  (while nil :type (or null function))
  (times-run 0 :type integer))

;;-- *** Creation

;;-- The standard way to create a stepping function is via the step-function function:

(defun step-function (function &key after by (times 1) while until target on-start on-end)
  (let ((scheduled-time (if after (+ (get-internal-real-time) after)
                            0)))
    (make-step-function :step function
                        :scheduled-time scheduled-time
                        :by by
                        :times times
                        :while (or (and until (alexandria:compose #'not until))
                                   while)
                        :target target
                        :on-start on-start
                        :on-end on-end)))

;;-- *** Execution

(defvar *next-step-functions*)

(defun run-step-function (step-function scene)
  (let ((time (get-internal-real-time)))
    (when (and (step-function-on-start step-function)
               (zerop (step-function-times-run step-function)))
      (funcall (step-function-on-start step-function)))
    (funcall (step-function-step step-function)
             :scene scene
             :time time
             :target (step-function-target step-function))
    (with-slots (target step by times while on-start on-end times-run) step-function
      (if (and (or (null times) (> times 0))
               (or (null while) (funcall while)))
          (push (make-step-function :target target
                                    :step step
                                    :scheduled-time (if by (+ (get-internal-real-time) by) 0)
                                    :while while
                                    :by by
                                    :times (and times (1- times))
                                    :times-run (1+ times-run)
                                    :on-start on-start
                                    :on-end on-end)
                *next-step-functions*)
          (when on-end
            (funcall on-end))))))

(defun run-step-functions (scene)
  (let ((*next-step-functions* nil)
        (now (get-internal-real-time))
        (step-functions (step-functions scene)))
    (dolist (step-function step-functions)
      (let ((scheduled-time (step-function-scheduled-time step-function)))
        (if (or (not scheduled-time)
                (<= scheduled-time now))
            (run-step-function step-function scene)
            ;; else
            (push step-function *next-step-functions*))))
    (setf (step-functions scene) *next-step-functions*)))

(defun add-step-function (step-function scene)
  (setf (step-functions scene)
        (sort (cons step-function (step-functions scene)) '< :key 'step-function-scheduled-time)))

;;-- *** Removal

(defun remove-step-function (step-function scene)
  (setf (step-functions scene)
        (delete step-function (step-functions scene))))
