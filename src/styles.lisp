(in-package :scenic.style)

(defvar *stylesheet* nil)
(defvar *stylesheets* (make-hash-table))
(defvar *ignore-stylesheet-errors* t "When T, no stylesheet errors are triggered. A warning is raised instead.")

(defclass sheet ()
  ((name :initarg :name
         :accessor sheet-name
         :initform (error "Provide the stylesheet name")
         :type symbol)
   (parent :initarg :parent
           :initform nil)))

(defmethod print-object ((sheet sheet) stream)
  (print-unreadable-object (sheet stream :type t :identity t)
    (format stream "~A" (sheet-name sheet))))                        

(defclass stylesheet (sheet)
  ((rules :initarg :rules
          :accessor stylesheet-rules
          :initform nil
          :type list)
   (sorted-rules :initform nil
                 :type list)
   (package :initarg :package
            :initform *package*
            :type (or symbol package))))

(defmethod stylesheet-package ((stylesheet stylesheet))
  (find-package (slot-value stylesheet 'package)))

(defclass style-rule ()
  ((selector :initarg :selector
             :accessor rule-selector
             :initform (error "Provide the selector"))
   (properties :initarg :properties
               :accessor rule-properties
               :initform nil
               :type list)))

(defmethod print-object ((stylesheet stylesheet) stream)
  (print-unreadable-object (stylesheet stream :type t :identity t)
    (format stream "~A (~A rules)"
            (sheet-name stylesheet)
            (length (stylesheet-rules stylesheet)))))

(defmethod print-object ((style-rule style-rule) stream)
  (print-unreadable-object (style-rule stream :type t :identity t)
    (format stream "~A" (rule-selector style-rule))))

(defgeneric stylesheet-sorted-rules (stylesheet &key sort-order force))

(defmethod stylesheet-sorted-rules ((stylesheet stylesheet) &key (sort-order #'<) force)
  (when (or (null (slot-value stylesheet 'sorted-rules)) force)
    (setf (slot-value stylesheet 'sorted-rules)
          (sort (copy-list (stylesheet-rules stylesheet))
                ;; Sort ready to apply. Less specific first, so that more specific come last and override the former
                sort-order
                :key 'style-rule-specificity)))
  (slot-value stylesheet 'sorted-rules))

(defun parse-style-rule-selector (selector)
  (check-type selector (or string list))
  (cond
    ((stringp selector)
     (css-selectors:parse-results selector))
    ;; TODO: do style validation here
    ((listp selector)
     selector)
    (t (error "Invalid style selector: ~S" selector))))

(defun make-style-rule (selector properties)
  (make-instance 'style-rule
                 :selector (parse-style-rule-selector selector)
                 :properties properties))

(defun find-stylesheet (name &key (error-p t))
  (check-type name symbol)
  (or (gethash name *stylesheets*)
      (when error-p
        (error "Stylesheet ~A not defined" name))))

(defun add-rule (rule stylesheet)
  (push rule (stylesheet-rules stylesheet))
  (setf (slot-value stylesheet 'sorted-rules) nil))

(defmacro defstylesheet (name args &body rules)
  (check-type name symbol)
  (let ((stylesheet (gensym)))
    `(let ((,stylesheet (make-instance 'stylesheet :name ',name ,@args)))
       (setf (gethash ',name *stylesheets*) ,stylesheet)
       (let ((*stylesheet* (find-stylesheet ',name)))
         ,@(loop
              for rule in rules
              collect (destructuring-bind (selector &body properties) rule
                        `(defstyle-rule ,(if (stringp selector)
                                             selector
                                             `(quote ,selector))
                           ,@properties))))
       ,stylesheet)))

(defmacro defstyle-rule (selector &body properties)
  (let ((style (gensym)))
    `(let ((,style (make-style-rule ',selector ',properties)))
       (when *stylesheet*
         (add-rule ,style *stylesheet*))
       ,style)))

(defmacro in-stylesheet (name)
  `(setf *stylesheet* (find-stylesheet ',name)))

(defstylesheet test (:package 'scenic)
  ((:CHILD (:AND (:ELEMENT "button") (:HASH "my-button")) (:ELEMENT "label"))
   :font-family "Verdana"
   :background-color cl-colors:+red+))

;; Stylesheets should work with direct symbol references too (for example, scenic:button)

(defstylesheet test (:package 'scenic)
  ((:CHILD (:AND (:ELEMENT 'scenic:button) (:HASH "my-button")) (:ELEMENT "label"))
   :font-family "Verdana"
   :background-color cl-colors:+red+))

;; (in-stylesheet test)

;; (defstyle-rule (:AND (:ELEMENT "button") (:CLASS "primary"))
;;   :background-color cl-colors:+blue2+)

;; (defstyle-rule "button.secondary"
;;   :background-color cl-colors:+green+)

;; (defstyle-rule "#button.primary"
;;   :background-color cl-colors:+red+)

;; (stylesheet-sorted-rules (find-stylesheet 'test))

(defun make-style-rule-matcher (tree)
  (ecase (typecase tree
           (atom tree)
           (list (car tree)))
    (:or (make-or-matcher (rest tree)))
    (:and (make-and-matcher (rest tree)))
    (:class (make-class-matcher (second tree)))
    (:hash (make-hash-matcher (second tree)))
    (:element (make-elem-matcher (second tree)))
    (:everything (lambda (%node%) (declare (ignore %node%)) T))
    (:attribute
     (let ((attrib (second tree)))
       (ecase (length tree)
         (2 (make-attrib-matcher attrib :exists nil))
         (3 (destructuring-bind (match-type match-to) (third tree)
              (make-attrib-matcher attrib match-type match-to))))))
    (:immediate-child
     (make-immediate-child-matcher
      (make-style-rule-matcher (second tree))
      (make-style-rule-matcher (third tree))))
    (:child
     (make-child-matcher
      (make-style-rule-matcher (second tree))
      (make-style-rule-matcher (third tree))))
    (:immediatly-preceded-by
     (make-immediatly-preceded-by-matcher
      (make-style-rule-matcher (third tree))
      (make-style-rule-matcher (second tree))))
    (:preceded-by
     (make-preceded-by-matcher
      (make-style-rule-matcher (third tree))
      (make-style-rule-matcher (second tree))))
    (:pseudo
     (destructuring-bind (pseudo name &optional subselector) tree
       (declare (ignore pseudo))
       (make-pseudo-matcher
        (fdefinition (intern (string-upcase name) :pseudo))
        (when subselector
          (make-style-rule-matcher subselector)))))
    (:nth-pseudo
     (destructuring-bind (pseudo name mul add) tree
       (declare (ignore pseudo ))
       (make-nth-pseudo-matcher
        (fdefinition (intern (string-upcase name) :pseudo))
        mul add)))))

(defun make-or-matcher (forms)
  (let ((matchers (mapcar (lambda (f) (make-style-rule-matcher f)) forms)))
    (lambda (%node%)
      "or-matcher"
      (some (lambda (matcher)
              (funcall matcher %node%))
            matchers))))

(defun make-and-matcher (forms)
  (let ((matchers (mapcar (lambda (f) (make-style-rule-matcher f)) forms)))
    (lambda (%node%)
      "and-matcher"
      (every (lambda (matcher)
               (funcall matcher %node%))
             matchers))))

(defun make-class-matcher (class)
  (lambda (%node%)
    "class-matcher"
    (and (typep %node% 'scenic:widget)
         (find class (scenic:widget-class %node%) :test 'equalp))))

(defun make-hash-matcher (id)
  (lambda (%node%)
    "hash-matcher"
    (and (typep %node% 'scenic:widget)
         (or (equalp (scenic:name %node%) id)
             (equalp (scenic:auto-name %node%) id)))))

(defun make-elem-matcher (tag)
  (lambda (%node%)
    "element-matcher"
    (let ((tag-symbol (cond
                        ((stringp tag) (intern (string-upcase tag)))
                        ((symbolp tag) tag)
                        (t (error "Invalid style element: ~A" tag)))))
    (typep %node% tag-symbol))))

(defun make-attrib-matcher (attrib match-type match-to)
  "attrib-matcher"
  (lambda (%node%)
    (case match-type
      (:equals (string-equal (get-attribute %node% attrib) match-to))
      (:includes (attrib-includes? %node% attrib match-to))
      (:dashmatch (member match-to
                          (cl-ppcre:split "-" (get-attribute %node% attrib))
                          :test #'string-equal))
      (:begins-with (alexandria:starts-with-subseq
                     match-to
                     (get-attribute %node% attrib)
                     :test #'char-equal))
      (:ends-with (alexandria:ends-with-subseq
                   match-to
                   (get-attribute %node% attrib)
                   :test #'char-equal))
      (:substring (search match-to (get-attribute %node% attrib)
                          :test #'string-equal ))
      (:exists (get-attribute %node% attrib)))))

(defun make-immediate-child-matcher (parent-matcher child-matcher)
  (lambda (%node%)
    (and (funcall child-matcher %node%)
         (scenic:parent %node%)
         (funcall parent-matcher (scenic:parent %node%)))))

(defun make-child-matcher (parent-matcher child-matcher)
  (lambda (%node%)
    (and (funcall child-matcher %node%)
         (let ((parents (scenic::get-widget-chain (list %node%))))
           (some parent-matcher parents)))))

(defun make-immediatly-preceded-by-matcher (this-matcher sibling-matcher  )
  (lambda (%node%)
    (and (funcall this-matcher %node%)
         (previous-sibling %node%)
         (funcall sibling-matcher (previous-sibling %node%)))))

(defun make-preceded-by-matcher (this-matcher sibling-matcher  )
  (lambda (%node%)
    (and (funcall this-matcher %node%)
         (iter:iter (for n initially (previous-sibling %node%)
                         then (previous-sibling n))
                    (while n)
                    (thereis (funcall sibling-matcher n))))))

(defun make-pseudo-matcher (pseudo submatcher)
  (lambda (%node%) (funcall pseudo %node% submatcher)))

(defun make-nth-pseudo-matcher (pseudo mul add)
  (lambda (%node%) (funcall pseudo %node% mul add)))

(defun reduce-tree (function tree &key initial-value)
  (if (listp tree)
      (funcall function
               (first tree)
               (mapcar (lambda (child)
                         (reduce-tree function child :initial-value initial-value))
                       (rest tree)))
      initial-value))

(defun style-rule-specificity (rule)
  (destructuring-bind (a b c d)
      (style-rule-selector-specificity (rule-selector rule))
    (+ (* a 1000)
       (* b 100)
       (* c 10)
       d)))

(defun style-rule-selector-specificity (selector)
  (let ((selector (parse-style-rule-selector selector)))
    (flet ((sum-elems (elems)
             (reduce-tree (lambda (x xs)
                            (let ((res (apply #'+ xs)))
                              (if (member x elems)
                                  (1+ res)
                                  res)))
                          selector
                          :initial-value 0)))

      (let ((a 0 ) ;; Inline styles
            (b (sum-elems '(:hash)))
            (c (sum-elems '(:class :attribute :pseudo)))
            (d (sum-elems '(:element :pseudo))))
        (list a b c d)))))

#+test(flet ((assert-specificity (selector specificity)
               (assert (equal (style-rule-selector-specificity selector)
                              specificity))))
        (assert-specificity "div" '(0 0 0 1))
        (assert-specificity "#nav .selected > a:hover" '(0 1 2 2))
        (assert-specificity "button.primary" '(0 0 1 1))
        (assert-specificity "#my-button > div.my-class" '(0 1 1 1)))

(define-condition stylesheet-error (simple-error)
  ())

(defun stylesheet-error (message &rest args)
  (error 'stylesheet-error
         :format-control message
         :format-arguments args))

(defgeneric apply-style-property (prop value object)
  (:method (prop value object)
    (stylesheet-error "Could not apply property ~A with value ~A to ~A")))

(defmethod apply-style-property ((prop function) value object)
  (funcall prop object value))

(defmethod apply-style-property ((prop symbol) value object)
  (if (not (or (access:has-slot? object prop)
               (access:has-writer? object prop)))
      (stylesheet-error "Object ~A does not have ~A property"
                        object prop)
      (setf (access:access object prop) value)))

(defmethod apply-style-property ((prop (eql :background-color)) value object)
  (setf (scenic::background-color object)
        (scenic::color value)))

(defmethod apply-style-property ((prop (eql :background-color)) value (background scenic::background))
  (setf (scenic::fill-color background) (scenic::color value)))

(defun apply-style-rule (rule object)
  (loop for (prop . value) in (alexandria:plist-alist (rule-properties rule))
     do (apply-style-property prop value object)))

(defgeneric make-style-rule-selector (selector)
  (:method ((selector function))
    selector)
  (:method ((selector string))
    (make-style-rule-matcher (parse-style-rule-selector selector)))
  (:method ((selector list))
    (make-style-rule-matcher selector)))

(defgeneric find-widget (selector widget))
(defgeneric find-widgets (selector widget))

(defmethod find-widget ((selector function) widget)
  (when (funcall selector widget)
    widget))

(defmethod find-widget ((selector string) widget)
  (find-widget (parse-style-rule-selector selector) widget))

(defmethod find-widget ((selector list) widget)
  (find-widget (make-style-rule-matcher selector) widget))

(defun find-widget-by-name (name widget)
  (find-widget (list :hash name) widget))

(defmethod find-widget ((selector function) (widget scenic:container))
  (if (funcall selector widget)
      widget
      (loop
         for child in (scenic::children widget)
         for found-widget = (find-widget selector child)
         when found-widget
         return found-widget)))

(defmethod find-widget ((selector function) (widget scenic::container1))
  (if (funcall selector widget)
      widget
      (find-widget selector (scenic:child widget))))

(defmethod find-widgets ((selector function) widget)
  (when (funcall selector widget)
    (list widget)))

(defmethod find-widgets ((selector string) widget)
  (find-widgets (parse-style-rule-selector selector) widget))

(defmethod find-widgets ((selector list) widget)
  (find-widgets (make-style-rule-matcher selector) widget))

(defmethod find-widgets ((selector function) (widget scenic:container))
  (let ((widgets
         (loop
            for child in (scenic:children widget)
            appending (find-widgets selector child))))
    (if (funcall selector widget)
        (cons widget widgets)
        widgets)))

(defmethod find-widgets ((selector function) (widget scenic::container1))
  (let ((widgets (find-widgets selector (scenic:child widget))))
    (if (funcall selector widget)
        (cons widget widgets)
        widgets)))

(defgeneric map-widgets (function widget))

(defmethod map-widgets (function widget)
  (funcall function widget))

(defmethod map-widgets (function (widget scenic:container))
  (funcall function widget)
  (map-widgets function (scenic:children widget)))

(defmethod map-widgets (function (widget scenic::container1))
  (funcall function widget)
  (map-widgets function (scenic:child widget)))

(defun call-with-stylesheet-error-handler (function)
  (if (not *ignore-stylesheet-errors*)
      (funcall function)
      (handler-case
          (funcall function)
        (stylesheet-error (e)
          (apply #'warn
                 (format nil "STYLESHEET ERROR: ~A" (simple-condition-format-control e))
                 (simple-condition-format-arguments e)))
        (error (e)
          (warn (format nil "STYLESHEET INTERNAL ERROR: ~e" e))))))

(defmacro with-stylesheet-error-handler (&body body)
  `(call-with-stylesheet-error-handler (lambda () ,@body)))

(defun apply-stylesheet (stylesheet root-widget)
  "Apply stylesheet rules from ROOT widget"
  (dolist (rule (stylesheet-sorted-rules stylesheet))
    (with-stylesheet-error-handler
      (let ((widgets (let ((*package* (stylesheet-package stylesheet)))
                       (find-widgets (rule-selector rule) root-widget))))
        (mapcan (lambda (widget)
                  (apply-style-rule rule widget))
                widgets)))))

(defun matching-style-rules (widget stylesheet)
  "Returns applyable style rules to widget from stylesheet"
  (loop :for style-rule :in (stylesheet-sorted-rules stylesheet)
     :for selector := (make-style-rule-selector (rule-selector style-rule))
     :when (let ((*package* (stylesheet-package stylesheet)))
             (funcall selector widget))
     :collect style-rule))

(defun apply-matching-style-rules (widget stylesheet)
  "Applies matching style rules to widget"
  (dolist (rule (matching-style-rules widget stylesheet))
    (with-stylesheet-error-handler
      (apply-style-rule rule widget))))

;; -- Style specs

(defun linear-gradient (direction &rest color-stops)
  (scenic.draw::make-linear-gradient
   :direction direction
   :color-stops color-stops))

(defstruct border
  width
  style
  color)

(defun border (width style color)
  (make-border :width width :style style :color color))

(defstruct font
  family
  slant
  size
  color
  weight)

(defun font (family slant size color &optional (weight :normal))
  (make-font :family family :slant slant
             :size size :color color
             :weight weight))

(defstruct padding
  top right bottom left)

(defun expand-padding-spec (padding)
  (if (numberp padding)
      (make-padding :top padding :right padding
                    :bottom padding :left padding)
      (ecase (length padding)
        (1 (make-padding :top (first padding) :right (first padding)
                         :bottom (first padding) :left (first padding)))
        (2 (make-padding :top (first padding) :right (second padding)
                         :bottom (first padding) :left (second padding)))
        (4 (make-padding :top (first padding) :right (second padding)
                         :bottom (third padding) :left (nth 3 padding))))))

(defun merge-padding-spec (padding top right bottom left)
  (make-padding :top (or top (padding-top padding))
                :right (or right (padding-right padding))
                :bottom (or bottom (padding-bottom padding))
                :left (or left (padding-left padding))))

(defun padding (&rest args)
  (expand-padding-spec args))

(defun color (source)
  (scenic.draw:parse-color-or-pattern source))

(defun expand-style-spec (spec)
  (if (atom spec)
      (list spec spec spec spec)
      (ecase (length spec)
        (1 (list (first spec) (first spec) (first spec) (first spec)))
        (2 (list (first spec) (second spec) (first spec) (second spec)))
        (4 spec))))

(defun merge-style-spec (spec first second third fourth)
  (let ((expanded-spec (expand-style-spec spec)))
    (list (or first (first expanded-spec))
          (or second (second expanded-spec))
          (or third (third expanded-spec))
          (or fourth (nth 3 expanded-spec)))))

(merge-style-spec (border 1 :solid cl-colors:+black+) nil nil nil
                  (border 3 :solid cl-colors:+green+) )

(defgeneric get-style-property-value (stylesheet prop-name widget &key status default error-p)
  )

(defmethod get-style-property-value ((stylesheet stylesheet) prop-name widget &key status default error-p)
  (check-type prop-name keyword)
  (flet ((resolve-prop-val (prop-val)
           (case prop-val
             (:inherit (get-style-property-value
                        stylesheet prop-name
                        (parent widget)
                        :status status :default default
                        :error-p error-p))
             (:default (error "TODO: not implemented"))
             (:initial (error "TODO: not implemented"))
             (t (return-from get-style-property-value prop-val)))))
    (let ((rules (matching-style-rules widget stylesheet)))
      (let ((sorted-rules
             (sort (copy-list (stylesheet-rules stylesheet))
                   #'>
                   :key 'style-rule-specificity)))
        (dolist (rule sorted-rules)
          (alexandria:when-let (prop-val (getf (rule-properties rule) prop-name))
            (resolve-prop-val prop-val)))
        ;; The property was not found if we got here
        (or default
            (and error-p
                 (error "Property not found: ~A in: ~A for: ~A"
                        prop-name stylesheet widget)))))))

(defparameter *themes* (make-hash-table))

(defclass theme-sheet (sheet)
  ((properties :initarg :properties
               :accessor theme-properties
               :documentation "The theme properties.")))

(defmethod sheet-parent ((sheet theme-sheet))
  (when (sheet-parent sheet)
    (find-theme (sheet-parent sheet))))

(defmacro deftheme (name super &body properties)
  `(setf (gethash ',name *themes*)
         (make-instance 'theme-sheet
                        :name ',name
                        :parent ',(first super)
                        :properties
                        (list ,@(loop for class-props in properties
                                   collect `(list ',(first class-props)
                                                  ,@(loop for prop in (rest class-props)
                                                       collect `(list ,(alexandria:make-keyword (first prop)) ,@(rest prop)))))))))

(defun find-theme (name &key (error-p t))
  (or (gethash name *themes*)
      (and error-p (error "Theme not found: ~A" name))))

(defmethod get-style-property-value ((theme theme-sheet) prop-name widget &key status default error-p)
    (check-type prop-name keyword)
  (flet ((resolve-prop-val (prop-val)
           (case prop-val
             (:inherit (if (symbolp widget)
                           (error "Cannot inherit from symbol: ~A" widget)
                           (get-style-property-value
                        theme prop-name
                        (parent widget)
                        :status status :default default
                        :error-p error-p)))
             (:default (error "TODO: not implemented"))
             (:initial (error "TODO: not implemented"))
             (t (return-from get-style-property-value prop-val)))))
    (let* ((class-props (cdr (find (if (symbolp widget)
                                       widget
                                       (class-name (class-of widget)))
                              (theme-properties theme) :key 'car)))
           (prop (find prop-name class-props :key 'car)))
      (if prop
          (destructuring-bind (p-name p-val &rest statuses) prop
            (when status
              (alexandria:when-let (p-status-val (getf statuses status))
                (return-from get-style-property-value
                  (resolve-prop-val p-status-val))))
            (resolve-prop-val p-val))
          (or default (and error-p (error "Property not found: ~A for: ~A in: ~A"
                                          prop-name widget theme)))))))

(defclass property-sheet (sheet)
  ((properties-table
    :accessor properties-table
    :initform (make-hash-table :test 'equalp)
    :documentation "A hash-table for holding property values.
Keys have the form (WIDGET-CLASS PROP-NAME STATUS) and values are the property value"))
  (:documentation "A flat sheet with resolved properties"))

(defgeneric normalize-sheet (sheet)
  (:documentation "Normalizes the sheet to a flat property sheet"))

(defun merge-property-sheets (property-sheets)
  "Merges normalized property sheets"
  
  )

(defmethod get-style-property-value ((sheet property-sheet) prop-name widget &key status default error-p)
    (check-type prop-name keyword)
  (flet ((resolve-prop-val (prop-val)
           (case prop-val
             (:inherit (get-style-property-value
                        sheet prop-name
                        (parent widget)
                        :status status :default default
                        :error-p error-p))
             (:default (error "TODO: not implemented"))
             (:initial (error "TODO: not implemented"))
             (t (return-from get-style-property-value prop-val)))))
    (when status
      (alexandria:when-let (p-status-val (gethash (list (class-of widget)
                                                        prop-name
                                                        status)
                                                  (properties-table sheet)))
        (return-from get-style-property-value
          (resolve-prop-val p-status-val))))
    (let ((prop-val (gethash (list (class-of widget)
                                   prop-name
                                   nil)
                             (properties-table sheet))))
      (or (and prop-val (resolve-prop-val prop-val))
          default
          (and error-p (error "Property not found: ~A for: ~A in: ~A"
                              prop-name widget sheet))))))

(defmethod normalize-sheet ((sheet theme-sheet))
  )

(defmethod normalize-sheet ((sheet stylesheet))
  (error "Don't know how to normalize a stylesheet"))

(defun inline-stylesheet (properties)
  (let ((sheet
         (make-instance 'theme-sheet
                        :name 'inline-styles
                        :properties properties)))
    (normalize-sheet sheet)))

(defmacro with-widget-styles (widget &body body)
  `(flet ((style-val (prop &key status default error-p)
            (get-widget-style-property-value
             ,widget prop
             :status status :default default
             :error-p error-p)))
     ,@body))
