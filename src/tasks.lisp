(in-package :scenic)

;;-- * Threads
;;-- Threads to execute code asynchrounsly in the background

(defvar *ui-task-queue-lock*)
(setf *ui-task-queue-lock* (bt:make-recursive-lock))

(defvar *ui-task-list*)
(setf *ui-task-list* nil)

(defvar *ui-task-counters-per-thread*)
(setf *ui-task-counters-per-thread* nil)

(defvar *ui-thread*)

(defvar *thread-counter*)
(setf *thread-counter* 0)

(defvar *scenic-threads*)
(setf *scenic-threads* (make-hash-table))

(defun check-ui-thread ()
  #+nil(unless (on-ui-thread)
         (error "Not on UI thread"))
  )

(defun on-ui-thread ()
  (eq (bt:current-thread) *ui-thread*))

(defun cleanup-dead-threads ()
  (maphash (lambda (key value)
             (declare (ignore value))
             (unless (bt:thread-alive-p key)
               (remhash key *scenic-threads*)))
           *scenic-threads*))

(defun allocate-thread (code &key name fast)
  (check-ui-thread)
  (bt:with-recursive-lock-held (*ui-task-queue-lock*)
    (unless fast
      (cleanup-dead-threads))
    (let ((new-thread (bt:make-thread code :name name)))
      (setf (gethash new-thread *scenic-threads*) (incf *thread-counter*))
      new-thread)))

(defmacro with-thread ((&key name fast) &body body)
  `(allocate-thread (lambda () ,@body) :name ,name :fast ,fast))

(defun reset-threads ()
  (setf *scenic-threads* (make-hash-table))
  (setf (gethash (bt:current-thread) *scenic-threads*)
        0))

;;-- * Tasks

;;-- ** Overview
;;-- Tasks are facilities to execute code

(defclass task ()
  ((number :accessor task-number :initarg :number :initform nil)
   (name :accessor task-name :initarg :name :initform nil)
   (code :accessor task-code :initarg :code :initform nil)
   (time :accessor task-time :initarg :time :initform nil)
   (result :accessor task-result :initform nil)))

(defgeneric execute (task))

;;-- ** UI tasks
;;-- UI Tasks are executed inside the UI loop. That means they must take short time to run.
;;-- UI tasks are created via the as-ui-task macro. They can be triggered either from a background thread, or from the main UI loop directly.
;;-- To trigger a task from a background thread, the SCENIC:WITH-THREAD macro should be used.

(defclass ui-task (task)
  ()
  (:documentation "UI tasks are tasks that execute in the main UI loop. They are taken by Scenic events handling subsystem and executed in the UI loop."))

(gen-print-object ui-task (number name code time))

(defmethod execute ((task task))
  (funcall (task-code task) task))

(defun pick-ui-task (task-number timeout-ms)
  (format t "Picking task: ~A~%" task-number)
  (labels ((get-task (tasks)
             (values (find-if (lambda (task) (string= (task-number task) task-number))
                              tasks)
                     (remove-if (lambda (task) (string= (task-number task) task-number)) tasks))))
    (let ((start-time (get-internal-real-time))
          (timeout-itu (* (/ timeout-ms 1000) internal-time-units-per-second))
          task)
      (loop
         (bt:with-recursive-lock-held (*ui-task-queue-lock*)
           (setf (values task *ui-task-list*) (get-task *ui-task-list*)))
         (cond (task (execute task)
                     (return))
               (t (sleep 0.05)
                  (when (> (get-internal-real-time)
                           (+ start-time timeout-itu))
                    (error "Timed out waiting for task to be put in queue."))))))))

(defun add-ui-task (task-code &optional task-name (after-ms 0))
  (labels ((add-sorted (task task-list)
             (if (null task-list)
                 (list task)
                 (if (< (task-time task) (task-time (car task-list)))
                     (cons task task-list)
                     (cons (car task-list) (add-sorted task (cdr task-list)))))))
    (bt:with-recursive-lock-held (*ui-task-queue-lock*)
      (let ((thread-id (gethash (bt:current-thread) *scenic-threads*)))
        (unless thread-id
          (error
           "Cannot add a task from a thread that wasn't created using scenic:allocate-thread: ~A" (bt:current-thread)))
        (when (null (gethash (bt:current-thread) *ui-task-counters-per-thread*))
          (setf (gethash (bt:current-thread) *ui-task-counters-per-thread*) 0))
        (let ((task (make-instance 'ui-task
                                   :number (format nil "~a-~a"
                                                   thread-id
                                                   (incf (gethash (bt:current-thread)
                                                                  *ui-task-counters-per-thread*)))
                                   :name task-name
                                   :code task-code
                                   :time (+ (get-internal-real-time)
                                            (* (/ after-ms 1000)
                                               internal-time-units-per-second)))))
          (setf *ui-task-list* (add-sorted task *ui-task-list*)))))))

(defmacro as-ui-task ((&key delay-ms task-name) &body body)
  `(add-ui-task (lambda (_task)
                  ,@body)
                ,task-name ,(or delay-ms 0)))

(defun get-ui-tasks-to-execute ()
  (labels ((split-time (tasks time &optional acc)
             (cond ((null tasks)
                    (values tasks (reverse acc)))
                   ((< (task-time (car tasks)) time)
                    (split-time (cdr tasks) time (cons (car tasks) acc)))
                   (t (values tasks (reverse acc))))))
    (bt:with-recursive-lock-held (*ui-task-queue-lock*)
      (let ((time (get-internal-real-time))
            tasks)
        (setf (values *ui-task-list* tasks) (split-time *ui-task-list* time))
        tasks))))

(defun execute-ui-tasks (scene)
  (dolist (task (get-ui-tasks-to-execute))
    (record-events
     (list (make-instance 'ui-task-executed-event
                          :task-number (task-number task))))
    (execute task)))

(defun reset-ui-tasks ()
  (setf *ui-task-list* nil)
  (setf *ui-task-counters-per-thread* (make-hash-table))
  (setf *thread-counter* 0)
  (setf *ui-thread* (bt:current-thread)))

;;-- ** Async tasks
;;-- Async tasks execute outside the UI main loop.

(defvar *async-tasks-queue* (make-instance 'chanl:stack-channel))

(defclass async-task (task)
  ((start-callback :initarg :on-start
                   :type (or null symbol function)
                   :accessor start-callback
                   :initform nil
                   :documentation "Function to execute when the task starts running")
   (complete-callback :initarg :on-complete
                      :type (or null symbol function)
                      :accessor complete-callback
                      :initform nil
                      :documentation "Function to execute from the UI main loop when the async task completes execution successfully")
   (error-callback :initarg :on-error
                   :type (or null symbol function)
                   :accessor error-callback
                   :initform nil
                   :documentation "Function to execute from the UI main loop when there was an error executing the task")
   (progress-callback :initarg :on-progress
                      :type (or null symbol function)
                      :accessor progress-callback
                      :initform nil
                      :documentation "Function that is executed when the task notifies progress via PUBLISH-PROGRESS"))
  (:documentation "Async tasks run outside the main UI loop"))

(defun add-async-task (async-task)
  (chanl:send *async-tasks-queue* async-task :blockp nil))

(defmacro as-async-task (args &body body)
  `(add-async-task
    (make-instance 'async-task
                   :code
                   (lambda (_task)
                     (flet ((update-progress (progress)
                              (publish-progress _task progress)))
                           ,@body))
                   ,@args)))

(defmethod publish-progress ((task async-task) progress)
  (when (progress-callback task)
    (as-ui-task ()
      (funcall (progress-callback task) progress))))

(defun start-async-task (task)
  (when (start-callback task)
    (as-ui-task ()
      (funcall (start-callback task) task))))

(defun completed-async-task (task result)
  (when (complete-callback task)
    (as-ui-task ()
      (funcall (complete-callback task) result task))))

(defun error-async-task (task error)
  (when (error-callback task)
    (as-ui-task ()
      (funcall (error-callback task) task error))))

(defun process-async-task (task)
  (start-async-task task)
  (handler-case
      (let ((result (execute task)))
        (completed-async-task task result))
    (error (e)
      (warn "ASYNC TASK ERROR: ~A" e)
      (error-async-task task e))))

(defun async-task-processor ()
  (loop
     do (let ((task (chanl:recv *async-tasks-queue*)))
          (process-async-task task))))

(defun start-async-task-processor ()
  (allocate-thread #'async-task-processor :name "Scenic Async Task Processor"))

(defparameter +async-task-processors-count+ 4)

(defun start-async-task-processors ()
  (dotimes (i +async-task-processors-count+)
    (start-async-task-processor)))
