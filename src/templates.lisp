(defpackage :scenic-templates
  (:nicknames :scenic.wt)
  (:use :cl))

(in-package :scenic.wt)

;;-- * Widget templates
;;--
;;-- Widget Templates are a functional description of the user interface.
;;-- Instead of working with widgets directly, the developer works with an immutable representation of the state of the view (widgets layout) based on some state.
;;-- This is very similar to how React and Flutter and other Web GUI frameworks work.

(defparameter *widget-templates-log* nil)

(defclass widget-template ()
  ((key :initarg :key :accessor wt-key :initform nil)
   (events :initarg :events :accessor wt-events :initform nil)
   (ref :initarg :ref :accessor wt-ref :initform nil))
  (:documentation "A widget-template is a representation of a configuration of a widget."))

(defmethod print-object ((wt widget-template) stream)
  (print-unreadable-object (wt stream :type t)
    (format stream "#~A" (wt-key wt))))

(defmacro slet (bindings &body body)
  "Stateful let"
  `(let ,(loop for binding in bindings
            collect (list (first binding)
                          (let ((svar (gensym)))
                            `(if (boundp ',svar)
                                 (symbol-value ',svar)
                                 (setf (symbol-value ',svar)
                                       ,(second binding))))))
     ,@body))

#+nil(defun set-prop (wt prop w)
       (funcall (or (wt-prop-writer prop)
                    (fdefinition `(setf ,(wt-prop-accessor prop))))
                (slot-value wt (wt-prop-name prop))
                w))

(defgeneric set-prop (wt w prop val))
(defgeneric get-prop (wt w prop))

(defmethod set-prop ((wt widget-template) w prop val)
  (funcall
   (or (wt-prop-writer prop)
       (fdefinition `(setf ,(wt-prop-accessor prop))))
   val
   w))

(defmethod get-prop ((wt widget-template) w prop)
  (funcall (or (wt-prop-reader prop)
               (fdefinition (wt-prop-accessor prop)))
           w))

(defmethod update-prop ((wt widget-template) w prop new-val old-val)
  (let ((prop-test (or (wt-prop-test prop)
                       #'equalp)))
    (when (not (funcall prop-test new-val old-val))
      (set-prop wt w prop new-val))))

(defmethod update-props ((wt widget-template) w new-tpl old-tpl)
  (loop for prop in (wt-props new-tpl)
     do (update-prop wt w prop
                     (get-prop wt new-tpl prop)
                     (get-prop wt old-tpl prop))))

(defstruct wt-prop
  name
  initarg
  accessor
  reader
  writer
  test)

(defstruct wt-event
  name
  handler)

(defclass wt-container (widget-template)
  ((children :initarg :children :accessor wt-children)))

(defclass wt-container1 (widget-template)
  ((child :initarg :child :accessor wt-child)))

(defmethod wt-children ((wt wt-container1))
  (list (wt-child wt)))

(defclass wt-child (widget-template)
  ())

(defgeneric wt-props (wt)
  (:method ((wt widget-template))
    nil))

(defgeneric wt-create-widget (wt)
  (:documentation "WT-CREATE-WIDGET is in charge of creating the widget tree from the template representation"))

(defmethod wt-create-widget ((wt widget-template))
  (let ((w
         (apply #'make-instance (wt-widget-class wt)
                (alexandria:flatten
                 (mapcar (lambda (prop)
                           (when (slot-boundp wt (wt-prop-name prop))
                             (list (wt-prop-initarg prop)
                                   (slot-value wt (wt-prop-name prop)))))
                         (wt-props wt))))))
    (when (wt-ref wt)
      (funcall (wt-ref wt) w))
    (loop for ev in (wt-events wt)
       do (scenic:add-event-handler w (car ev) :bubble (cdr ev)))
    w))

(defmethod wt-create-widget ((wt wt-container))
  (let ((w (call-next-method)))
    (setf (slot-value w 'scenic:children)
          (mapcar (lambda (wt-child)
                    (let ((child (wt-create-widget wt-child)))
                      (setf (scenic:parent child) w)
                      child))
                  (wt-children wt)))
    w))

(defmethod wt-create-widget ((wt wt-container1))
  (let ((w (call-next-method)))
    (let ((child (wt-create-widget (wt-child wt))))
      (setf (slot-value w 'scenic:child) child)
      (setf (scenic:parent child) w)
      w)))


(defun wt-changed-p (wt1 wt2)
  (not
   (and (eql (wt-widget-class wt1) (wt-widget-class wt2))
        (eql (wt-key wt1) (wt-key wt2)))))

(defun extract-events (args)
  (let (rest-args events)
    (loop
       for key in args by #'cddr
       for val in (rest args) by #'cddr
       do (if (and (>= (length (string key)) 3)
                   (equalp (subseq (string key) 0 3) "ON-"))
              (push (cons (alexandria:make-keyword (subseq (string key) 3))
                          val)
                    events)
              (progn
                (push key rest-args)
                (push val rest-args))))
    (values events (reverse rest-args))))

(defclass widget-template-container (widget-template)
  ((widget :initarg :widget
           :accessor widget))
  (:documentation "A template wrapping a widget"))

(defmethod wt-widget-class ((template widget-template-container))
  (class-name (class-of (widget template))))

(defmethod wt-create-widget ((template widget-template-container))
  (widget template))

(defmethod wt-child ((template widget-template-container))
  nil)


(defclass template-widget (scenic::container1)
  ((w-template :initarg :w-template
               :initform nil
               :accessor w-template
               :documentation "The current widget template")
   (dirty :initform t :accessor dirty-p))
  (:metaclass scenic:widget-class))

(defun set-dirty (template-widget &optional (value t))
  (setf (dirty-p template-widget) value))

(defmethod scenic::build ((widget template-widget))
  (format *widget-templates-log* "Building ~A~%" widget)
  (let ((wt (render widget)))
    (setf (scenic::child widget) (wt-create-widget wt)))
  t)

(defmethod scenic::prepare ((widget template-widget) scene)
  ;;(format *widget-templates-log* "Preparing ~A~%" widget)
  (when (dirty-p widget)
    (let ((new-template (render widget)))
      (diff/update widget
                   widget
                   new-template
                   (w-template widget)
                   0 0
                   (alexandria:copy-sequence 'list (scenic:children widget)))
      (setf (w-template widget) new-template)))
  t)

(defmethod render ((widget template-widget))
  (error "Implement render method for ~A" widget))

(defmethod render :after ((widget template-widget))
  (setf (dirty-p widget) nil))

;; Delegate template api methods from template-widget to its template
(defun ensure-w-template (template-widget)
  (when (null (w-template template-widget))
    (setf (w-template template-widget) (render template-widget)))
  (w-template template-widget))
  
(defmethod wt-create-widget ((widget template-widget))
  widget)

(defmethod wt-key ((widget template-widget))
  (wt-key (ensure-w-template widget)))

(defmethod wt-widget-class ((widget template-widget))
  (wt-widget-class (ensure-w-template widget)))

(defmethod wt-props ((widget template-widget))
  (wt-props (ensure-w-template widget)))

(defmethod wt-children ((widget template-widget))
  (wt-children (ensure-w-template widget)))

(defmethod wt-child ((widget template-widget))
  (wt-child (ensure-w-template widget)))

(defgeneric diff/update (tw parent old-tpl new-tpl index old-index original-children))

;; Generic templates?

;; template := (class-name (&rest props) &rest children)
;; prop := (accessor value &optional initarg) . default-initarg: (make-keyword accessor) | event
;; event := (on-<event-name> handler)

(defclass generic-widget-template (widget-template)
  ((widget-class :initarg :widget-class
                 :accessor wt-widget-class
                 :initform (error "Provide the widget class"))
   (props :initarg :props
          :accessor wt-props
          :initform nil)
   (child :initarg :child
          :accessor wt-child
          :initform nil)
   (children :initarg :children
             :accessor wt-children
             :initform nil)))

(defmethod initialize-instance :after ((template generic-widget-template) &rest initargs)
  (check-template-children-duplicates template))

(defmethod print-object ((object generic-widget-template) stream)
  (print-unreadable-object (object stream :type nil :identity nil)
    (format stream "GWT ~A~@[ #~A~]"
            (wt-widget-class object)
            (wt-key object))))

(defun make-generic-widget-template (widget-class props child-or-children)
  (multiple-value-bind (events props)
      (extract-events props)
    (make-instance 'generic-widget-template
                   :key (getf props :key)
                   :ref (or (getf props :ref)
                            (getf props :initialize))
                   :widget-class widget-class
                   :props (alexandria:remove-from-plist props :key :ref :initialize)
                   :events events
                   :children (and (listp child-or-children)
                                  child-or-children)
                   :child (and (not (listp child-or-children))
                               child-or-children))))

(defun list-duplicates (list &key (key 'identity) (test 'eql))
  (let ((occurrences (make-hash-table :test test)))
    (dolist (x list)
      (let ((k (funcall key x)))
        (when k
          (if (gethash k occurrences)
              (incf (gethash k occurrences))
              (setf (gethash k occurrences) 1)))))
    (loop
       for k being the hash-keys in occurrences
       using (hash-value v)
       when (> v 1)
       collect k)))

(defun check-template-children-duplicates (template)
  (let ((duplicates (list-duplicates (wt-children template)
                                     :test 'eql
                                     :key 'wt-key)))
    (when duplicates
      (error "Duplicate keys in template ~A: ~A" template duplicates))))
    

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun tree-to-widget-templates (list)
    (when (not (null list))
      (cond
        ((equalp (symbol-name (first list)) (symbol-name 'esc))
         (first (rest list)))
        ((equalp (symbol-name (first list)) (symbol-name 'w))
         (destructuring-bind (_ key widget) list
             `(make-instance 'widget-template-container
                             :key ,key
                             :widget ,widget)))
        ((equalp (symbol-name (first list)) (symbol-name 'rnd))
         (let ((renderer (gensym)))
           `(slet ((,renderer ,(second list)))
              (render ,renderer))))
        (t
         (destructuring-bind (widget-class props &optional child-or-children) list
           `(make-generic-widget-template
             ',widget-class
             (list ,@props)
             ,(if (listp (first child-or-children))
                  `(remove nil (list ,@(mapcar 'tree-to-widget-templates child-or-children)))
                  (tree-to-widget-templates child-or-children))))))))

  (defmacro with-widget-template (options &body body)
    `(macrolet ((wt (body)
                  (tree-to-widget-templates body)))
       (wt ,@body))))

(scenic:defwidget generic-template-widget (template-widget)
  ())

(defmethod scenic::prepare ((widget generic-template-widget) scene)
  ;;(format *widget-templates-log* "Preparing ~A~%" widget)
  (when (dirty-p widget)
    (let ((new-template (render widget)))
      (diff/update widget
                   widget
                   (w-template widget)
                   new-template
                   0 0
                   (alexandria:copy-sequence 'list (scenic:children widget)))

      (setf (w-template widget) new-template)))
  t)

;; TODO: optimize diff/update algorithm.
;; Look at Mithril algorithm for example: https://github.com/MithrilJS/mithril.js/blob/next/render/vnode.js

(defmethod diff/update ((template-widget generic-template-widget)
                        parent old-tpl new-tpl index old-index original-children)
  "Diff/Update algorithm. Similar to wt-diff, but updates the widget tree W directly instead of producing patches"
  (macrolet ((set-nth-child (children n child)
               `(progn
                  (assert (<= ,n (length ,children)))
                  (if (> (length ,children) ,n)
                      (setf (nth ,n ,children) ,child)
                      (setf ,children (append ,children (list ,child)))))))
    (cond
      ((not old-tpl)
       ;; new-tpl is not in old tree. add it.
       (let ((new-child (wt-create-widget new-tpl)))
         (if (typep parent 'scenic::container1)
             (setf (scenic::child parent) new-child)
             (scenic::add-child parent new-child))
         (format *widget-templates-log* "TEMPLATE: ~a ADD CHILD: ~a~%"
                 parent new-child)
         ))
      #+nil((not new-tpl)
       ;; there's no new tpl. remove from tree.
       (if (typep parent 'scenic::container1)
           (progn
             (setf (scenic::child parent) nil)
             (format *widget-templates-log* "TEMPLATE: ~A REMOVE CHILD~%" parent))
           (progn
             (scenic::remove-child parent index)
             (format *widget-templates-log* "TEMPLATE: ~A REMOVE CHILD AT ~A~%" parent index))))
      ((wt-changed-p new-tpl old-tpl)
       ;; if there are both new-tpl and old-tpl, but they are different, replace.
       (format *widget-templates-log* "TEMPLATE: REPLACE CHILD: ~A ~A~%"
               new-tpl old-tpl)
       (cond
         ((typep parent 'scenic::container1)
          (setf (scenic:child parent) (wt-create-widget new-tpl)))
         (t
          (scenic::replace-child parent index (wt-create-widget new-tpl)))))
      ;; there are two compatible tpls which have not "changed". still we have to look at their properties and children.
      (t

       (when (not (= index old-index))
         ;; old and new tpls are compatible, but at different indexes. move the child.
         (let ((moving-child (nth old-index original-children)))
           (set-nth-child (scenic:children parent) index
                          moving-child)
           (scenic::mark-for-layout parent)
           (format *widget-templates-log* "TEMPLATE: MOVING ~A FROM ~A to ~A~%"
                   moving-child old-index index)))

       (let* ((target (cond
                        ((typep parent 'scenic::container1)
                         (scenic:child parent))
                        (t
                         (nth index (scenic:children parent)))))
              (target-original-children (alexandria:copy-sequence 'list (scenic:children target))))

         ;; update props
         (update-props (w-template template-widget) target new-tpl old-tpl)

         (when (typep target 'scenic::container)
           ;; update children
           (loop for i from 0 to (1- (length (wt-children new-tpl)))
              do
                (let* ((new-child-tpl (nth i (wt-children new-tpl)))
                       (old-child-index (or (and new-child-tpl
                                                 (wt-key new-child-tpl)
                                                 (position (wt-key new-child-tpl)
                                                           (wt-children old-tpl)
                                                           :key 'wt-key
                                                           :test #'eql))
                                            i))
                       (old-child-tpl (nth old-child-index (wt-children old-tpl))))
                  (diff/update template-widget
                               target
                               old-child-tpl
                               new-child-tpl
                               i
                               old-child-index
                               target-original-children))))

         ;; Remove extra children
         
         (when (and (typep target 'scenic::container)
                    (< (length (wt-children new-tpl))
                       (length (wt-children old-tpl))))
           (loop
              :for i :from (1- (length (wt-children old-tpl)))
              :downto (length (wt-children new-tpl))
              :do
              (format *widget-templates-log* "TEMPLATE: REMOVING ~a child AT ~a~%" target i) 
              (scenic::remove-child target i)))

         (when (wt-child new-tpl)
           (diff/update template-widget
                        target
                        (wt-child old-tpl)
                        (wt-child new-tpl)
                        0 0 original-children))

         )))))

(defmethod set-prop ((wt generic-widget-template) w prop val)
  (let ((prop-writer
         (first (scenic::computed-writers-of
                 (first (remove-if-not
                         (lambda (slot)
                           (member prop (c2mop:slot-definition-initargs slot)))
                         (c2mop:class-slots (class-of w))))))))
    (funcall
     (fdefinition prop-writer)
     val
     w)))

(defmethod get-prop ((wt generic-widget-template) w prop)
  (let* ((slot (first (remove-if-not
                       (lambda (slot)
                         (member prop (c2mop:slot-definition-initargs slot)))
                       (c2mop:class-slots (class-of w)))))
         (prop-reader (and slot
                           (first (scenic::computed-readers-of
                                   slot)))))
    (when (not slot)
      (error "Prop ~A not found in ~A" prop w))

    (if prop-reader
        (funcall (fdefinition prop-reader)
                 w)
        (slot-value w (c2mop:slot-definition-name slot)))))

(defmethod update-prop ((wt generic-widget-template) w prop new-val old-val)
  (let ((prop-test #'equalp))
    (when (not (funcall prop-test new-val old-val))
      (format *widget-templates-log* "TEMPLATE: UPDATED PROP ~A OF ~a FROM: ~A TO: ~A ~%" prop w old-val new-val)
      (set-prop wt w prop new-val))))

(defun plist-keys (plist)
  (loop for key in plist by #'cddr
     collect key))

(defmethod update-props ((wt generic-widget-template) w new-tpl old-tpl)
  (loop for prop in (plist-keys (wt-props new-tpl))
     do (update-prop wt w prop
                     (getf (wt-props new-tpl) prop)
                     (getf (wt-props old-tpl) prop))))

(defmethod wt-create-widget ((wt generic-widget-template))
  (let ((w
         (apply #'make-instance (wt-widget-class wt)
                (wt-props wt))))
    (when (wt-ref wt)
      (funcall (wt-ref wt) w))
    (loop for ev in (wt-events wt)
       do (scenic:add-event-handler w (car ev) :bubble (cdr ev)))
    (when (wt-children wt)
      (setf (scenic:children w)
            (mapcar (lambda (wt-child)
                      (let ((child (wt-create-widget wt-child)))
                        (setf (scenic:parent child) w)
                        child))
                    (wt-children wt))))
    (when (wt-child wt)
      (let ((child (wt-create-widget (wt-child wt))))
        (setf (scenic:child w) child)
        (setf (scenic:parent child) w)))
    w))

(scenic:defwidget wt-embedded-ex (generic-template-widget)
  ((selected :initform nil
             :accessor selected?)
   (hover :initform nil
          :accessor hover?)))

(defmethod (setf selected?) :after (new-value (obj wt-embedded-ex))
  (setf (dirty-p obj) t))

(defmethod (setf hover?) :after (new-value (obj wt-embedded-ex))
  (setf (dirty-p obj) t))

(defmethod render ((self wt-embedded-ex))
  (with-widget-template ()
    (scenic:pack (:orientation :vertical
                               :space-between-cells 3)
                 ((scenic:button
                   (:on-click (lambda (btn ev)
                                (setf (selected? self)
                                      (not (selected? self))))
                              :on-mouse-enter (lambda (btn ev)
                                                (setf (hover? self) t))
                              :on-mouse-leave (lambda (btn ev)
                                                (setf (hover? self) nil))
                              ;;:fill-color (if (hover? self) cl-colors:+cyan+ cl-colors:+lightgray+)
                              )
                   (scenic:label (:text (if (selected? self) "Hello" "World"))))
                  (esc (when (selected? self)
                         (wt (scenic:label (:text "Selected!!")))))
                  (esc (when (hover? self)
                         (wt (scenic:label (:text "Hovered!!")))))))))

(scenic:defwidget wt-example-3 (generic-template-widget)
  ((selected-item :initform :opt1 :accessor selected-item)
   (embedded :initform
             (make-instance 'wt-embedded-ex)
             :accessor embedded)))

(defmethod (setf selected-item) :after (new-value (obj wt-example-3))
  (setf (dirty-p obj) t))

(defmethod render ((renderer wt-example-3))
  (let ((emb1 (with-widget-template ()
                (wt-embedded-ex ()))))
    (with-widget-template ()
      (scenic:background
       (:fill-color
        (ecase (selected-item renderer)
          (:opt1 cl-colors:+white+)
          (:opt2 cl-colors:+red+)))
       (scenic:pack
        (:orientation :vertical
                      :space-between-cells (ecase (selected-item renderer)
                                             (:opt1 5)
                                             (:opt2 15)))
        ((scenic:button
          (:on-click
           (lambda (btn ev)
             (setf (selected-item renderer)
                   (case (selected-item renderer)
                     (:opt1 :opt2)
                     (:opt2 :opt1)))
             (format t "Selected: ~A~%" (selected-item renderer))))
          (scenic:label
           (:text (ecase (selected-item renderer)
                    (:opt1 "Switch to Option 2")
                    (:opt2 "Switch to Option 1")))))
         (esc (ecase (selected-item renderer)
                (:opt1 (wt (scenic:label (:text "Option 1"
                                                :font-color cl-colors:+black+
                                                :key :opt1
                                                ))))
                (:opt2 (wt (scenic:label (:text "Option 2"
                                                :font-color cl-colors:+black+
                                                :key :opt2
                                                :font-color cl-colors:+white+))))))
         (esc emb1)
         (esc (with-widget-template () (wt-embedded-ex ())))
         (w :label-1 (scenic:label "Direct embed"))
         (wt-embedded-ex ())
         (esc (embedded renderer))         
         ))))))

(defun wt-example-3 ()
  "This is an example of generic templates

(wt-example-3)"
  (let ((scene (scenic:scene
                500 500
                (scenic-helpers:background
                 cl-colors:+black+
                 (scenic-helpers:uniform-padding
                  10
                  (make-instance 'wt-example-3))))))
    (scenic:run-scene scene)))


(scenic:defwidget wt-todo-list (generic-template-widget)
  ((items :initarg :items
          :initform (list "a" "b" "c" "d")
          :accessor items)
   (todo-list :accessor todo-list)))

#+nil(defmethod render ((renderer wt-todo-list))
  (with-widget-template ()
    (scenic:background
     (:fill-color
      cl-colors:+white+)
     (scenic.nui::listview (:ref (lambda (list) (setf (todo-list renderer) list)))
       (esc (loop for item in (items renderer)
               collect
                 (wt (scenic.nui::listitem ()
                                           (scenic:padding (:padding-left 5
                                                                          :padding-right 5
                                                                          :padding-top 5
                                                                          :padding-bottom 5)
                                                           (scenic:pack (:orientation :horizontal
                                                                                      :space-between-cells 5)
                                                                        ((scenic:label (:text item))
                                                                         (scenic:button (:on-click (lambda (btn ev)
                                                                                                     (setf (items renderer)
                                                                                                           (remove item (items renderer)))
                                                                                                     ;;(scenic::rebuild (todo-list renderer))
                                                                                                     ))
                                                                                        (scenic:label (:text "-"))))))))))))))



#+nil(defmethod render ((renderer wt-todo-list))
  (with-widget-template ()
      
       (scenic:sizer
        (:min-width 200 :max-width 200
                    :min-height 20 :max-height 20
                    ;;:key :text-input
                    )
        (scenic.nui::nui-textbox
         ()))))

(defmethod render ((renderer wt-todo-list))
  (let (textbox)
    (with-widget-template ()
      (scenic:background
       (:fill-color
        cl-colors:+white+)
       (scenic:pack
        (:orientation :vertical
                      :space-between-cells 5)
        ((scenic:pack
          (:orientation :vertical
                        :space-between-cells 5)
          (esc (loop for item in (items renderer)
                  collect
                    (wt (scenic:padding
                         (:padding-left 5
                                        :padding-right 5
                                        :padding-top 5
                                        :padding-bottom 5
                                        :key item)
                         (scenic:pack
                          (:orientation :horizontal
                                        :space-between-cells 5)
                          ((scenic:label (:text item))
                           (scenic:button
                            (:on-click
                             (let ((itm item))
                               (lambda (btn ev)
                                 (setf (items renderer)
                                       (remove itm (items renderer) :test #'eql))
                                 (set-dirty renderer))))
                            (scenic:padding
                             (:padding-left 5
                                            :padding-right 5
                                            :padding-top 5
                                            :padding-bottom 5)
                             (scenic:label (:text "-")))))))))))
         (scenic:sizer
              (:min-width 200 :max-width 200
                          :min-height 20 :max-height 20
                          ;;:key :text-input
                          )
              (scenic:textbox
               (:ref (lambda (tb)
                       (setf textbox tb))
                     :cursor-position 0
                     :selection-start 0
                     :caret-color cl-colors:+black+
                     :selection-color cl-colors:+lightblue+
                     :background-color cl-colors:+white+
                     :font-face "Courier"
                     :font-size 14
                     :text "Write what to do here")))
         (scenic:button
              (:on-click
               (lambda (btn ev)
                 (setf (items renderer)
                       (append 
                        (items renderer)
                        (list (copy-seq (scenic::text textbox)))))
                 (set-dirty renderer)))
              (scenic:padding
               (:padding-left 5
                              :padding-right 5
                              :padding-top 5
                              :padding-bottom 5)
               (scenic:label (:text "+")))                    
              )))
       ))))

(defun wt-todo-list ()
  "(wt-todo-list)"
  (let ((scene (scenic:scene
                500 500
                (scenic-helpers:background
                 cl-colors:+black+
                 (scenic-helpers:uniform-padding
                  10
                  (make-instance 'wt-todo-list))))))
    (scenic:run-scene scene)))

(scenic:defwidget wt-todo-list-2 (generic-template-widget)
  ((items :initarg :items
          :initform (list "a" "b" "c" "d")
          :accessor items)
   (todo-list :accessor todo-list)))

(defmethod render ((renderer wt-todo-list-2))
  (let (textbox)
    (with-widget-template ()
      (scenic:background
       (:fill-color
        cl-colors:+white+)
       (scenic:pack
        (:orientation :vertical :space-between-cells 5)
        ((scenic:pack
          (:orientation :horizontal :space-between-cells 0)
          (esc 
           (loop
              for item in (items renderer)
              for i from 0
              collect
                (wt (scenic:padding
                     (:padding-left 5
                                    :padding-right 5
                                    :padding-top 5
                                    :padding-bottom 5
                                    :key item
                                    )
                     (scenic:pack
                      (:orientation :horizontal
                                    :space-between-cells 5)
                      ((scenic:label (:text item))
                       (scenic:button
                        (:on-click
                         (let ((itm item))
                           (lambda (btn ev)
                             (setf (items renderer)
                                   (remove itm (items renderer) :test #'eql))
                             (set-dirty renderer)
                             (format *widget-templates-log* "Removed item: ~A~%" itm)
                             (format *widget-templates-log* "Items: ~A~%" (items renderer))
                             )))
                        (scenic:padding
                         (:padding-left 5
                                        :padding-right 5
                                        :padding-top 5
                                        :padding-bottom 5)
                         (scenic:label (:text "-"))))))))
              when (not (= i (1- (length (items renderer)))))
              collect (wt
                       (scenic:padding
                         (:padding-left 5
                                        :padding-right 5
                                        :padding-top 5
                                        :padding-bottom 5)
                         (scenic:label (:text ", ")))))))
         (scenic:sizer
          (:min-width 200 :max-width 200
                      :min-height 20 :max-height 20
                      :key :text-input
                      )
          (scenic:textbox
           (:ref (lambda (tb)
                   (setf textbox tb))
                 :cursor-position 0
                 :selection-start 0
                 :caret-color cl-colors:+black+
                 :selection-color cl-colors:+lightblue+
                 :background-color cl-colors:+white+
                 :font-face "Courier"
                 :font-size 14
                 :text "Write what to do here")))
         (scenic:button
          (:on-click
           (lambda (btn ev)
             (setf (items renderer)
                   (append 
                    (items renderer)
                    (list (copy-seq (scenic::text textbox)))))
             (set-dirty renderer)))
          (scenic:padding
           (:padding-left 5
                          :padding-right 5
                          :padding-top 5
                          :padding-bottom 5)
           (scenic:label (:text "+")))                    
          ))))
      )))

(defun wt-todo-list-2 ()
  "(wt-todo-list-2)"
  (let ((scene (scenic:scene
                500 500
                (scenic-helpers:background
                 cl-colors:+black+
                 (scenic-helpers:uniform-padding
                  10
                  (make-instance 'wt-todo-list-2))))))
    (scenic:run-scene scene)))

(scenic:defwidget wt-todo-list-3 (generic-template-widget)
  ((items :initarg :items
          :initform (list "a" "b" "c" "d")
          :accessor items)
   (todo-list :accessor todo-list)))

(defmethod render ((renderer wt-todo-list-3))
  (let (textbox)
    (with-widget-template ()
      (scenic:background
       (:fill-color
        cl-colors:+white+)
       (scenic:pack
        (:orientation :vertical :space-between-cells 5)
        ((scenic:pack
          (:orientation :horizontal :space-between-cells 0)
          (esc 
           (loop
              for item in (items renderer)
              for i from 0
              collect
                (wt (scenic:padding
                     (:padding-left 5
                                    :padding-right 5
                                    :padding-top 5
                                    :padding-bottom 5
                                    :key item
                                    )
                     (scenic:pack
                      (:orientation :horizontal
                                    :space-between-cells 5)
                      ((scenic:label (:text item))
                       (scenic:button
                        (:on-click
                         (let ((itm item))
                           (lambda (btn ev)
                             (setf (items renderer)
                                   (remove itm (items renderer) :test #'eql))
                             (set-dirty renderer)
                             (format *widget-templates-log* "Removed item: ~A~%" itm)
                             (format *widget-templates-log* "Items: ~A~%" (items renderer))
                             )))
                        (scenic:padding
                         (:padding-left 5
                                        :padding-right 5
                                        :padding-top 5
                                        :padding-bottom 5)
                         (scenic:label (:text "-"))))))))
              when (not (= i (1- (length (items renderer)))))
              collect (wt
                       (scenic:padding
                         (:padding-left 5
                                        :padding-right 5
                                        :padding-top 5
                                        :padding-bottom 5)
                         (scenic:label (:text ", ")))))))
         (scenic:sizer
          (:min-width 200 :max-width 200
                      :min-height 20 :max-height 20
                      :key :text-input
                      )
          (scenic:textbox
           (:ref (lambda (tb)
                   (setf textbox tb))
                 :cursor-position 0
                 :selection-start 0
                 :caret-color cl-colors:+black+
                 :selection-color cl-colors:+lightblue+
                 :background-color cl-colors:+white+
                 :font-face "Courier"
                 :font-size 14
                 :text "Write what to do here")))
         (scenic:button
          (:on-click
           (lambda (btn ev)
             (setf (items renderer)
                   (push
                    (copy-seq (scenic::text textbox))
                    (items renderer)))
             (set-dirty renderer)))
          (scenic:padding
           (:padding-left 5
                          :padding-right 5
                          :padding-top 5
                          :padding-bottom 5)
           (scenic:label (:text "+")))                    
          ))))
      )))

(defun wt-todo-list-3 ()
  "(wt-todo-list-3)"
  (let ((scene (scenic:scene
                500 500
                (scenic-helpers:background
                 cl-colors:+black+
                 (scenic-helpers:uniform-padding
                  10
                  (make-instance 'wt-todo-list-3))))))
    (scenic:run-scene scene)))


(scenic:defwidget wt-move-example (generic-template-widget)
  ((items :initarg :items
          :initform (list "foo" "bar" "baz" "zas")
          :accessor items)))

(defmethod render ((renderer wt-move-example))
  (with-widget-template ()
    (scenic:background
     (:fill-color
      cl-colors:+white+)
     (scenic:pack (:orientation :vertical
                                :space-between-cells 3)
                  (esc (append
                        (loop for item in (items renderer)
                           collect
                             (wt (scenic:label (:text item
                                                      :key item
                                                      ))))
                        (list
                         (wt
                          (scenic:button (:on-click (lambda (ev btn)
                                                      (setf (items renderer) (reverse (items renderer)))

                                                      (set-dirty renderer)
                                                      ))
                                         (scenic:label (:text "Reverse"
                                                              :font-color cl-colors:+black+)))))))))))

(defun wt-move-example ()
  "(wt-move-example)"
  (let ((scene (scenic:scene
                500 500
                (scenic-helpers:background
                 cl-colors:+black+
                 (scenic-helpers:uniform-padding
                  10
                  (make-instance 'wt-move-example))))))
    (scenic:run-scene scene)))


;; NON-GENERIC WIDGET TEMPLATES


;; (defmacro def-widget-template (name super props &rest options)
;;   (let* ((widget-class
;;           (cadr (find :widget-class options :key 'car)))
;;          (events (cdr (find :events options :key 'car)))
;;          (props (if (eql props t)
;;                     ;; Extract props from widget class
;;                     (loop for slot in (scenic::prop-slots (find-class widget-class))
;;                        collect (list (c2mop:slot-definition-name slot)
;;                                      :initarg
;;                                      (first (c2mop:slot-definition-initargs slot))
;;                                      :writer
;;                                      (first (scenic::computed-writers-of slot))
;;                                      :reader
;;                                      (first (scenic::computed-readers-of slot))
;;                                      :test (scenic::slot-test slot)))
;;                     props)))
;;     `(progn
;;        (defclass ,name ,super
;;          ,(mapcar (lambda (prop)
;;                     `(,(first prop)
;;                        ,@(when (getf (rest prop) :initarg)
;;                            (list :initarg (getf (rest prop) :initarg)))
;;                        ,@(when (getf (rest prop) :accessor)
;;                            (list :accessor (getf (rest prop) :accessor)))
;;                        ,@(when (getf (rest prop) :writer)
;;                            (list :writer (getf (rest prop) :writer)))
;;                        ,@(when (getf (rest prop) :reader)
;;                            (list :reader (getf (rest prop) :reader)))
;;                        ))
;;                   props))
;;        (defmethod wt-props ((,name ,name))
;;          (append (mapcar (lambda (prop)
;;                            (apply #'make-wt-prop :name (first prop)
;;                                   (rest prop)))
;;                          ',props)
;;                  (call-next-method)))
;;        (defmethod wt-widget-class ((,name ,name))
;;          ',widget-class)
;;        )))

;; (defmethod diff/update ((template-widget template-widget)
;;                         parent old-tpl new-tpl index old-index original-children)
;;   "Similar to wt-diff, but updates the widget tree W directly instead of producing patches"
;;   (cond
;;     ((not old-tpl)
;;      ;; new-tpl is not in old tree. add it.
;;      (if (typep parent 'scenic::container1)
;;          (setf (scenic::child parent) (wt-create-widget new-tpl))
;;          (scenic::add-child parent (wt-create-widget new-tpl)))
;;      ;;(format t "TEMPLATE: ADD CHILD~%")
;;      )
;;     ((not new-tpl)
;;      ;; there's no new tpl. remove from tree.
;;      (if (typep parent 'scenic::container1)
;;          (setf (scenic::child parent) nil)
;;          (scenic::remove-child parent index))
;;      ;;(format t "TEMPLATE: REMOVE CHILD~%")
;;      )
;;     ((wt-changed-p new-tpl old-tpl)
;;      ;; if there are both new-tpl and old-tpl, but they are different, replace.
;;      ;;(format t "TEMPLATE: REPLACE CHILD: ~A ~A~%"
;;      ;;        new-tpl old-tpl)
;;      (cond
;;        ((typep parent 'scenic::container1)
;;         (setf (scenic:child parent) (wt-create-widget new-tpl)))
;;        (t
;;         (scenic::replace-child parent index (wt-create-widget new-tpl)))))
;;     ;; there are two compatible tpls which have not "changed". still we have to look at their properties and children.
;;     ((typep new-tpl 'wt-container)
;;      (let* ((target (cond
;;                       ((typep parent 'scenic::container1)
;;                        (scenic:child parent))
;;                       (t
;;                        (nth index (scenic:children parent)))))
;;             (original-children (alexandria:copy-sequence 'list (scenic:children target))))
;;        ;; update props
;;        (update-props (w-template template-widget) target new-tpl old-tpl)
;;        ;; update children
;;        (loop for i from 0 to (1- (max (length (wt-children new-tpl))
;;                                       (length (wt-children old-tpl))))
;;           do (diff/update template-widget
;;                           target
;;                           (nth i (wt-children old-tpl))
;;                           (nth i (wt-children new-tpl))
;;                           i i
;;                           original-children))))
;;     ((typep new-tpl 'wt-container1)
;;      (let* ((target (cond
;;                       ((typep parent 'scenic::container1)
;;                        (scenic:child parent))
;;                       (t
;;                        (nth index (scenic:children parent)))))
;;             (original-children (alexandria:copy-sequence 'list (scenic:children target))))
;;        ;; update props
;;        (update-props (w-template template-widget) target new-tpl old-tpl)
;;        ;; update child
;;        (diff/update template-widget
;;                     target
;;                     (wt-child old-tpl)
;;                     (wt-child new-tpl)
;;                     0 0 original-children)))
;;     (t
;;      ;;(format t "No change")
;;      )))

;; (def-widget-template wt-button (wt-container1)
;;   t
;;   (:widget-class scenic:button))

;; (def-widget-template wt-label (wt-child)
;;   ;;((text :initarg :text :accessor scenic::text :test string=))
;;   t ;; if T, then props are inferred from widget class
;;   (:widget-class scenic:label))

;; (def-widget-template wt-pack (wt-container)
;;   ((space-between-cells :initarg :space-between-cells
;;                         :accessor scenic::space-between-cells
;;                         :test eql)
;;    (orientation :initarg :orientation
;;                 :accessor scenic::orientation))
;;   (:widget-class scenic:pack))

;; (def-widget-template wt-background (wt-container1)
;;   ((fill-color :initarg :fill-color :accessor scenic:fill-color))
;;   (:widget-class scenic:background))

;; (defun button (child &rest args)
;;   (multiple-value-bind (events args)
;;       (extract-events args)
;;     (apply #'make-instance 'wt-button :child child :events events args)))

;; (defun label (text &rest args)
;;   (apply #'make-instance 'wt-label :text text args))

;; (defun simple-vertical-pack (space children)
;;   (make-instance 'wt-pack
;;                  :space-between-cells space
;;                  :orientation :vertical
;;                  :children children))

;; (defun background (color child)
;;   (make-instance 'wt-background
;;                  :fill-color color
;;                  :child child))



;; (scenic:defwidget wt-example-1 (template-widget)
;;   ((selected-item :initform :opt1 :accessor selected-item)))

;; (defmethod (setf selected-item) :after (new-value (obj wt-example-1))
;;   (setf (dirty-p obj) t))

;; (defmethod render ((renderer wt-example-1))
;;   (background cl-colors:+white+
;;               (simple-vertical-pack
;;                5
;;                (list (button (label "Switch")
;;                              :on-click
;;                              (lambda (btn ev)
;;                                (setf (selected-item renderer)
;;                                      (case (selected-item renderer)
;;                                        (:opt1 :opt2)
;;                                        (:opt2 :opt1)))
;;                                (format t "Selected: ~A~%" (selected-item renderer))
;;                                ))
;;                      (ecase (selected-item renderer)
;;                        (:opt1 (label "Option 1" :key :opt1))
;;                        (:opt2 (label "Option 2" :key :opt2)))))))

;; (defun wt-example-1 ()
;;   "(wt-example-1)"
;;   (let ((scene (scenic:scene 500 500 (make-instance 'wt-example-1))))
;;     (scenic:run-scene scene)))

;; (defun wt-example-2 ()
;;   "This is an example of templates mixed with normal widgets.

;; (wt-example-2)"
;;   (let ((scene (scenic:scene 500 500
;;                              (scenic-helpers:background cl-colors:+black+
;;                                          (scenic-helpers:uniform-padding
;;                                           10
;;                                           (make-instance 'wt-example-1))))))
;;     (scenic:run-scene scene)))
