(in-package :scenic)

(defparameter *debug-text* nil)

;; Look at TCL/TK text widget for ideas. http://www.tkdocs.com/tutorial/text.html

;;-- * TEXT widget

(defclass text-properties ()
  ((alignment :initarg :alignment
              :accessor text-alignment
              :initform :left
              :type (member :inherit :left :right :center :justify)
              :documentation ":left - Aligns the text to the left
:right - Aligns the text to the right
:center - Centers the text
:justify - Stretches the lines so that each line has equal width (like in newspapers and magazines)")
   (word-wrap :initarg :word-wrap
              :accessor word-wrap
              :initform t
              :type (or (member :inherit) t null)
              :documentation "Allows long words to be able to be broken and wrap onto the next line.")
   (font :initarg :font
         :accessor text-font
         :type (or scenic.style:font (member :inherit))
         :initform ;;:inherit
         (scenic.style:font "Arial" :normal 12 cl-colors:+black+)
         )))

(defclass text-container (container text-properties)
  ()
  (:metaclass widget-class))

(defclass text (text-properties)
  ((text :initarg :text
         :initform nil
         :accessor text)
   (offset-x :initarg :x-offset
             :initform 0
             :accessor offset-x))
  (:documentation "Leaf text")
  (:metaclass widget-class))

(defclass paragraph (text-container)
  ()
  (:metaclass widget-class))

(defwidget span (text-container)
  ())

(defclass bold-text (text-container)
  ()
  (:metaclass widget-class))

(defclass italic-text (text-container)
  ()
  (:metaclass widget-class))

(defun get-text-alias-class (alias)
  (ecase alias
    (p 'paragraph)
    (b 'bold-text)
    (i 'italic-text)))

(defun compile-text (text-spec)
  (loop for txt in text-spec
     collect
       (cond
         ((stringp txt)
          `(make-instance 'text :text ,txt))
         ((and (listp txt)
               (member (first txt) '(p b i)))
          `(make-instance ',(get-text-alias-class (first txt))
                          :children (list ,@(compile-text (cddr txt)))
                          ,@(second txt)))
         (t txt))))

(defmacro with-text (args &body body)
  `(make-instance 'text-container
                  :children (list ,@(compile-text body))
                  ,@args))

(with-text (:alignment :left)
  (p ()
     "Hello" (b () "world!!"))
  (p ()
     "This is wrong")
  ;;234 ;; <--- presenters should be used here
  (button (label "Foo")) ;; <--- widgets can be embedded into text
  )

(defclass text-attribute ()
  ((from :initarg :from
         :type integer
         :accessor text-attr-from)
   (to :initarg :to
       :type integer
       :accessor text-attr-to))
  (:documentation "Applies a text attribute (bold, italics, color, etc) to a range of text"))

(defclass text-editor ()
  ()
  (:metaclass widget-class)
  (:documentation "Simple text editor widget"))

(defstruct textpos
  x y line-height lines
  area-width area-height)

(defun textpos-break (textpos height)
  (with-slots (x y line-height lines) textpos
    (setf x 0)
    (incf y line-height)
    (setf line-height height)
    (incf lines)))

(defun textpos-inline (textpos width height)
  (with-slots (x y line-height lines) textpos
    (incf x width)
    (setf line-height (max line-height height))))

(defun textpos-inline-or-break (textpos width height)
  (with-slots (x y line-height lines area-width) textpos
    (if (> (+ x width) area-width)
        ;;break
        (progn
          (setf x 0)
          (incf y line-height)
          (setf line-height height)
          (incf lines)
          :break)
        ;; inline
        (progn
          (incf x width)
          (setf line-height (max line-height height))
          :inline))))

(defgeneric measure-text (widget available-width available-height textpos))

(defmethod measure-text ((widget widget) available-width available-height textpos)
  (multiple-value-bind (width height)
      (measure widget available-width available-height)
    (textpos-inline-or-break textpos width height)))

(defmethod measure ((object text-container) available-width available-height)
  (let ((textpos (make-textpos :x 0 :y 0
                               :line-height 0
                               :area-width available-width
                               :area-height available-height
                               :lines 1)))
    (mapcar (lambda (child)
              (measure-text child available-width available-height textpos))
            (children object))
    (with-slots (x y line-height lines) textpos
      (set-measured object
                    (if (> lines 1)
                        available-width
                        x)
                    (+ y line-height)))))

(defmethod measure ((object text) available-width available-height)
  (scenic.draw:set-font-size (scenic.style:font-size (text-font object)))
  (scenic.draw:select-font-face (scenic.style:font-family (text-font object))
                                (scenic.style:font-slant (text-font object))
                                (scenic.style:font-weight (text-font object)))
  (multiple-value-bind
        (x_bearing y_bearing width height x_advance y_advance)
      (scenic.draw:text-extents (text object))
    (declare (ignore x_bearing y_bearing x_advance y_advance height))
    (let* ((extents (scenic.draw:get-font-extents))
           (ascent (scenic.draw:font-ascent extents))
           (descent (scenic.draw:font-descent extents))
           (height (+ ascent descent)))
      (set-measured object
                    (min width available-width)
                    (min height available-height)))))

(defmethod measure-text ((object text-container) available-width available-height textpos)
  (let ((initial-x (textpos-x textpos))
        (initial-y (textpos-y textpos))
        (initial-lines (textpos-lines textpos)))
    (mapcar (lambda (child)
              (measure-text child available-width available-height textpos))
            (children object))
    (with-slots (x y line-height lines) textpos
      (set-measured object
                    (if (> lines initial-lines)
                        available-width
                        (- x initial-x))
                    (- (+ y line-height) initial-y)))))

(defmethod measure-text ((object text) available-width available-height textpos)
  (flet ((string-width (text)
           (multiple-value-bind
                 (x_bearing y_bearing width height x_advance y_advance)
               (scenic.draw:text-extents text)
             (declare (ignore x_bearing y_bearing x_advance y_advance))
             (values width height))))

    (scenic.draw:set-font-size (scenic.style:font-size (text-font object)))
    (scenic.draw:select-font-face (scenic.style:font-family (text-font object))
                                  (scenic.style:font-slant (text-font object))
                                  (scenic.style:font-weight (text-font object)))
    (let* ((extents (scenic.draw:get-font-extents))
           (ascent (scenic.draw:font-ascent extents))
           (descent (scenic.draw:font-descent extents))
           (height (+ ascent descent))
           (initial-y (textpos-y textpos))
           (initial-x (textpos-x textpos)))
      (let ((words (split-sequence:split-sequence
                    #\space (text object)
                    :remove-empty-subseqs t)))
        (dolist (word words)
          (multiple-value-bind (word-width word-height)
              (string-width word)
            (let ((action
                   (textpos-inline-or-break textpos word-width word-height)))
              (when (eql action :break)
                (setf (measured-width object) available-width))))
          (let ((action
                 (textpos-inline-or-break textpos (* (space-width) 2)
                                          (textpos-line-height textpos))
                  (when (eql action :break)
                    (setf (measured-width object) available-width))
                  )))))
      (when (null (measured-width object))
        (setf (measured-width object) (- (textpos-x textpos)
                                         initial-x)))
      (setf (measured-height object)
            (- (+ (textpos-y textpos)
                  (textpos-line-height textpos))
               initial-y)))))

(defmethod layout ((object text-container) left top width height)
  (let ((textpos (make-textpos
                  :x 0 :y 0
                  :line-height 0
                  :area-width width
                  :area-height height
                  :lines 1)))
    (mapcar (lambda (child)
              (layout-text child
                           left top
                           width height textpos))
            (children object))
    (with-slots  (x y line-height lines) textpos
      (set-layout object left top
                  (if (> lines 1)
                      width
                      x)
                  (+ y line-height)))))

(defmethod layout-text ((widget widget) left top width height textpos)
  (layout widget
          (+ left (textpos-x textpos))
          (+ top (textpos-y textpos))
          (measured-width widget)
          (measured-height widget)
          )
  (textpos-inline-or-break textpos
                           (measured-width widget)
                           (measured-height widget)
                           ))

(defmethod layout-text ((object text-container) left top width height textpos)
  (let ((initial-x (textpos-x textpos))
        (initial-y (textpos-y textpos))
        (initial-lines (textpos-lines textpos)))
    (mapcar (lambda (child)
              (layout-text child
                           left top
                           width height textpos))
            (children object))
    (with-slots  (x y line-height lines) textpos
      (set-layout object
                  (if (> lines initial-lines)
                      left
                      (+ left initial-x))
                  (+ top initial-y)
                  (measured-width object)
                  (measured-height object)))))

(defmethod layout-text ((object text) left top width height textpos)
  (flet ((string-width (text)
           (multiple-value-bind
                 (x_bearing y_bearing width height x_advance y_advance)
               (scenic.draw:text-extents text)
             (declare (ignore x_bearing y_bearing x_advance y_advance))
             (values width height))))

    ;; update the textpos

    (scenic.draw:set-font-size (scenic.style:font-size (text-font object)))
    (scenic.draw:select-font-face (scenic.style:font-family (text-font object))
                                  (scenic.style:font-slant (text-font object))
                                  (scenic.style:font-weight (text-font object)))
    (let* ((extents (scenic.draw:get-font-extents))
           (ascent (scenic.draw:font-ascent extents))
           (descent (scenic.draw:font-descent extents))
           (height (+ ascent descent))
           (initial-x (textpos-x textpos))
           (initial-y (textpos-y textpos))
           (initial-lines (textpos-lines textpos)))
      (let ((words (split-sequence:split-sequence
                    #\space (text object)
                    :remove-empty-subseqs t)))
        (dolist (word words)
          (multiple-value-bind (word-width word-height)
              (string-width word)
            (textpos-inline-or-break textpos word-width word-height))
          #+nil(multiple-value-bind (space-width space-height) (string-width " ")
                 (textpos-inline-or-break textpos space-width space-height))
          (textpos-inline-or-break textpos (* (space-width) 2)
                                   (textpos-line-height textpos))
          ))
      (setf (offset-x object) (textpos-x textpos))
      
      ;; set layout
      (set-layout object
                  (if (> (textpos-lines textpos) initial-lines)
                      left
                      (+ left initial-x))
                  (+ top (textpos-y textpos))
                  (measured-width object)
                  (measured-height object))
      )))


(defmethod paint ((object text-container))
  (let ((textpos (make-textpos
                  :x (layout-left object)
                  :y (layout-top object)
                  :line-height 0
                  :area-width (layout-width object)
                  :area-height (layout-height object)
                  :lines 1)))
    (mapcar (lambda (child)
              (paint-text child textpos))
            (children object))))

(defmethod paint-text ((object text) textpos)
  (flet ((string-width (text)
           (multiple-value-bind
                 (x_bearing y_bearing width height x_advance y_advance)
               (scenic.draw:text-extents text)
             (declare (ignore x_bearing y_bearing x_advance y_advance))
             (values width height))))

    (scenic.draw:set-color (scenic.style:font-color (text-font object)))
    (scenic.draw:set-font-size (scenic.style:font-size (text-font object)))
    (scenic.draw:select-font-face (scenic.style:font-family (text-font object))
                                  (scenic.style:font-slant (text-font object))
                                  (scenic.style:font-weight (text-font object)))
    (let* ((extents (scenic.draw:get-font-extents))
           (ascent (scenic.draw:font-ascent extents))
           (descent (scenic.draw:font-descent extents))
           (height (+ ascent descent)))
      (let ((words (split-sequence:split-sequence
                    #\space (text object)
                    :remove-empty-subseqs t)))
        (dolist (word words)
          (with-slots (x y) textpos
                                        ;(format t "Displaying ~s at ~A~%" word (cons x y))
            (scenic.draw:move-to (+ #+nil(layout-left object) x)
                                 (+ #+nil(layout-top object) y ascent))
            (scenic.draw:show-text word))
          (multiple-value-bind (word-width word-height)
              (string-width word)
            (textpos-inline-or-break textpos word-width word-height))
          #+nil(multiple-value-bind (space-width space-height)
                   (string-width "   ")
                 (textpos-inline-or-break textpos space-width space-height))
          (textpos-inline-or-break textpos (* (space-width) 2) (textpos-line-height textpos))
          )))))

(defmethod paint-text ((object widget) textpos)
  (paint object)
  (textpos-inline-or-break textpos
                           (+ (measured-width object) 20)
                           (measured-height object)))

(defmethod paint-text ((object text-container) textpos)
  (mapcar (lambda (child)
            (paint-text child textpos))
          (children object))
  #+nil(textpos-inline-or-break textpos
                           (measured-width object)
                           (measured-height object))
  )

(defun text-test ()
  (let ((text (with-text ()
                "Hello" " world!!" " What is this"
                (button (label "lalal"))
                "  jajaja")))
    (run-scene
     (scene 500 500 (scenic-helpers:background cl-colors:+white+
                                               (scenic-helpers:uniform-padding 10  text)))
     :window-options
     (list :resizable t :title "Text test"))))

(defun lorem-test ()
  (let ((text (with-text ()
                (p ()
                   "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ut nisl varius, tincidunt orci nec, pellentesque lectus. Nulla a odio nec risus gravida sodales non eget justo. Nulla fermentum consequat commodo. Aenean justo justo, commodo laoreet tellus in, accumsan scelerisque nibh. Fusce laoreet aliquam sapien, in tempor leo ultrices eu. Aliquam dignissim leo non metus ultrices sollicitudin. Pellentesque non purus eu urna pellentesque sodales quis id lacus."
                   (scenic-helpers:button (scenic-helpers:label "Hello!!"))
                   "The end"
                   )
                (p () "yes" (scenic-helpers:button (scenic-helpers:label "lalal")) "   no")
                (p () (sizer
                          (image (asdf:system-relative-pathname
                                  :scenic
                                  "icons/lisplogo_warning_256.png"))
                          :width 165 :height 256))
                (p ()
                   "Ut a sodales est. Duis commodo elit mi, ac consequat velit consectetur ac. Suspendisse venenatis tellus ac lectus pretium, a hendrerit turpis consectetur. Nam porta nisl et maximus fringilla. Etiam ac est lobortis, ultricies mi at, dignissim velit. Mauris ultricies leo felis. Phasellus sollicitudin euismod laoreet. Phasellus rutrum, nibh et tempor porttitor, ipsum nulla pharetra dui, quis facilisis risus ipsum sed nisl.
"))))
    (run-scene
     (scene 500 500 (scenic-helpers:background
                     cl-colors:+white+
                     (scenic-helpers:uniform-padding 10 text)
                                        ;text
                     )
            )
     :window-options
     (list :resizable t :title "Text test"))))

(defmethod measure-text :before ((object paragraph) available-width available-height textpos)
  (textpos-break textpos 0)
  )

(defmethod layout-text :before ((object paragraph) left top width height textpos)
  (textpos-break textpos 0)
  )

(defmethod paint-text :before (widget textpos)
  (when *debug-text*
    (scenic.draw:rectangle (textpos-x textpos)
                           (textpos-y textpos)
                           4 4)
    (scenic.draw:set-source-rgba 1 0 0 1)
    (scenic.draw:fill-path)))

(defmethod paint-text :after (widget textpos)
  (when *debug-text*
    (scenic.draw:rectangle (textpos-x textpos)
                           (textpos-y textpos)
                           4 4)
    (scenic.draw:set-source-rgba 0 0 1 1)
    (scenic.draw:fill-path)))

(defmethod paint-text :before ((widget paragraph) textpos)
  (textpos-break textpos 0)
  (when *debug-text*
    (scenic.draw:rectangle (layout-left widget)
                           (layout-top widget)
                           (layout-width widget)
                           (layout-height widget))
    (scenic.draw:set-source-rgba 0 0 1 0.5)
    (scenic.draw:fill-path)))

(defmethod paint-text :before ((widget text) textpos)
  (when *debug-text*
    (scenic.draw:rectangle (layout-left widget)
                           (layout-top widget)
                           (layout-width widget)
                           (layout-height widget))
    (scenic.draw:set-source-rgba 0 1 0 0.5)
    (scenic.draw:fill-path)))

(defmethod paint ((object paragraph))
  ;;(call-next-method)
  )
