(in-package :scenic-utils)

(defun max-box (boxes)
  (list (apply #'max (mapcar #'first boxes))
        (apply #'max (mapcar #'second boxes))))

(defun orzero (x)
  (or x 0))

;; Rects

(defclass rect ()
  ((left :accessor left :initarg :left :initform 0)
   (top :accessor top :initarg :top :initform 0)
   (width :accessor width :initarg :width :initform 0)
   (height :accessor height :initarg :height :initform 0)))

(defun right (rect)
  (1- (+ (left rect) (width rect))))

(defun bottom (rect)
  (1- (+ (top rect) (height rect))))

(defclass point ()
  ((x :accessor x :initarg :x :initform 0)
   (y :accessor y :initarg :y :initform 0)))

(defun corners-of-rectangle (rect)
  (with-slots (left top width height) rect
    (list (make-instance 'point :x left :y top)
          (make-instance 'point :x (1- (+ left width)) :y top)
          (make-instance 'point :x left :y (1- (+ top height)))
          (make-instance 'point :x (1- (+ left width)) :y (1- (+ top height))))))

(defun in-rect (x y rect)
  (with-slots (left top width height) rect
    (and (<= left x)
         (< x (+ left width))
         (<= top y)
         (< y (+ top height)))))

(defun rect-intersect (rect1 rect2)
  (labels ((rect-intersection ()
             (let ((left (max (left rect1) (left rect2)))
                   (top (max(top rect1) (top rect2)))
                   (right (min (right rect1) (right rect2)))
                   (bottom (min (bottom rect1) (bottom rect2))))
               (make-instance 'rect
                              :left left
                              :top top
                              :width (1+ (- right left))
                              :height (1+ (- bottom top)))))
           (intersect-p ()
             (or (dolist (corner (corners-of-rectangle rect1))
                   (if (in-rect (x corner) (y corner) rect2)
                       (return t)))
                 (dolist (corner (corners-of-rectangle rect2))
                   (if (in-rect (x corner) (y corner) rect1)
                       (return t))))))
    (when (intersect-p)
      (rect-intersection))))

(defun rect-union (rect1 rect2)
  (let ((left (min (left rect1) (left rect2)))
        (top (min (top rect1) (top rect2))))
    (make-instance 'rect
                   :left left
                   :top top
                   :width (- (max (right rect1) (right rect2)) left)
                   :height (- (max (bottom rect1) (bottom rect2)) top))))       

(defun layout-rect (widget)
  (make-instance 'rect
                 :left (layout-left widget)
                 :top (layout-top widget)
                 :width (layout-width widget)
                 :height (layout-height widget)))

(defun intersects-clip-rect (widget offset-x offset-y clip-rect)
  (let ((widget-rect (layout-rect widget)))
    (decf (left widget-rect) offset-x)
    (decf (top widget-rect) offset-y)
    (if (null clip-rect)
        t
        (rect-intersect widget-rect clip-rect))))

(defun common-bounding-box (bbox1 bbox2)
  (list (min (first bbox1) (first bbox2))
        (min (second bbox1) (second bbox2))
        (max (third bbox1) (third bbox2))
        (max (fourth bbox1) (fourth bbox2))))

(defun visible-rect (widget offset-x offset-y clip-rect)
  "Calculates the visible area of WIDGET, taking into account OFFSET values and CLIP region (they are set when the widget is inside a clipped or scrolling widget)."
  (let ((visible-rect (layout-rect widget)))
    (decf (left visible-rect) offset-x)
    (decf (top visible-rect) offset-y)
    (if (null clip-rect)
        visible-rect
        (rect-intersect visible-rect clip-rect))))

(defun fill-rect (left top width height
                  &key fill-color corner-radius)
  (if corner-radius
      (let ((left (+ (/ corner-radius 2) left))
            (top (+ (/  corner-radius 2) top))
            (width (- width (/  corner-radius 2)))
            (height (- height (/ corner-radius 2))))
              
          (scenic.draw:arc left
                         top
                         corner-radius
                         (/ (* 2 pi) 2)
                         (/ (* 3 pi) 2))

          (scenic.draw:arc (+ left width)
                         top
                         corner-radius
                         (/ (* 3 pi) 2)
                         (/ (* 4 pi) 2))

          (scenic.draw:arc (+ left width)
                         (+ top height)
                         corner-radius
                         (/ (* 0 pi) 2)
                         (/ (* 1 pi) 2))

          (scenic.draw:arc left
                         (+ top height)
                         corner-radius
                         (/ (* 1 pi) 2)
                         (/ (* 2 pi) 2)))
        ;; else
        (scenic.draw:rectangle left
                             top
                             width
                             height))
  (scenic.draw:set-color fill-color)
  (scenic.draw:fill-path))

(defun draw-rect (left top width height
                  &key (stroke-width 1)
                    (stroke-color cl-colors:+black+)
                    fill-color corner-radius)
  (let ((left left)
        (top top)
        (width width)
        (height height))
    (if corner-radius
        (let ((left (+ (/ corner-radius 2) left))
              (top (+ (/  corner-radius 2) top))
              (width (- width (/  corner-radius 2)))
              (height (- height (/ corner-radius 2))))
          (scenic.draw:new-path)
              
          (scenic.draw:arc (+ left corner-radius)
                         (+ top corner-radius)
                         corner-radius
                         (/ (* 2 pi) 2)
                         (/ (* 3 pi) 2))

          (scenic.draw:arc (- (+ left width) corner-radius)
                         (+ top corner-radius)
                         corner-radius
                         (/ (* 3 pi) 2)
                         (/ (* 4 pi) 2))

          (scenic.draw:arc (- (+ left width) corner-radius)
                         (- (+ top height) corner-radius)
                         corner-radius
                         (/ (* 0 pi) 2)
                         (/ (* 1 pi) 2))

          (scenic.draw:arc (+ left corner-radius)
                         (- (+ top height) corner-radius)
                         corner-radius
                         (/ (* 1 pi) 2)
                         (/ (* 2 pi) 2))
          (scenic.draw:close-path))
        ;; else
        (scenic.draw:rectangle left
                             top
                             width
                             height)))
  (when stroke-color
    (scenic.draw:set-color stroke-color)
    (scenic.draw:set-line-width stroke-width)
    (scenic.draw:stroke-preserve))
  (when fill-color
    (scenic.draw:set-fill-extent fill-color height)
    (scenic.draw:set-color fill-color)
    (scenic.draw:fill-path)))

;; Box


(defun fill-box (left top width height
                  &key fill-color corner-radius)
  "Fills a rectangle with the given color, with the posibility of specifying corner radiuses"
  
  ;; We build a Cairo path in this order: top-left, top-right, bottom-right, bottom-left, taking into account corner radius. If corner radius is present, then we draw an arc; we draw a direct line otherwise.
  
  (if corner-radius
      (destructuring-bind (radius-top-left radius-top-right
                                           radius-bottom-right radius-bottom-left)
          (scenic.style:expand-style-spec corner-radius)

        (scenic.draw:new-path)

        ;; Left top corner
        (if radius-top-left
            (scenic.draw:arc (+ radius-top-left left)
                           (+ radius-top-left top)
                           radius-top-left
                           (/ (* 2 pi) 2)
                           (/ (* 3 pi) 2))
            (scenic.draw:move-to left
                               top))

        ;; Top right corner
            
        (if radius-top-right
            (scenic.draw:arc (+ left (- width
                                      radius-top-right
                                      ))
                           (+ top radius-top-right)
                           radius-top-right
                           (/ (* 3 pi) 2)
                           (/ (* 4 pi) 2))
            (scenic.draw:line-to (+ left width)
                               top))

        ;; Bottom right corner

        (if radius-bottom-right
            (scenic.draw:arc (+ left (- width
                                      radius-bottom-right))
                           (+ top (- height
                                     radius-bottom-right))
                           radius-bottom-right
                           (/ (* 0 pi) 2)
                           (/ (* 1 pi) 2))
            (scenic.draw:line-to (+ left width)
                               (+ top height)))

        ;; Bottom left corner

        (if radius-bottom-left
            (scenic.draw:arc (+ left
                              radius-bottom-left)
                           (+ top (- height
                                     radius-bottom-left))
                           radius-bottom-left
                           (/ (* 1 pi) 2)
                           (/ (* 2 pi) 2))
            (scenic.draw:line-to left
                               (+ top height)))

        (scenic.draw:close-path)
        )
      
      ;; else, there are no corner radius, we can draw a rectangle directly
      (scenic.draw:rectangle left
                           top
                           width
                           height))

  ;; The path was build. Fill it with given color
  (scenic.draw:set-color fill-color)
  (scenic.draw:fill-path))

(defun draw-box-border
    (left top width height
     &key border corner-radius)
  (destructuring-bind (border-top border-right border-bottom border-left)
      (scenic.style:expand-style-spec border)

      
    (destructuring-bind (radius-top-left radius-top-right
                                         radius-bottom-right radius-bottom-left)
        (scenic.style:expand-style-spec corner-radius)

      ;; Adjust sizes using border sizes
      (let ((left0 (+ left (orzero
                            (and border-left
                                 (/ (scenic.style:border-width border-left) 2)))))
            (top0 (+ top (orzero
                          (and border-top
                               (/ (scenic.style:border-width border-top) 2)))))
            (width0 (- width
                       (orzero
                        (and border-right
                             (/ (scenic.style:border-width border-right) 2)))
                       (orzero
                        (and border-left
                             (/ (scenic.style:border-width border-left) 2)))))
            (height0 (- height (orzero
                                (and border-bottom
                                     (/ (scenic.style:border-width border-bottom) 2)))
                        (orzero
                         (and border-top
                              (/ (scenic.style:border-width border-top) 2))))))
          
        ;; Top border

        (when border-top
              
          (scenic.draw:new-path)

          (scenic.draw:set-line-cap :square)

          ;; Left top corner
          (if radius-top-left
              (progn
                (scenic.draw:move-to left 
                               (+ radius-top-left top))
                (scenic.draw:arc (+ radius-top-left left)
                           (+ radius-top-left top)
                           radius-top-left
                           (/ (* 2 pi) 2)
                           (/ (* 3 pi) 2))
                (scenic.draw:line-to
                 (+ left (- width 
                            (orzero radius-top-right)))
                 top))
              (progn
                (scenic.draw:move-to left
                               top0)
                (scenic.draw:line-to
                 (+ left (- width
                            (orzero radius-top-right)))
                 top0)))

          (scenic.draw:set-color (scenic.style:border-color border-top))
          (scenic.draw:set-line-width (scenic.style:border-width border-top))
          
          (scenic.draw:stroke))

        ;; Right border

        (when border-right
          (scenic.draw:new-path)

          (scenic.draw:set-line-cap :square)
          
          (if radius-top-right
              (progn
                (scenic.draw:move-to (+ left (- width
                                          radius-top-right))
                               top)
                (scenic.draw:arc (+ left (- width
                                      radius-top-right))
                           (+ top radius-top-right)
                           radius-top-right
                           (/ (* 3 pi) 2)
                           (/ (* 4 pi) 2))
                (scenic.draw:line-to (+ left width)
                               (+ top (- height
                                         (orzero radius-bottom-right)
                                         ))))
              (progn
                (scenic.draw:move-to (+ left0 width0)
                               top)
                (scenic.draw:line-to (+ left0 width0)
                               (+ top (- height 
                                         (orzero radius-bottom-right)
                                         )))))
              
          (scenic.draw:set-color (scenic.style:border-color border-right))
          (scenic.draw:set-line-width (scenic.style:border-width border-right))
          (scenic.draw:stroke))

        ;; Bottom border

        (when border-bottom
          (scenic.draw:new-path)

          (scenic.draw:set-line-cap :square)
          
          (if radius-bottom-right
              (progn
                (scenic.draw:move-to (+ left width)
                               (+ top (- height radius-bottom-right)))
                
                (scenic.draw:arc (+ left (- width
                                      radius-bottom-right))
                           (+ top (- height
                                     radius-bottom-right))
                           radius-bottom-right
                           (/ (* 0 pi) 2)
                           (/ (* 1 pi) 2))
                (scenic.draw:line-to (+ left (orzero radius-bottom-left))
                               (+ top height)))
              (progn
                (scenic.draw:move-to (+ left width) (+ top0 height0))
                (scenic.draw:line-to (+ left (orzero radius-bottom-left))
                               (+ top0 height0))))
          
          (scenic.draw:set-color (scenic.style:border-color border-bottom))
          (scenic.draw:set-line-width (scenic.style:border-width border-bottom))
          (scenic.draw:stroke))
        
        ;; Left border
        
        (when border-left
          (scenic.draw:new-path)
          (scenic.draw:set-line-cap :square)
          
          (if radius-bottom-left
              (progn
                (scenic.draw:move-to (+ left radius-bottom-left)
                               (+ top height))
                (scenic.draw:arc (+ left radius-bottom-left)
                           (+ top (- height radius-bottom-left))
                           radius-bottom-left
                           (/ (* 1 pi) 2)
                           (/ (* 2 pi) 2))
                (scenic.draw:line-to left (+ top (orzero radius-top-left))))          
              (progn
                (scenic.draw:move-to left0 (+ top height))
                (scenic.draw:line-to left0
                               (+ top (orzero radius-top-left)))))
          
          (scenic.draw:set-color (scenic.style:border-color border-left))
          (scenic.draw:set-line-width (scenic.style:border-width border-left))
          (scenic.draw:stroke))))))


(defun draw-box (left top width height
                  &key border fill-color corner-radius)
  "Draw a boxangle. It supports fillcolor, borders and radius.
See: https://github.com/litehtml/litehtml/blob/320810f2f4faa78626fc6df8130bae9de42ba988/containers/linux/container_linux.cpp"

  ;; 1. background color
  ;; 2. background image
  ;; 3. border
  ;; 4. children
  ;; 5. outline
  
  ;; Draw the background shape first
  (when fill-color
    (fill-box left top width height
               :fill-color fill-color
               :corner-radius corner-radius))
  
  ;; Draw the border
  (when border
    #+nil(draw-box-border left top width height
                      :border border
                      :corner-radius corner-radius)
    (draw-borders left top width height
                  :border border
                  :corner-radius corner-radius)

    ))

(defun draw-button-raw (left top width height pressed)
  ;; draw the inner borders (for the 3d illusion)
  ;; left border
  (scenic.draw:move-to (+ 0.5 left)
                     (+ 0.5 top))
  (scenic.draw:line-to (+ 0.5 left)
                     (+ top height))
  ;; upper border
  (scenic.draw:move-to left
                     (+ 0.5 top))
  (scenic.draw:line-to (+ left width)
                     (+ 0.5 top))
  ;; draw
  (scenic.draw:set-line-width 1)
  (if pressed
      (scenic.draw:set-source-rgb 0.3 0.3 0.3)
      (scenic.draw:set-source-rgb 0.9 0.9 0.9))
  (scenic.draw:stroke)
  ;; lower border
  (scenic.draw:move-to (+ 1 left)
                     (- (+ top height) 0.5))
  (scenic.draw:line-to (+ left width)
                     (- (+ top height) 0.5))
  ;; right border
  (scenic.draw:move-to (- (+ left width) 0.5)
                     (+ 1 top))
  (scenic.draw:line-to (- (+ left width) 0.5)
                     (- (+ top height) 1))
  ;; draw
  (scenic.draw:set-line-width 1)
  (if pressed
      (scenic.draw:set-source-rgb 0.9 0.9 0.9)
      (scenic.draw:set-source-rgb 0.3 0.3 0.3))
  (scenic.draw:stroke)
  ;; draw the background
  (scenic.draw:rectangle (+ 1 left) (+ 1 top)
                       (- width 2) (- height 2))
  ;;(scenic.draw:set-source-rgb 0.8 0.8 0.8)
  (scenic.draw:set-source-rgb 240/255 239/255 238/255)
  (scenic.draw:fill-path))

(defmacro pass-to-child (class child-slot property-slot)
  `(defmethod (setf ,property-slot) :after (value (instance ,class))
              (when (not (= (,property-slot (,child-slot instance)) (,property-slot instance)))
                (setf (,property-slot (,child-slot instance)) (,property-slot instance))
                (scenic:invalidate (,child-slot instance)))))

(defmacro ifhorizontal (instance horizontal-body &optional (vertical-body nil))
  `(if (eq (scenic:orientation ,instance) :horizontal)
       ,horizontal-body
       ,vertical-body))

(defun make-keyword (str)
  (intern (string-upcase str) "KEYWORD"))

(defmacro let-from-options (options variables &body body)
  (let ((goption (gensym "option")))
    `(let ,variables
       (loop
          for ,goption in ,options
            ,@(mapcan (lambda (var)
                        `(when (eq ,(make-keyword (symbol-name (first var))) (car ,goption))
                           do (setf ,(first var) (cdr ,goption))))
                      variables))
       ,@body)))

(defun fill-list (list desired-count element)
  (let ((add-count (- desired-count (length list))))
    (if (> add-count 0)
        (append list
                (loop
                   for i from 1 to add-count
                   collect element))
        list)))

(defun groups (list n)
  (cond ((null list) nil)
        ((< (length list) n)
         (list list))
        (t (cons (subseq list 0 n)
                 (groups (subseq list n) n)))))

(defmacro gen-print-object (class slots)
  (when (not (symbolp class))
    (error "Class should be a symbol!"))
  `(defmethod print-object ((object ,class) stream)
     (write-string (string-upcase (format nil
                                          ,(format nil "~a (~{~a~^, ~})"
                                                   (symbol-name class)
                                                   (mapcar (lambda (slot)
                                                             (format nil "~a:~~a" slot))
                                                           slots))
                                          ,@(mapcar (lambda (slot) `(slot-value object ',slot))
                                                    slots)))
                   stream)))

(defmacro gen-serializer (class slots)
  (when (not (symbolp class))
    (error "Class should be a symbol!"))
  `(defmethod scenic:serialize ((object ,class))
     (list ',class
           ,@(mapcan (lambda (slot) `(,(make-keyword slot) (slot-value object ',slot)))
                     slots))))

(defun yes-no-query (prompt)
  (loop
     (format t "~%~a [Y/N] " prompt)
     (let ((reply (read-char)))
       (when (or (char-equal reply #\y)
                 (char-equal reply #\n))
         (return (char-equal reply #\y)))
       (format t "~%Please type 'Y' or 'N'."))))

(defmacro -> (obj &rest forms)
  "Similar to the -> macro from clojure, but with a tweak: if there is
  a $ symbol somewhere in the form, the object is not added as the
  first argument to the form, but instead replaces the $ symbol."
  (if forms
      (if (consp (car forms))
          (let* ((first-form (first forms))
                 (other-forms (rest forms))
                 (pos (position '$ first-form)))
            (if pos
                `(-> ,(append (subseq first-form 0 pos)
                              (list obj)
                              (subseq first-form (1+ pos)))
                     ,@other-forms)
                `(-> ,(list* (first first-form) obj (rest first-form))
                     ,@other-forms)))
          `(-> ,(list (car forms) obj)
               ,@(cdr forms)))
      obj))

(defmacro set2val1 (var form)
  "The FORM returns at least two values. This macro sets the second
  value to the given VAR, and returns the first value."
  (let ((g-val (gensym "VAL")))
    `(let (,g-val)
       (setf (values ,g-val ,var) ,form)
       ,g-val)))

(defun validate-layout-spec (layout-spec)
  (cond ((null layout-spec) (values))
        ((eq (first layout-spec) :auto) (validate-layout-spec (cdr layout-spec)))
        ((consp (first layout-spec))
         (let ((arg (first (first layout-spec)))
               (kind (second (first layout-spec))))
           (if (and (numberp arg)
                    (or (eq :auto kind) (eq :px kind) (eq :ext kind)))
               (validate-layout-spec (cdr layout-spec))
               (error (format nil "Invalid layout option ~a." (first layout-spec))))))
        (t (error (format nil "Invalid layout option ~a." (first layout-spec))))))

(defun is-auto (layout-spec-cell)
  (or (eq :auto layout-spec-cell)
      (and (consp layout-spec-cell)
           (= 2 (length layout-spec-cell))
           (eq :auto (second layout-spec-cell)))))

(defun sorted-auto-indices (layout-spec)
  (labels ((adjust-layout-spec (layout-spec next-auto)
             (when layout-spec
               (if (is-auto (car layout-spec))
                   (if (eq :auto (car layout-spec))
                       (cons (list next-auto :auto)
                             (adjust-layout-spec (cdr layout-spec) (1+ next-auto)))
                       (cons (car layout-spec) (adjust-layout-spec (cdr layout-spec) next-auto)))
                   (cons (car layout-spec) (adjust-layout-spec (cdr layout-spec) next-auto))))))
    (let* ((autos (remove-if-not #'is-auto layout-spec)))
      (when autos
        (let ((max-option (1+ (apply #'max (mapcar (lambda (auto)
                                                     (if (eq :auto auto)
                                                         0
                                                         (first auto)))
                                                   autos)))))
          (-> (adjust-layout-spec layout-spec max-option)
              (loop
                 for lo in $
                 for pos = 0 then (1+ pos)
                 when (is-auto lo)
                 collect (list pos (first lo)))
              (sort $ #'< :key #'second)
              (mapcar #'first $)))))))

(defun intersperse (list elm)
  (cond ((>= (length list) 2)
         (list* (car list)
                elm
                (intersperse (cdr list) elm)))
        (t list)))

(defun color (source)
  (scenic.draw:parse-color-or-pattern source))

(defun add-path-arc (x y rx ry a1 a2 neg)
  (if (and (plusp rx) (plusp ry))
      (progn
        (scenic.draw:save)
        (scenic.draw:translate x y)
        (scenic.draw:scale 1 (/ ry rx))
        (scenic.draw:translate (- x) (- y))
        (if neg
            (scenic.draw:arc-negative x y rx a1 a2)
            ;; else
            (scenic.draw:arc x y rx a1 a2))
        (scenic.draw:restore))
      (scenic.draw:move-to x y)))

(defun draw-borders (left top width height
                     &key border corner-radius)
  (scenic.draw:save)
  (scenic.draw:new-path)

  (let ((right (+ left width))
        (bottom (+ top height)))
    
    (destructuring-bind (border-top border-right border-bottom border-left)
        (scenic.style:expand-style-spec border)
      
      (destructuring-bind (radius-top-left radius-top-right
                                           radius-bottom-right radius-bottom-left)
          (scenic.style:expand-style-spec corner-radius)

        (let ((bdr-left (orzero
                         (and border-left
                              (scenic.style:border-width border-left))))
              (bdr-top (orzero
                        (and border-top
                             (scenic.style:border-width border-top))))
              (bdr-right (orzero
                          (and border-right
                               (scenic.style:border-width border-right))))
              (bdr-bottom (orzero
                           (and border-bottom
                                (scenic.style:border-width border-bottom)))))
  

          ;; right border

          (when (plusp bdr-right)
            (scenic.draw:set-color (scenic.style:border-color border-right))

            (let ((r-top radius-top-right)
                  (r-bottom radius-bottom-right))
              (if (and r-top (plusp r-top))
                  (let* ((end-angle (* 2 pi))
                         (start-angle (- end-angle (1+ (/ pi 2 bdr-top bdr-right)))))
                    (add-path-arc (- right r-top)
                                  (+ top r-top)
                                  (- r-top bdr-right)
                                  (+ (- r-top bdr-right)
                                     (- bdr-right bdr-top))
                                  end-angle
                                  start-angle
                                  t)

                    (add-path-arc (- right r-top)
                                  (+ top r-top)
                                  r-top
                                  r-top
                                  start-angle
                                  end-angle
                                  nil))
                  ;; else
                  (progn
                    (scenic.draw:move-to (- right bdr-right)
                                   (+ top bdr-top))
                    (scenic.draw:line-to right top)))

              (if (and r-bottom (plusp r-bottom))
                  (progn
                    (scenic.draw:line-to right (- bottom r-bottom))

                    (let* ((start-angle 0)
                           (end-angle (+ start-angle (/ pi 2 (1+ (/ bdr-bottom bdr-right))))))

                      (add-path-arc
                       (- right r-bottom)
                       (- bottom r-bottom)
                       r-bottom
                       r-bottom
                       start-angle
                       end-angle
                       nil)

                      (add-path-arc
                       (- right r-bottom)
                       (- bottom r-bottom)
                       (- r-bottom bdr-right)
                       (+ (- r-bottom bdr-right)
                          (- bdr-right bdr-bottom))
                       end-angle
                       start-angle
                       t)))
                  ;; else
                  (progn
                    (scenic.draw:line-to right bottom)
                    (scenic.draw:line-to (- right bdr-right) (- bottom bdr-bottom)))))
            (scenic.draw:fill-path))

          ;; bottom border

          (when (and bdr-bottom (plusp bdr-bottom))

            (scenic.draw:set-color (scenic.style:border-color border-bottom))

            (let ((r-left radius-bottom-left)
                  (r-right radius-bottom-right))
             
              (if (and r-left (plusp r-left))
                  (let* ((start-angle (/ pi 2))
                         (end-angle (+ start-angle (/ pi 2 (+ 1 (/ bdr-left bdr-bottom))))))
                    (add-path-arc
                     (+ left r-left)
                     (- bottom r-left)
                     (+ (- r-left bdr-bottom)
                        (- bdr-bottom bdr-left))
                     (- r-left bdr-bottom)
                     start-angle
                     end-angle
                     nil)

                    (add-path-arc
                     (+ left r-left)
                     (- bottom r-left)
                     r-left
                     r-left
                     end-angle
                     start-angle
                     t))
                  ;; else
                  (progn
                    (scenic.draw:move-to left bottom)
                    (scenic.draw:line-to (+ left bdr-left) (- bottom bdr-bottom))))

              (if (and r-right (plusp r-right))
                  (progn
                    (scenic.draw:line-to (- right r-right) bottom)

                    (let* ((end-angle (/ pi 2))
                           (start-angle (- end-angle (/ pi 2 (1+ (/ bdr-right bdr-bottom))))))

                      (add-path-arc
                       (- right r-right)
                       (- bottom r-right)
                       r-right
                       r-right
                       end-angle
                       start-angle
                       t)

                      (add-path-arc
                       (- right r-right)
                       (- bottom r-right)
                       (+ (- r-right bdr-bottom)
                          (- bdr-bottom bdr-right))
                       (- r-right bdr-bottom)
                       start-angle
                       end-angle
                       nil)))
                  ;; else
                  (progn
                    (scenic.draw:line-to (- right bdr-right)
                                   (- bottom bdr-bottom))
                    (scenic.draw:line-to right bottom)))

              (scenic.draw:fill-path)))

          ;; top border

          (when (and bdr-top (plusp bdr-top))
            (scenic.draw:set-color (scenic.style:border-color border-top))

            (let ((r-left radius-top-left)
                  (r-right radius-top-right))

              (if (and r-left (plusp r-left))
                  (let* ((end-angle (/ (* pi 3) 2))
                         (start-angle (- end-angle (/ pi 2 (1+ (/ bdr-left bdr-top))))))
                    (add-path-arc
                     (+ left r-left)
                     (+ top r-left)
                     r-left
                     r-left
                     end-angle
                     start-angle
                     t)

                    (add-path-arc
                     (+ left r-left)
                     (+ top r-left)
                     (+ (- r-left bdr-top)
                        (- bdr-top bdr-left))
                     (- r-left bdr-top)
                     start-angle
                     end-angle
                     nil))
                  ;; else
                  (progn
                    (scenic.draw:move-to left top)
                    (scenic.draw:line-to (+ left bdr-left) (+ top bdr-top))))

              (if (and r-right (plusp r-right))
                  (progn
                    (scenic.draw:line-to (- right r-right)
                                   (+ top bdr-top))

                    (let* ((start-angle (/ (* pi 3) 2))
                           (end-angle (+ start-angle (/ pi 2 (1+ (/ bdr-right bdr-top))))))
                      (add-path-arc
                       (- right r-right)
                       (+ top r-right)
                       (+ (- r-right bdr-top) (- bdr-top bdr-right))
                       (- r-right bdr-top)
                       start-angle
                       end-angle
                       nil)

                      (add-path-arc
                       (- right r-right)
                       (+ top r-right)
                       r-right
                       r-right
                       end-angle
                       start-angle
                       t)))
                  ;; else
                  (progn
                    (scenic.draw:line-to (- right bdr-right) (+ top bdr-top))
                    (scenic.draw:line-to right top)))
             
              (scenic.draw:fill-path)))

          ;; left border
  
          (when (and bdr-left (plusp bdr-left))
            (scenic.draw:set-color (scenic.style:border-color border-left))

            (let ((r-top radius-top-left)
                  (r-bottom radius-bottom-left))

              (if (and r-top (plusp r-top))
                  (let* ((start-angle pi)
                         (end-angle (+ start-angle (/ pi 2 (1+ (/ bdr-top bdr-left))))))

                    (add-path-arc
                     (+ left r-top)
                     (+ top r-top)
                     (- r-top bdr-left)
                     (+ (- r-top bdr-left)
                        (- bdr-left bdr-top))
                     start-angle
                     end-angle
                     nil)

                    (add-path-arc
                     (+ left r-top)
                     (+ top r-top)
                     r-top
                     r-top
                     end-angle
                     start-angle
                     t))
                  ;; else
                  (progn
                    (scenic.draw:move-to (+ left bdr-left) (+ top bdr-top))
                    (scenic.draw:line-to left top)))

              (if (and r-bottom (plusp r-bottom))
                  (progn
                    (scenic.draw:line-to left (- bottom r-bottom))

                    (let* ((end-angle pi)
                           (start-angle (- end-angle (/ pi 2 (1+ (/ bdr-bottom bdr-left))))))

                      (add-path-arc
                       (+ left r-bottom)
                       (- bottom r-bottom)
                       r-bottom
                       r-bottom
                       end-angle
                       start-angle
                       t)

                      (add-path-arc
                       (+ left r-bottom)
                       (- bottom r-bottom)
                       (- r-bottom bdr-left)
                       (+ (- r-bottom bdr-left)
                          (- bdr-left bdr-bottom))
                       start-angle
                       end-angle
                       nil)))
                  ;; else
                  (progn
                    (scenic.draw:line-to left bottom)
                    (scenic.draw:line-to (+ left bdr-left) (- bottom bdr-bottom))))
              (scenic.draw:fill-path)))))))

  (scenic.draw:restore))

;; Generic value-model like interface
(defgeneric value (value-model))
(defgeneric (setf value) (value value-model))
(defgeneric value-model-p (obj)
  (:method (obj)
    nil))

(defmethod value (obj)
  (if (value-model-p obj)
      (value obj)
      obj))

(defmacro define-vm-accessor (name class &key on-change (test 'eql))
  "Defines Value Model accessors"
  `(progn
     (defmethod ,name ((object ,class))
       (with-slots (,name) object
         (if (value-model-p ,name)
             (value ,name)
             ,name)))
     (defmethod (setf ,name) (val (object ,class))
       (with-slots (,name) object
         (if (value-model-p ,name)
             (when (not (,test ,name val))
               (setf (value ,name) val))
             (setf ,name val))
         ,@(when (member :invalidate on-change)
             `((scenic:invalidate object)))
         ,@(when (member :layout on-change)
             `((scenic::mark-for-layout object)))))))
